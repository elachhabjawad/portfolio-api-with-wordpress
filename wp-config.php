<?php

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the website, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/documentation/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'portfolio-api');

/** Database username */
define('DB_USER', 'root');

/** Database password */
define('DB_PASSWORD', '');

/** Database hostname */
define('DB_HOST', 'localhost');

/** Database charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The database collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Db[-)m?yPVALYnh&nBJ#Bx[_^sni[NbbEi;BWN=yK-$lPVtsIYq&&J/>xVys9S @');
define('SECURE_AUTH_KEY',  '*uJyvU~#;r_rBhp!XXPDVVN_*$.&+hz]xYu#$fOQ)NM_85^W97;ZpAnYh*@xbvmT');
define('LOGGED_IN_KEY',    '{,0G]Y%}^9{L/u)qCEAD.b2]aNawU>iY`DichUZJq6c%FNW G[Bn8DXsr#lM?,dj');
define('NONCE_KEY',        '22V;G;7. wun2^Z(Y(2fhkhT2edr?A^>id9G{,awis0~:_M$guY!2^hF/,QNvRl,');
define('AUTH_SALT',        'Vtg_W+i|Qv8@Rrb*1jvD;EX5$7||~k~y|Mu$fsE{jU.fqOdr|#_u;*qG|^v{YVe9');
define('SECURE_AUTH_SALT', 'mn{:qD+?^_4PQ]D.TwO8Cm}+cD0uJ7U_]Jyx#1QGb/0[Nf4XLvj*uX~X?e7Dj/Y?');
define('LOGGED_IN_SALT',   'F6[lZ@Lt8xjA9R:z{}#:Cdklvtfm2nRI7]Xu01qo>[h^P% iU%Qpf@e0kMRfe@.9');
define('NONCE_SALT',       'YsEzW-n#AU*IBn%X@uuo*[yH&WDJ;%Ju!pU6YVJriOMB)q42<L|l]g~!-mheTS0`');
define('JWT_AUTH_SECRET_KEY', '05e271abf7effb8e4baf2f8b6e00e92e6d76d32b');
define('JWT_AUTH_CORS_ENABLE', true);

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'portfolio_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/documentation/article/debugging-in-wordpress/
 */
define('WP_DEBUG', false);

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH')) {
	define('ABSPATH', __DIR__ . '/');
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
