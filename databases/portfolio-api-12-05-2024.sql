-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 12 mai 2024 à 20:57
-- Version du serveur : 10.4.27-MariaDB
-- Version de PHP : 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `portfolio-api`
--

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_commentmeta`
--

CREATE TABLE `portfolio_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_comments`
--

CREATE TABLE `portfolio_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `comment_author` tinytext NOT NULL,
  `comment_author_email` varchar(100) NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT 0,
  `comment_approved` varchar(20) NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) NOT NULL DEFAULT '',
  `comment_type` varchar(20) NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_links`
--

CREATE TABLE `portfolio_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) NOT NULL DEFAULT '',
  `link_name` varchar(255) NOT NULL DEFAULT '',
  `link_image` varchar(255) NOT NULL DEFAULT '',
  `link_target` varchar(25) NOT NULL DEFAULT '',
  `link_description` varchar(255) NOT NULL DEFAULT '',
  `link_visible` varchar(20) NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT 1,
  `link_rating` int(11) NOT NULL DEFAULT 0,
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) NOT NULL DEFAULT '',
  `link_notes` mediumtext NOT NULL,
  `link_rss` varchar(255) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_options`
--

CREATE TABLE `portfolio_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) NOT NULL DEFAULT '',
  `option_value` longtext NOT NULL,
  `autoload` varchar(20) NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_options`
--

INSERT INTO `portfolio_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://dev.portfolio-api.com', 'yes'),
(2, 'home', 'http://dev.portfolio-api.com', 'yes'),
(3, 'blogname', 'Portfolio API', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'elachhabjawad@gmail.com', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'F j, Y', 'yes'),
(24, 'time_format', 'g:i a', 'yes'),
(25, 'links_updated_date_format', 'F j, Y g:i a', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:21:\"polylang/polylang.php\";i:1;s:34:\"advanced-custom-fields-pro/acf.php\";i:2;s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";i:3;s:37:\"post-types-order/post-types-order.php\";i:4;s:25:\"sucuri-scanner/sucuri.php\";i:5;s:23:\"wordfence/wordfence.php\";i:6;s:33:\"wps-hide-login/wps-hide-login.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'portfolio', 'yes'),
(41, 'stylesheet', 'portfolio', 'yes'),
(42, 'comment_registration', '0', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '57155', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'posts', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '0', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '0', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:0:{}', 'yes'),
(77, 'widget_text', 'a:0:{}', 'yes'),
(78, 'widget_rss', 'a:0:{}', 'yes'),
(79, 'uninstall_plugins', 'a:1:{s:25:\"sucuri-scanner/sucuri.php\";s:19:\"sucuriscanUninstall\";}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '0', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '0', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1730019009', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'wp_force_deactivated_plugins', 'a:0:{}', 'yes'),
(99, 'wp_attachment_pages_enabled', '0', 'yes'),
(100, 'initial_db_version', '57155', 'yes'),
(101, 'portfolio_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:64:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:23:\"wf2fa_activate_2fa_self\";b:1;s:25:\"wf2fa_activate_2fa_others\";b:1;s:21:\"wf2fa_manage_settings\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(102, 'fresh_site', '0', 'yes'),
(103, 'user_count', '1', 'no'),
(104, 'widget_block', 'a:6:{i:2;a:1:{s:7:\"content\";s:19:\"<!-- wp:search /-->\";}i:3;a:1:{s:7:\"content\";s:154:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Posts</h2><!-- /wp:heading --><!-- wp:latest-posts /--></div><!-- /wp:group -->\";}i:4;a:1:{s:7:\"content\";s:227:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Recent Comments</h2><!-- /wp:heading --><!-- wp:latest-comments {\"displayAvatar\":false,\"displayDate\":false,\"displayExcerpt\":false} /--></div><!-- /wp:group -->\";}i:5;a:1:{s:7:\"content\";s:146:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Archives</h2><!-- /wp:heading --><!-- wp:archives /--></div><!-- /wp:group -->\";}i:6;a:1:{s:7:\"content\";s:150:\"<!-- wp:group --><div class=\"wp-block-group\"><!-- wp:heading --><h2>Categories</h2><!-- /wp:heading --><!-- wp:categories /--></div><!-- /wp:group -->\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:5:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";i:3;s:7:\"block-5\";i:4;s:7:\"block-6\";}s:13:\"array_version\";i:3;}', 'yes'),
(106, 'cron', 'a:15:{i:1715529011;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1715529624;a:1:{s:21:\"wordfence_ls_ntp_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1715529631;a:1:{s:21:\"wordfence_hourly_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1715529743;a:1:{s:19:\"jwt_auth_share_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1715547011;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1715547065;a:1:{s:21:\"wp_update_user_counts\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1715590211;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1715590265;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1715590267;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1715598031;a:1:{s:20:\"wordfence_daily_cron\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1715598057;a:1:{s:25:\"sucuriscan_scheduled_scan\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1715616000;a:1:{s:31:\"wordfence_email_activity_report\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1715676897;a:1:{s:30:\"wp_delete_temp_updater_backups\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}i:1715763011;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(107, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(117, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(118, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(119, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(120, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(121, '_transient_wp_core_block_css_files', 'a:2:{s:7:\"version\";s:5:\"6.5.2\";s:5:\"files\";a:500:{i:0;s:23:\"archives/editor-rtl.css\";i:1;s:27:\"archives/editor-rtl.min.css\";i:2;s:19:\"archives/editor.css\";i:3;s:23:\"archives/editor.min.css\";i:4;s:22:\"archives/style-rtl.css\";i:5;s:26:\"archives/style-rtl.min.css\";i:6;s:18:\"archives/style.css\";i:7;s:22:\"archives/style.min.css\";i:8;s:20:\"audio/editor-rtl.css\";i:9;s:24:\"audio/editor-rtl.min.css\";i:10;s:16:\"audio/editor.css\";i:11;s:20:\"audio/editor.min.css\";i:12;s:19:\"audio/style-rtl.css\";i:13;s:23:\"audio/style-rtl.min.css\";i:14;s:15:\"audio/style.css\";i:15;s:19:\"audio/style.min.css\";i:16;s:19:\"audio/theme-rtl.css\";i:17;s:23:\"audio/theme-rtl.min.css\";i:18;s:15:\"audio/theme.css\";i:19;s:19:\"audio/theme.min.css\";i:20;s:21:\"avatar/editor-rtl.css\";i:21;s:25:\"avatar/editor-rtl.min.css\";i:22;s:17:\"avatar/editor.css\";i:23;s:21:\"avatar/editor.min.css\";i:24;s:20:\"avatar/style-rtl.css\";i:25;s:24:\"avatar/style-rtl.min.css\";i:26;s:16:\"avatar/style.css\";i:27;s:20:\"avatar/style.min.css\";i:28;s:20:\"block/editor-rtl.css\";i:29;s:24:\"block/editor-rtl.min.css\";i:30;s:16:\"block/editor.css\";i:31;s:20:\"block/editor.min.css\";i:32;s:21:\"button/editor-rtl.css\";i:33;s:25:\"button/editor-rtl.min.css\";i:34;s:17:\"button/editor.css\";i:35;s:21:\"button/editor.min.css\";i:36;s:20:\"button/style-rtl.css\";i:37;s:24:\"button/style-rtl.min.css\";i:38;s:16:\"button/style.css\";i:39;s:20:\"button/style.min.css\";i:40;s:22:\"buttons/editor-rtl.css\";i:41;s:26:\"buttons/editor-rtl.min.css\";i:42;s:18:\"buttons/editor.css\";i:43;s:22:\"buttons/editor.min.css\";i:44;s:21:\"buttons/style-rtl.css\";i:45;s:25:\"buttons/style-rtl.min.css\";i:46;s:17:\"buttons/style.css\";i:47;s:21:\"buttons/style.min.css\";i:48;s:22:\"calendar/style-rtl.css\";i:49;s:26:\"calendar/style-rtl.min.css\";i:50;s:18:\"calendar/style.css\";i:51;s:22:\"calendar/style.min.css\";i:52;s:25:\"categories/editor-rtl.css\";i:53;s:29:\"categories/editor-rtl.min.css\";i:54;s:21:\"categories/editor.css\";i:55;s:25:\"categories/editor.min.css\";i:56;s:24:\"categories/style-rtl.css\";i:57;s:28:\"categories/style-rtl.min.css\";i:58;s:20:\"categories/style.css\";i:59;s:24:\"categories/style.min.css\";i:60;s:19:\"code/editor-rtl.css\";i:61;s:23:\"code/editor-rtl.min.css\";i:62;s:15:\"code/editor.css\";i:63;s:19:\"code/editor.min.css\";i:64;s:18:\"code/style-rtl.css\";i:65;s:22:\"code/style-rtl.min.css\";i:66;s:14:\"code/style.css\";i:67;s:18:\"code/style.min.css\";i:68;s:18:\"code/theme-rtl.css\";i:69;s:22:\"code/theme-rtl.min.css\";i:70;s:14:\"code/theme.css\";i:71;s:18:\"code/theme.min.css\";i:72;s:22:\"columns/editor-rtl.css\";i:73;s:26:\"columns/editor-rtl.min.css\";i:74;s:18:\"columns/editor.css\";i:75;s:22:\"columns/editor.min.css\";i:76;s:21:\"columns/style-rtl.css\";i:77;s:25:\"columns/style-rtl.min.css\";i:78;s:17:\"columns/style.css\";i:79;s:21:\"columns/style.min.css\";i:80;s:29:\"comment-content/style-rtl.css\";i:81;s:33:\"comment-content/style-rtl.min.css\";i:82;s:25:\"comment-content/style.css\";i:83;s:29:\"comment-content/style.min.css\";i:84;s:30:\"comment-template/style-rtl.css\";i:85;s:34:\"comment-template/style-rtl.min.css\";i:86;s:26:\"comment-template/style.css\";i:87;s:30:\"comment-template/style.min.css\";i:88;s:42:\"comments-pagination-numbers/editor-rtl.css\";i:89;s:46:\"comments-pagination-numbers/editor-rtl.min.css\";i:90;s:38:\"comments-pagination-numbers/editor.css\";i:91;s:42:\"comments-pagination-numbers/editor.min.css\";i:92;s:34:\"comments-pagination/editor-rtl.css\";i:93;s:38:\"comments-pagination/editor-rtl.min.css\";i:94;s:30:\"comments-pagination/editor.css\";i:95;s:34:\"comments-pagination/editor.min.css\";i:96;s:33:\"comments-pagination/style-rtl.css\";i:97;s:37:\"comments-pagination/style-rtl.min.css\";i:98;s:29:\"comments-pagination/style.css\";i:99;s:33:\"comments-pagination/style.min.css\";i:100;s:29:\"comments-title/editor-rtl.css\";i:101;s:33:\"comments-title/editor-rtl.min.css\";i:102;s:25:\"comments-title/editor.css\";i:103;s:29:\"comments-title/editor.min.css\";i:104;s:23:\"comments/editor-rtl.css\";i:105;s:27:\"comments/editor-rtl.min.css\";i:106;s:19:\"comments/editor.css\";i:107;s:23:\"comments/editor.min.css\";i:108;s:22:\"comments/style-rtl.css\";i:109;s:26:\"comments/style-rtl.min.css\";i:110;s:18:\"comments/style.css\";i:111;s:22:\"comments/style.min.css\";i:112;s:20:\"cover/editor-rtl.css\";i:113;s:24:\"cover/editor-rtl.min.css\";i:114;s:16:\"cover/editor.css\";i:115;s:20:\"cover/editor.min.css\";i:116;s:19:\"cover/style-rtl.css\";i:117;s:23:\"cover/style-rtl.min.css\";i:118;s:15:\"cover/style.css\";i:119;s:19:\"cover/style.min.css\";i:120;s:22:\"details/editor-rtl.css\";i:121;s:26:\"details/editor-rtl.min.css\";i:122;s:18:\"details/editor.css\";i:123;s:22:\"details/editor.min.css\";i:124;s:21:\"details/style-rtl.css\";i:125;s:25:\"details/style-rtl.min.css\";i:126;s:17:\"details/style.css\";i:127;s:21:\"details/style.min.css\";i:128;s:20:\"embed/editor-rtl.css\";i:129;s:24:\"embed/editor-rtl.min.css\";i:130;s:16:\"embed/editor.css\";i:131;s:20:\"embed/editor.min.css\";i:132;s:19:\"embed/style-rtl.css\";i:133;s:23:\"embed/style-rtl.min.css\";i:134;s:15:\"embed/style.css\";i:135;s:19:\"embed/style.min.css\";i:136;s:19:\"embed/theme-rtl.css\";i:137;s:23:\"embed/theme-rtl.min.css\";i:138;s:15:\"embed/theme.css\";i:139;s:19:\"embed/theme.min.css\";i:140;s:19:\"file/editor-rtl.css\";i:141;s:23:\"file/editor-rtl.min.css\";i:142;s:15:\"file/editor.css\";i:143;s:19:\"file/editor.min.css\";i:144;s:18:\"file/style-rtl.css\";i:145;s:22:\"file/style-rtl.min.css\";i:146;s:14:\"file/style.css\";i:147;s:18:\"file/style.min.css\";i:148;s:23:\"footnotes/style-rtl.css\";i:149;s:27:\"footnotes/style-rtl.min.css\";i:150;s:19:\"footnotes/style.css\";i:151;s:23:\"footnotes/style.min.css\";i:152;s:23:\"freeform/editor-rtl.css\";i:153;s:27:\"freeform/editor-rtl.min.css\";i:154;s:19:\"freeform/editor.css\";i:155;s:23:\"freeform/editor.min.css\";i:156;s:22:\"gallery/editor-rtl.css\";i:157;s:26:\"gallery/editor-rtl.min.css\";i:158;s:18:\"gallery/editor.css\";i:159;s:22:\"gallery/editor.min.css\";i:160;s:21:\"gallery/style-rtl.css\";i:161;s:25:\"gallery/style-rtl.min.css\";i:162;s:17:\"gallery/style.css\";i:163;s:21:\"gallery/style.min.css\";i:164;s:21:\"gallery/theme-rtl.css\";i:165;s:25:\"gallery/theme-rtl.min.css\";i:166;s:17:\"gallery/theme.css\";i:167;s:21:\"gallery/theme.min.css\";i:168;s:20:\"group/editor-rtl.css\";i:169;s:24:\"group/editor-rtl.min.css\";i:170;s:16:\"group/editor.css\";i:171;s:20:\"group/editor.min.css\";i:172;s:19:\"group/style-rtl.css\";i:173;s:23:\"group/style-rtl.min.css\";i:174;s:15:\"group/style.css\";i:175;s:19:\"group/style.min.css\";i:176;s:19:\"group/theme-rtl.css\";i:177;s:23:\"group/theme-rtl.min.css\";i:178;s:15:\"group/theme.css\";i:179;s:19:\"group/theme.min.css\";i:180;s:21:\"heading/style-rtl.css\";i:181;s:25:\"heading/style-rtl.min.css\";i:182;s:17:\"heading/style.css\";i:183;s:21:\"heading/style.min.css\";i:184;s:19:\"html/editor-rtl.css\";i:185;s:23:\"html/editor-rtl.min.css\";i:186;s:15:\"html/editor.css\";i:187;s:19:\"html/editor.min.css\";i:188;s:20:\"image/editor-rtl.css\";i:189;s:24:\"image/editor-rtl.min.css\";i:190;s:16:\"image/editor.css\";i:191;s:20:\"image/editor.min.css\";i:192;s:19:\"image/style-rtl.css\";i:193;s:23:\"image/style-rtl.min.css\";i:194;s:15:\"image/style.css\";i:195;s:19:\"image/style.min.css\";i:196;s:19:\"image/theme-rtl.css\";i:197;s:23:\"image/theme-rtl.min.css\";i:198;s:15:\"image/theme.css\";i:199;s:19:\"image/theme.min.css\";i:200;s:29:\"latest-comments/style-rtl.css\";i:201;s:33:\"latest-comments/style-rtl.min.css\";i:202;s:25:\"latest-comments/style.css\";i:203;s:29:\"latest-comments/style.min.css\";i:204;s:27:\"latest-posts/editor-rtl.css\";i:205;s:31:\"latest-posts/editor-rtl.min.css\";i:206;s:23:\"latest-posts/editor.css\";i:207;s:27:\"latest-posts/editor.min.css\";i:208;s:26:\"latest-posts/style-rtl.css\";i:209;s:30:\"latest-posts/style-rtl.min.css\";i:210;s:22:\"latest-posts/style.css\";i:211;s:26:\"latest-posts/style.min.css\";i:212;s:18:\"list/style-rtl.css\";i:213;s:22:\"list/style-rtl.min.css\";i:214;s:14:\"list/style.css\";i:215;s:18:\"list/style.min.css\";i:216;s:25:\"media-text/editor-rtl.css\";i:217;s:29:\"media-text/editor-rtl.min.css\";i:218;s:21:\"media-text/editor.css\";i:219;s:25:\"media-text/editor.min.css\";i:220;s:24:\"media-text/style-rtl.css\";i:221;s:28:\"media-text/style-rtl.min.css\";i:222;s:20:\"media-text/style.css\";i:223;s:24:\"media-text/style.min.css\";i:224;s:19:\"more/editor-rtl.css\";i:225;s:23:\"more/editor-rtl.min.css\";i:226;s:15:\"more/editor.css\";i:227;s:19:\"more/editor.min.css\";i:228;s:30:\"navigation-link/editor-rtl.css\";i:229;s:34:\"navigation-link/editor-rtl.min.css\";i:230;s:26:\"navigation-link/editor.css\";i:231;s:30:\"navigation-link/editor.min.css\";i:232;s:29:\"navigation-link/style-rtl.css\";i:233;s:33:\"navigation-link/style-rtl.min.css\";i:234;s:25:\"navigation-link/style.css\";i:235;s:29:\"navigation-link/style.min.css\";i:236;s:33:\"navigation-submenu/editor-rtl.css\";i:237;s:37:\"navigation-submenu/editor-rtl.min.css\";i:238;s:29:\"navigation-submenu/editor.css\";i:239;s:33:\"navigation-submenu/editor.min.css\";i:240;s:25:\"navigation/editor-rtl.css\";i:241;s:29:\"navigation/editor-rtl.min.css\";i:242;s:21:\"navigation/editor.css\";i:243;s:25:\"navigation/editor.min.css\";i:244;s:24:\"navigation/style-rtl.css\";i:245;s:28:\"navigation/style-rtl.min.css\";i:246;s:20:\"navigation/style.css\";i:247;s:24:\"navigation/style.min.css\";i:248;s:23:\"nextpage/editor-rtl.css\";i:249;s:27:\"nextpage/editor-rtl.min.css\";i:250;s:19:\"nextpage/editor.css\";i:251;s:23:\"nextpage/editor.min.css\";i:252;s:24:\"page-list/editor-rtl.css\";i:253;s:28:\"page-list/editor-rtl.min.css\";i:254;s:20:\"page-list/editor.css\";i:255;s:24:\"page-list/editor.min.css\";i:256;s:23:\"page-list/style-rtl.css\";i:257;s:27:\"page-list/style-rtl.min.css\";i:258;s:19:\"page-list/style.css\";i:259;s:23:\"page-list/style.min.css\";i:260;s:24:\"paragraph/editor-rtl.css\";i:261;s:28:\"paragraph/editor-rtl.min.css\";i:262;s:20:\"paragraph/editor.css\";i:263;s:24:\"paragraph/editor.min.css\";i:264;s:23:\"paragraph/style-rtl.css\";i:265;s:27:\"paragraph/style-rtl.min.css\";i:266;s:19:\"paragraph/style.css\";i:267;s:23:\"paragraph/style.min.css\";i:268;s:25:\"post-author/style-rtl.css\";i:269;s:29:\"post-author/style-rtl.min.css\";i:270;s:21:\"post-author/style.css\";i:271;s:25:\"post-author/style.min.css\";i:272;s:33:\"post-comments-form/editor-rtl.css\";i:273;s:37:\"post-comments-form/editor-rtl.min.css\";i:274;s:29:\"post-comments-form/editor.css\";i:275;s:33:\"post-comments-form/editor.min.css\";i:276;s:32:\"post-comments-form/style-rtl.css\";i:277;s:36:\"post-comments-form/style-rtl.min.css\";i:278;s:28:\"post-comments-form/style.css\";i:279;s:32:\"post-comments-form/style.min.css\";i:280;s:27:\"post-content/editor-rtl.css\";i:281;s:31:\"post-content/editor-rtl.min.css\";i:282;s:23:\"post-content/editor.css\";i:283;s:27:\"post-content/editor.min.css\";i:284;s:23:\"post-date/style-rtl.css\";i:285;s:27:\"post-date/style-rtl.min.css\";i:286;s:19:\"post-date/style.css\";i:287;s:23:\"post-date/style.min.css\";i:288;s:27:\"post-excerpt/editor-rtl.css\";i:289;s:31:\"post-excerpt/editor-rtl.min.css\";i:290;s:23:\"post-excerpt/editor.css\";i:291;s:27:\"post-excerpt/editor.min.css\";i:292;s:26:\"post-excerpt/style-rtl.css\";i:293;s:30:\"post-excerpt/style-rtl.min.css\";i:294;s:22:\"post-excerpt/style.css\";i:295;s:26:\"post-excerpt/style.min.css\";i:296;s:34:\"post-featured-image/editor-rtl.css\";i:297;s:38:\"post-featured-image/editor-rtl.min.css\";i:298;s:30:\"post-featured-image/editor.css\";i:299;s:34:\"post-featured-image/editor.min.css\";i:300;s:33:\"post-featured-image/style-rtl.css\";i:301;s:37:\"post-featured-image/style-rtl.min.css\";i:302;s:29:\"post-featured-image/style.css\";i:303;s:33:\"post-featured-image/style.min.css\";i:304;s:34:\"post-navigation-link/style-rtl.css\";i:305;s:38:\"post-navigation-link/style-rtl.min.css\";i:306;s:30:\"post-navigation-link/style.css\";i:307;s:34:\"post-navigation-link/style.min.css\";i:308;s:28:\"post-template/editor-rtl.css\";i:309;s:32:\"post-template/editor-rtl.min.css\";i:310;s:24:\"post-template/editor.css\";i:311;s:28:\"post-template/editor.min.css\";i:312;s:27:\"post-template/style-rtl.css\";i:313;s:31:\"post-template/style-rtl.min.css\";i:314;s:23:\"post-template/style.css\";i:315;s:27:\"post-template/style.min.css\";i:316;s:24:\"post-terms/style-rtl.css\";i:317;s:28:\"post-terms/style-rtl.min.css\";i:318;s:20:\"post-terms/style.css\";i:319;s:24:\"post-terms/style.min.css\";i:320;s:24:\"post-title/style-rtl.css\";i:321;s:28:\"post-title/style-rtl.min.css\";i:322;s:20:\"post-title/style.css\";i:323;s:24:\"post-title/style.min.css\";i:324;s:26:\"preformatted/style-rtl.css\";i:325;s:30:\"preformatted/style-rtl.min.css\";i:326;s:22:\"preformatted/style.css\";i:327;s:26:\"preformatted/style.min.css\";i:328;s:24:\"pullquote/editor-rtl.css\";i:329;s:28:\"pullquote/editor-rtl.min.css\";i:330;s:20:\"pullquote/editor.css\";i:331;s:24:\"pullquote/editor.min.css\";i:332;s:23:\"pullquote/style-rtl.css\";i:333;s:27:\"pullquote/style-rtl.min.css\";i:334;s:19:\"pullquote/style.css\";i:335;s:23:\"pullquote/style.min.css\";i:336;s:23:\"pullquote/theme-rtl.css\";i:337;s:27:\"pullquote/theme-rtl.min.css\";i:338;s:19:\"pullquote/theme.css\";i:339;s:23:\"pullquote/theme.min.css\";i:340;s:39:\"query-pagination-numbers/editor-rtl.css\";i:341;s:43:\"query-pagination-numbers/editor-rtl.min.css\";i:342;s:35:\"query-pagination-numbers/editor.css\";i:343;s:39:\"query-pagination-numbers/editor.min.css\";i:344;s:31:\"query-pagination/editor-rtl.css\";i:345;s:35:\"query-pagination/editor-rtl.min.css\";i:346;s:27:\"query-pagination/editor.css\";i:347;s:31:\"query-pagination/editor.min.css\";i:348;s:30:\"query-pagination/style-rtl.css\";i:349;s:34:\"query-pagination/style-rtl.min.css\";i:350;s:26:\"query-pagination/style.css\";i:351;s:30:\"query-pagination/style.min.css\";i:352;s:25:\"query-title/style-rtl.css\";i:353;s:29:\"query-title/style-rtl.min.css\";i:354;s:21:\"query-title/style.css\";i:355;s:25:\"query-title/style.min.css\";i:356;s:20:\"query/editor-rtl.css\";i:357;s:24:\"query/editor-rtl.min.css\";i:358;s:16:\"query/editor.css\";i:359;s:20:\"query/editor.min.css\";i:360;s:19:\"quote/style-rtl.css\";i:361;s:23:\"quote/style-rtl.min.css\";i:362;s:15:\"quote/style.css\";i:363;s:19:\"quote/style.min.css\";i:364;s:19:\"quote/theme-rtl.css\";i:365;s:23:\"quote/theme-rtl.min.css\";i:366;s:15:\"quote/theme.css\";i:367;s:19:\"quote/theme.min.css\";i:368;s:23:\"read-more/style-rtl.css\";i:369;s:27:\"read-more/style-rtl.min.css\";i:370;s:19:\"read-more/style.css\";i:371;s:23:\"read-more/style.min.css\";i:372;s:18:\"rss/editor-rtl.css\";i:373;s:22:\"rss/editor-rtl.min.css\";i:374;s:14:\"rss/editor.css\";i:375;s:18:\"rss/editor.min.css\";i:376;s:17:\"rss/style-rtl.css\";i:377;s:21:\"rss/style-rtl.min.css\";i:378;s:13:\"rss/style.css\";i:379;s:17:\"rss/style.min.css\";i:380;s:21:\"search/editor-rtl.css\";i:381;s:25:\"search/editor-rtl.min.css\";i:382;s:17:\"search/editor.css\";i:383;s:21:\"search/editor.min.css\";i:384;s:20:\"search/style-rtl.css\";i:385;s:24:\"search/style-rtl.min.css\";i:386;s:16:\"search/style.css\";i:387;s:20:\"search/style.min.css\";i:388;s:20:\"search/theme-rtl.css\";i:389;s:24:\"search/theme-rtl.min.css\";i:390;s:16:\"search/theme.css\";i:391;s:20:\"search/theme.min.css\";i:392;s:24:\"separator/editor-rtl.css\";i:393;s:28:\"separator/editor-rtl.min.css\";i:394;s:20:\"separator/editor.css\";i:395;s:24:\"separator/editor.min.css\";i:396;s:23:\"separator/style-rtl.css\";i:397;s:27:\"separator/style-rtl.min.css\";i:398;s:19:\"separator/style.css\";i:399;s:23:\"separator/style.min.css\";i:400;s:23:\"separator/theme-rtl.css\";i:401;s:27:\"separator/theme-rtl.min.css\";i:402;s:19:\"separator/theme.css\";i:403;s:23:\"separator/theme.min.css\";i:404;s:24:\"shortcode/editor-rtl.css\";i:405;s:28:\"shortcode/editor-rtl.min.css\";i:406;s:20:\"shortcode/editor.css\";i:407;s:24:\"shortcode/editor.min.css\";i:408;s:24:\"site-logo/editor-rtl.css\";i:409;s:28:\"site-logo/editor-rtl.min.css\";i:410;s:20:\"site-logo/editor.css\";i:411;s:24:\"site-logo/editor.min.css\";i:412;s:23:\"site-logo/style-rtl.css\";i:413;s:27:\"site-logo/style-rtl.min.css\";i:414;s:19:\"site-logo/style.css\";i:415;s:23:\"site-logo/style.min.css\";i:416;s:27:\"site-tagline/editor-rtl.css\";i:417;s:31:\"site-tagline/editor-rtl.min.css\";i:418;s:23:\"site-tagline/editor.css\";i:419;s:27:\"site-tagline/editor.min.css\";i:420;s:25:\"site-title/editor-rtl.css\";i:421;s:29:\"site-title/editor-rtl.min.css\";i:422;s:21:\"site-title/editor.css\";i:423;s:25:\"site-title/editor.min.css\";i:424;s:24:\"site-title/style-rtl.css\";i:425;s:28:\"site-title/style-rtl.min.css\";i:426;s:20:\"site-title/style.css\";i:427;s:24:\"site-title/style.min.css\";i:428;s:26:\"social-link/editor-rtl.css\";i:429;s:30:\"social-link/editor-rtl.min.css\";i:430;s:22:\"social-link/editor.css\";i:431;s:26:\"social-link/editor.min.css\";i:432;s:27:\"social-links/editor-rtl.css\";i:433;s:31:\"social-links/editor-rtl.min.css\";i:434;s:23:\"social-links/editor.css\";i:435;s:27:\"social-links/editor.min.css\";i:436;s:26:\"social-links/style-rtl.css\";i:437;s:30:\"social-links/style-rtl.min.css\";i:438;s:22:\"social-links/style.css\";i:439;s:26:\"social-links/style.min.css\";i:440;s:21:\"spacer/editor-rtl.css\";i:441;s:25:\"spacer/editor-rtl.min.css\";i:442;s:17:\"spacer/editor.css\";i:443;s:21:\"spacer/editor.min.css\";i:444;s:20:\"spacer/style-rtl.css\";i:445;s:24:\"spacer/style-rtl.min.css\";i:446;s:16:\"spacer/style.css\";i:447;s:20:\"spacer/style.min.css\";i:448;s:20:\"table/editor-rtl.css\";i:449;s:24:\"table/editor-rtl.min.css\";i:450;s:16:\"table/editor.css\";i:451;s:20:\"table/editor.min.css\";i:452;s:19:\"table/style-rtl.css\";i:453;s:23:\"table/style-rtl.min.css\";i:454;s:15:\"table/style.css\";i:455;s:19:\"table/style.min.css\";i:456;s:19:\"table/theme-rtl.css\";i:457;s:23:\"table/theme-rtl.min.css\";i:458;s:15:\"table/theme.css\";i:459;s:19:\"table/theme.min.css\";i:460;s:23:\"tag-cloud/style-rtl.css\";i:461;s:27:\"tag-cloud/style-rtl.min.css\";i:462;s:19:\"tag-cloud/style.css\";i:463;s:23:\"tag-cloud/style.min.css\";i:464;s:28:\"template-part/editor-rtl.css\";i:465;s:32:\"template-part/editor-rtl.min.css\";i:466;s:24:\"template-part/editor.css\";i:467;s:28:\"template-part/editor.min.css\";i:468;s:27:\"template-part/theme-rtl.css\";i:469;s:31:\"template-part/theme-rtl.min.css\";i:470;s:23:\"template-part/theme.css\";i:471;s:27:\"template-part/theme.min.css\";i:472;s:30:\"term-description/style-rtl.css\";i:473;s:34:\"term-description/style-rtl.min.css\";i:474;s:26:\"term-description/style.css\";i:475;s:30:\"term-description/style.min.css\";i:476;s:27:\"text-columns/editor-rtl.css\";i:477;s:31:\"text-columns/editor-rtl.min.css\";i:478;s:23:\"text-columns/editor.css\";i:479;s:27:\"text-columns/editor.min.css\";i:480;s:26:\"text-columns/style-rtl.css\";i:481;s:30:\"text-columns/style-rtl.min.css\";i:482;s:22:\"text-columns/style.css\";i:483;s:26:\"text-columns/style.min.css\";i:484;s:19:\"verse/style-rtl.css\";i:485;s:23:\"verse/style-rtl.min.css\";i:486;s:15:\"verse/style.css\";i:487;s:19:\"verse/style.min.css\";i:488;s:20:\"video/editor-rtl.css\";i:489;s:24:\"video/editor-rtl.min.css\";i:490;s:16:\"video/editor.css\";i:491;s:20:\"video/editor.min.css\";i:492;s:19:\"video/style-rtl.css\";i:493;s:23:\"video/style-rtl.min.css\";i:494;s:15:\"video/style.css\";i:495;s:19:\"video/style.min.css\";i:496;s:19:\"video/theme-rtl.css\";i:497;s:23:\"video/theme-rtl.min.css\";i:498;s:15:\"video/theme.css\";i:499;s:19:\"video/theme.min.css\";}}', 'yes'),
(123, 'recovery_keys', 'a:0:{}', 'yes'),
(125, 'theme_mods_twentytwentyfour', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1715257500;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:7:\"block-2\";i:1;s:7:\"block-3\";i:2;s:7:\"block-4\";}s:9:\"sidebar-2\";a:2:{i:0;s:7:\"block-5\";i:1;s:7:\"block-6\";}}}}', 'no'),
(139, 'can_compress_scripts', '1', 'yes'),
(150, '_site_transient_wp_plugin_dependencies_plugin_data', 'a:0:{}', 'no'),
(151, 'recently_activated', 'a:3:{s:32:\"real-custom-post-order/index.php\";i:1715256697;s:31:\"simply-static/simply-static.php\";i:1714930194;s:33:\"classic-editor/classic-editor.php\";i:1714924589;}', 'yes'),
(154, 'finished_updating_comment_type', '1', 'yes'),
(161, 'polylang', 'a:14:{s:7:\"browser\";i:0;s:7:\"rewrite\";i:1;s:12:\"hide_default\";i:1;s:10:\"force_lang\";i:1;s:13:\"redirect_lang\";i:0;s:13:\"media_support\";b:0;s:9:\"uninstall\";i:0;s:4:\"sync\";a:0:{}s:10:\"post_types\";a:4:{i:0;s:7:\"profile\";i:1;s:11:\"technologie\";i:2;s:7:\"project\";i:3;s:10:\"experience\";}s:10:\"taxonomies\";a:0:{}s:7:\"domains\";a:0:{}s:7:\"version\";s:5:\"3.6.1\";s:16:\"first_activation\";i:1714467307;s:12:\"default_lang\";s:2:\"en\";}', 'yes'),
(162, 'polylang_wpml_strings', 'a:0:{}', 'yes'),
(163, 'widget_polylang', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(175, 'category_children', 'a:0:{}', 'yes'),
(192, 'pll_dismissed_notices', 'a:1:{i:0;s:6:\"wizard\";}', 'yes'),
(195, 'wp_calendar_block_has_published_posts', '1', 'yes'),
(228, 'https_detection_errors', 'a:2:{s:23:\"ssl_verification_failed\";a:1:{i:0;s:24:\"SSL verification failed.\";}s:19:\"bad_response_source\";a:1:{i:0;s:55:\"It looks like the response did not come from this site.\";}}', 'yes'),
(229, '_transient_health-check-site-status-result', '{\"good\":\"17\",\"recommended\":\"4\",\"critical\":\"3\"}', 'yes'),
(234, 'acf_version', '6.2.0', 'yes'),
(278, 'jwt_auth_admin_notice', '1', 'yes');
INSERT INTO `portfolio_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(284, 'rewrite_rules', 'a:359:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:53:\"^(fr)/wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:92:\"index.php?lang=$matches[1]&sitemap=$matches[2]&sitemap-subtype=$matches[3]&paged=$matches[4]\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:39:\"^(fr)/wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:64:\"index.php?lang=$matches[1]&sitemap=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:16:\"(fr)/profiles/?$\";s:44:\"index.php?lang=$matches[1]&post_type=profile\";s:11:\"profiles/?$\";s:35:\"index.php?lang=en&post_type=profile\";s:46:\"(fr)/profiles/feed/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=profile&feed=$matches[2]\";s:41:\"profiles/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=profile&feed=$matches[1]\";s:41:\"(fr)/profiles/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=profile&feed=$matches[2]\";s:36:\"profiles/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=profile&feed=$matches[1]\";s:33:\"(fr)/profiles/page/([0-9]{1,})/?$\";s:62:\"index.php?lang=$matches[1]&post_type=profile&paged=$matches[2]\";s:28:\"profiles/page/([0-9]{1,})/?$\";s:53:\"index.php?lang=en&post_type=profile&paged=$matches[1]\";s:20:\"(fr)/technologies/?$\";s:48:\"index.php?lang=$matches[1]&post_type=technologie\";s:15:\"technologies/?$\";s:39:\"index.php?lang=en&post_type=technologie\";s:50:\"(fr)/technologies/feed/(feed|rdf|rss|rss2|atom)/?$\";s:65:\"index.php?lang=$matches[1]&post_type=technologie&feed=$matches[2]\";s:45:\"technologies/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?lang=en&post_type=technologie&feed=$matches[1]\";s:45:\"(fr)/technologies/(feed|rdf|rss|rss2|atom)/?$\";s:65:\"index.php?lang=$matches[1]&post_type=technologie&feed=$matches[2]\";s:40:\"technologies/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?lang=en&post_type=technologie&feed=$matches[1]\";s:37:\"(fr)/technologies/page/([0-9]{1,})/?$\";s:66:\"index.php?lang=$matches[1]&post_type=technologie&paged=$matches[2]\";s:32:\"technologies/page/([0-9]{1,})/?$\";s:57:\"index.php?lang=en&post_type=technologie&paged=$matches[1]\";s:16:\"(fr)/projects/?$\";s:44:\"index.php?lang=$matches[1]&post_type=project\";s:11:\"projects/?$\";s:35:\"index.php?lang=en&post_type=project\";s:46:\"(fr)/projects/feed/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=project&feed=$matches[2]\";s:41:\"projects/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=project&feed=$matches[1]\";s:41:\"(fr)/projects/(feed|rdf|rss|rss2|atom)/?$\";s:61:\"index.php?lang=$matches[1]&post_type=project&feed=$matches[2]\";s:36:\"projects/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?lang=en&post_type=project&feed=$matches[1]\";s:33:\"(fr)/projects/page/([0-9]{1,})/?$\";s:62:\"index.php?lang=$matches[1]&post_type=project&paged=$matches[2]\";s:28:\"projects/page/([0-9]{1,})/?$\";s:53:\"index.php?lang=en&post_type=project&paged=$matches[1]\";s:19:\"(fr)/experiences/?$\";s:47:\"index.php?lang=$matches[1]&post_type=experience\";s:14:\"experiences/?$\";s:38:\"index.php?lang=en&post_type=experience\";s:49:\"(fr)/experiences/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&post_type=experience&feed=$matches[2]\";s:44:\"experiences/feed/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?lang=en&post_type=experience&feed=$matches[1]\";s:44:\"(fr)/experiences/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&post_type=experience&feed=$matches[2]\";s:39:\"experiences/(feed|rdf|rss|rss2|atom)/?$\";s:55:\"index.php?lang=en&post_type=experience&feed=$matches[1]\";s:36:\"(fr)/experiences/page/([0-9]{1,})/?$\";s:65:\"index.php?lang=$matches[1]&post_type=experience&paged=$matches[2]\";s:31:\"experiences/page/([0-9]{1,})/?$\";s:56:\"index.php?lang=en&post_type=experience&paged=$matches[1]\";s:52:\"(fr)/category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:47:\"(fr)/category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:69:\"index.php?lang=$matches[1]&category_name=$matches[2]&feed=$matches[3]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:28:\"(fr)/category/(.+?)/embed/?$\";s:63:\"index.php?lang=$matches[1]&category_name=$matches[2]&embed=true\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:40:\"(fr)/category/(.+?)/page/?([0-9]{1,})/?$\";s:70:\"index.php?lang=$matches[1]&category_name=$matches[2]&paged=$matches[3]\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:22:\"(fr)/category/(.+?)/?$\";s:52:\"index.php?lang=$matches[1]&category_name=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:49:\"(fr)/tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:44:\"(fr)/tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&tag=$matches[2]&feed=$matches[3]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:25:\"(fr)/tag/([^/]+)/embed/?$\";s:53:\"index.php?lang=$matches[1]&tag=$matches[2]&embed=true\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:37:\"(fr)/tag/([^/]+)/page/?([0-9]{1,})/?$\";s:60:\"index.php?lang=$matches[1]&tag=$matches[2]&paged=$matches[3]\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:19:\"(fr)/tag/([^/]+)/?$\";s:42:\"index.php?lang=$matches[1]&tag=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:50:\"(fr)/type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&post_format=$matches[1]&feed=$matches[2]\";s:45:\"(fr)/type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&post_format=$matches[2]&feed=$matches[3]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&post_format=$matches[1]&feed=$matches[2]\";s:26:\"(fr)/type/([^/]+)/embed/?$\";s:61:\"index.php?lang=$matches[1]&post_format=$matches[2]&embed=true\";s:21:\"type/([^/]+)/embed/?$\";s:52:\"index.php?lang=en&post_format=$matches[1]&embed=true\";s:38:\"(fr)/type/([^/]+)/page/?([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&post_format=$matches[2]&paged=$matches[3]\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:59:\"index.php?lang=en&post_format=$matches[1]&paged=$matches[2]\";s:20:\"(fr)/type/([^/]+)/?$\";s:50:\"index.php?lang=$matches[1]&post_format=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:41:\"index.php?lang=en&post_format=$matches[1]\";s:41:\"(fr)/profiles/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:36:\"profiles/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"(fr)/profiles/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:46:\"profiles/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"(fr)/profiles/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:66:\"profiles/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"(fr)/profiles/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:61:\"profiles/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"(fr)/profiles/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:61:\"profiles/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"(fr)/profiles/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:42:\"profiles/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"(fr)/profiles/([^/]+)/embed/?$\";s:57:\"index.php?lang=$matches[1]&profile=$matches[2]&embed=true\";s:25:\"profiles/([^/]+)/embed/?$\";s:40:\"index.php?profile=$matches[1]&embed=true\";s:34:\"(fr)/profiles/([^/]+)/trackback/?$\";s:51:\"index.php?lang=$matches[1]&profile=$matches[2]&tb=1\";s:29:\"profiles/([^/]+)/trackback/?$\";s:34:\"index.php?profile=$matches[1]&tb=1\";s:54:\"(fr)/profiles/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&profile=$matches[2]&feed=$matches[3]\";s:49:\"profiles/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?profile=$matches[1]&feed=$matches[2]\";s:49:\"(fr)/profiles/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&profile=$matches[2]&feed=$matches[3]\";s:44:\"profiles/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?profile=$matches[1]&feed=$matches[2]\";s:42:\"(fr)/profiles/([^/]+)/page/?([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&profile=$matches[2]&paged=$matches[3]\";s:37:\"profiles/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?profile=$matches[1]&paged=$matches[2]\";s:49:\"(fr)/profiles/([^/]+)/comment-page-([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&profile=$matches[2]&cpage=$matches[3]\";s:44:\"profiles/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?profile=$matches[1]&cpage=$matches[2]\";s:38:\"(fr)/profiles/([^/]+)(?:/([0-9]+))?/?$\";s:63:\"index.php?lang=$matches[1]&profile=$matches[2]&page=$matches[3]\";s:33:\"profiles/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?profile=$matches[1]&page=$matches[2]\";s:30:\"(fr)/profiles/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:25:\"profiles/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"(fr)/profiles/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:35:\"profiles/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"(fr)/profiles/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:55:\"profiles/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"(fr)/profiles/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:50:\"profiles/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"(fr)/profiles/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:50:\"profiles/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"(fr)/profiles/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:31:\"profiles/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:45:\"(fr)/technologies/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:40:\"technologies/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:55:\"(fr)/technologies/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:50:\"technologies/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:75:\"(fr)/technologies/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:70:\"technologies/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"(fr)/technologies/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:65:\"technologies/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:70:\"(fr)/technologies/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:65:\"technologies/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:51:\"(fr)/technologies/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:46:\"technologies/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:34:\"(fr)/technologies/([^/]+)/embed/?$\";s:61:\"index.php?lang=$matches[1]&technologie=$matches[2]&embed=true\";s:29:\"technologies/([^/]+)/embed/?$\";s:44:\"index.php?technologie=$matches[1]&embed=true\";s:38:\"(fr)/technologies/([^/]+)/trackback/?$\";s:55:\"index.php?lang=$matches[1]&technologie=$matches[2]&tb=1\";s:33:\"technologies/([^/]+)/trackback/?$\";s:38:\"index.php?technologie=$matches[1]&tb=1\";s:58:\"(fr)/technologies/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&technologie=$matches[2]&feed=$matches[3]\";s:53:\"technologies/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?technologie=$matches[1]&feed=$matches[2]\";s:53:\"(fr)/technologies/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&technologie=$matches[2]&feed=$matches[3]\";s:48:\"technologies/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?technologie=$matches[1]&feed=$matches[2]\";s:46:\"(fr)/technologies/([^/]+)/page/?([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&technologie=$matches[2]&paged=$matches[3]\";s:41:\"technologies/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?technologie=$matches[1]&paged=$matches[2]\";s:53:\"(fr)/technologies/([^/]+)/comment-page-([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&technologie=$matches[2]&cpage=$matches[3]\";s:48:\"technologies/([^/]+)/comment-page-([0-9]{1,})/?$\";s:51:\"index.php?technologie=$matches[1]&cpage=$matches[2]\";s:42:\"(fr)/technologies/([^/]+)(?:/([0-9]+))?/?$\";s:67:\"index.php?lang=$matches[1]&technologie=$matches[2]&page=$matches[3]\";s:37:\"technologies/([^/]+)(?:/([0-9]+))?/?$\";s:50:\"index.php?technologie=$matches[1]&page=$matches[2]\";s:34:\"(fr)/technologies/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:29:\"technologies/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:44:\"(fr)/technologies/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:39:\"technologies/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:64:\"(fr)/technologies/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:59:\"technologies/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"(fr)/technologies/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:54:\"technologies/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:59:\"(fr)/technologies/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:54:\"technologies/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:40:\"(fr)/technologies/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:35:\"technologies/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:41:\"(fr)/projects/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:36:\"projects/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:51:\"(fr)/projects/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:46:\"projects/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:71:\"(fr)/projects/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:66:\"projects/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"(fr)/projects/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:61:\"projects/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:66:\"(fr)/projects/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:61:\"projects/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:47:\"(fr)/projects/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:42:\"projects/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:30:\"(fr)/projects/([^/]+)/embed/?$\";s:57:\"index.php?lang=$matches[1]&project=$matches[2]&embed=true\";s:25:\"projects/([^/]+)/embed/?$\";s:40:\"index.php?project=$matches[1]&embed=true\";s:34:\"(fr)/projects/([^/]+)/trackback/?$\";s:51:\"index.php?lang=$matches[1]&project=$matches[2]&tb=1\";s:29:\"projects/([^/]+)/trackback/?$\";s:34:\"index.php?project=$matches[1]&tb=1\";s:54:\"(fr)/projects/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&feed=$matches[3]\";s:49:\"projects/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:49:\"(fr)/projects/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&feed=$matches[3]\";s:44:\"projects/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?project=$matches[1]&feed=$matches[2]\";s:42:\"(fr)/projects/([^/]+)/page/?([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&project=$matches[2]&paged=$matches[3]\";s:37:\"projects/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&paged=$matches[2]\";s:49:\"(fr)/projects/([^/]+)/comment-page-([0-9]{1,})/?$\";s:64:\"index.php?lang=$matches[1]&project=$matches[2]&cpage=$matches[3]\";s:44:\"projects/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?project=$matches[1]&cpage=$matches[2]\";s:38:\"(fr)/projects/([^/]+)(?:/([0-9]+))?/?$\";s:63:\"index.php?lang=$matches[1]&project=$matches[2]&page=$matches[3]\";s:33:\"projects/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?project=$matches[1]&page=$matches[2]\";s:30:\"(fr)/projects/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:25:\"projects/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:40:\"(fr)/projects/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:35:\"projects/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:60:\"(fr)/projects/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:55:\"projects/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"(fr)/projects/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:50:\"projects/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:55:\"(fr)/projects/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:50:\"projects/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:36:\"(fr)/projects/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:31:\"projects/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:44:\"(fr)/experiences/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:39:\"experiences/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:54:\"(fr)/experiences/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:49:\"experiences/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:74:\"(fr)/experiences/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:69:\"experiences/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"(fr)/experiences/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:64:\"experiences/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:69:\"(fr)/experiences/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:64:\"experiences/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:50:\"(fr)/experiences/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:45:\"experiences/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"(fr)/experiences/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&experience=$matches[2]&embed=true\";s:28:\"experiences/([^/]+)/embed/?$\";s:43:\"index.php?experience=$matches[1]&embed=true\";s:37:\"(fr)/experiences/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&experience=$matches[2]&tb=1\";s:32:\"experiences/([^/]+)/trackback/?$\";s:37:\"index.php?experience=$matches[1]&tb=1\";s:57:\"(fr)/experiences/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&experience=$matches[2]&feed=$matches[3]\";s:52:\"experiences/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?experience=$matches[1]&feed=$matches[2]\";s:52:\"(fr)/experiences/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&experience=$matches[2]&feed=$matches[3]\";s:47:\"experiences/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?experience=$matches[1]&feed=$matches[2]\";s:45:\"(fr)/experiences/([^/]+)/page/?([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&experience=$matches[2]&paged=$matches[3]\";s:40:\"experiences/([^/]+)/page/?([0-9]{1,})/?$\";s:50:\"index.php?experience=$matches[1]&paged=$matches[2]\";s:52:\"(fr)/experiences/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&experience=$matches[2]&cpage=$matches[3]\";s:47:\"experiences/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?experience=$matches[1]&cpage=$matches[2]\";s:41:\"(fr)/experiences/([^/]+)(?:/([0-9]+))?/?$\";s:66:\"index.php?lang=$matches[1]&experience=$matches[2]&page=$matches[3]\";s:36:\"experiences/([^/]+)(?:/([0-9]+))?/?$\";s:49:\"index.php?experience=$matches[1]&page=$matches[2]\";s:33:\"(fr)/experiences/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:28:\"experiences/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"(fr)/experiences/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:38:\"experiences/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"(fr)/experiences/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:58:\"experiences/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"(fr)/experiences/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:53:\"experiences/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"(fr)/experiences/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:53:\"experiences/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"(fr)/experiences/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:34:\"experiences/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:37:\"(fr)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?lang=$matches[1]&&feed=$matches[2]\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:35:\"index.php?lang=en&&feed=$matches[1]\";s:32:\"(fr)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?lang=$matches[1]&&feed=$matches[2]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:35:\"index.php?lang=en&&feed=$matches[1]\";s:13:\"(fr)/embed/?$\";s:38:\"index.php?lang=$matches[1]&&embed=true\";s:8:\"embed/?$\";s:29:\"index.php?lang=en&&embed=true\";s:25:\"(fr)/page/?([0-9]{1,})/?$\";s:45:\"index.php?lang=$matches[1]&&paged=$matches[2]\";s:20:\"page/?([0-9]{1,})/?$\";s:36:\"index.php?lang=en&&paged=$matches[1]\";s:7:\"(fr)/?$\";s:26:\"index.php?lang=$matches[1]\";s:46:\"(fr)/comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?lang=en&&feed=$matches[1]&withcomments=1\";s:41:\"(fr)/comments/(feed|rdf|rss|rss2|atom)/?$\";s:59:\"index.php?lang=$matches[1]&&feed=$matches[2]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?lang=en&&feed=$matches[1]&withcomments=1\";s:22:\"(fr)/comments/embed/?$\";s:38:\"index.php?lang=$matches[1]&&embed=true\";s:17:\"comments/embed/?$\";s:29:\"index.php?lang=en&&embed=true\";s:49:\"(fr)/search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?lang=en&s=$matches[1]&feed=$matches[2]\";s:44:\"(fr)/search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:57:\"index.php?lang=$matches[1]&s=$matches[2]&feed=$matches[3]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:48:\"index.php?lang=en&s=$matches[1]&feed=$matches[2]\";s:25:\"(fr)/search/(.+)/embed/?$\";s:51:\"index.php?lang=$matches[1]&s=$matches[2]&embed=true\";s:20:\"search/(.+)/embed/?$\";s:42:\"index.php?lang=en&s=$matches[1]&embed=true\";s:37:\"(fr)/search/(.+)/page/?([0-9]{1,})/?$\";s:58:\"index.php?lang=$matches[1]&s=$matches[2]&paged=$matches[3]\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:49:\"index.php?lang=en&s=$matches[1]&paged=$matches[2]\";s:19:\"(fr)/search/(.+)/?$\";s:40:\"index.php?lang=$matches[1]&s=$matches[2]\";s:14:\"search/(.+)/?$\";s:31:\"index.php?lang=en&s=$matches[1]\";s:52:\"(fr)/author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&author_name=$matches[1]&feed=$matches[2]\";s:47:\"(fr)/author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:67:\"index.php?lang=$matches[1]&author_name=$matches[2]&feed=$matches[3]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:58:\"index.php?lang=en&author_name=$matches[1]&feed=$matches[2]\";s:28:\"(fr)/author/([^/]+)/embed/?$\";s:61:\"index.php?lang=$matches[1]&author_name=$matches[2]&embed=true\";s:23:\"author/([^/]+)/embed/?$\";s:52:\"index.php?lang=en&author_name=$matches[1]&embed=true\";s:40:\"(fr)/author/([^/]+)/page/?([0-9]{1,})/?$\";s:68:\"index.php?lang=$matches[1]&author_name=$matches[2]&paged=$matches[3]\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:59:\"index.php?lang=en&author_name=$matches[1]&paged=$matches[2]\";s:22:\"(fr)/author/([^/]+)/?$\";s:50:\"index.php?lang=$matches[1]&author_name=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:41:\"index.php?lang=en&author_name=$matches[1]\";s:74:\"(fr)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:88:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:69:\"(fr)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&feed=$matches[5]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:88:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:50:\"(fr)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:91:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&embed=true\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:82:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:62:\"(fr)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:98:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]&paged=$matches[5]\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:89:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:44:\"(fr)/([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:80:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&day=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:71:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:61:\"(fr)/([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:72:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:56:\"(fr)/([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:81:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&feed=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:72:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:37:\"(fr)/([0-9]{4})/([0-9]{1,2})/embed/?$\";s:75:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&embed=true\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:66:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&embed=true\";s:49:\"(fr)/([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:82:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]&paged=$matches[4]\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:73:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:31:\"(fr)/([0-9]{4})/([0-9]{1,2})/?$\";s:64:\"index.php?lang=$matches[1]&year=$matches[2]&monthnum=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:55:\"index.php?lang=en&year=$matches[1]&monthnum=$matches[2]\";s:48:\"(fr)/([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?lang=en&year=$matches[1]&feed=$matches[2]\";s:43:\"(fr)/([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&year=$matches[2]&feed=$matches[3]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:51:\"index.php?lang=en&year=$matches[1]&feed=$matches[2]\";s:24:\"(fr)/([0-9]{4})/embed/?$\";s:54:\"index.php?lang=$matches[1]&year=$matches[2]&embed=true\";s:19:\"([0-9]{4})/embed/?$\";s:45:\"index.php?lang=en&year=$matches[1]&embed=true\";s:36:\"(fr)/([0-9]{4})/page/?([0-9]{1,})/?$\";s:61:\"index.php?lang=$matches[1]&year=$matches[2]&paged=$matches[3]\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:52:\"index.php?lang=en&year=$matches[1]&paged=$matches[2]\";s:18:\"(fr)/([0-9]{4})/?$\";s:43:\"index.php?lang=$matches[1]&year=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:34:\"index.php?lang=en&year=$matches[1]\";s:32:\"(fr)/.?.+?/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"(fr)/.?.+?/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"(fr)/.?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(fr)/.?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(fr)/.?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"(fr)/.?.+?/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"(fr)/(.?.+?)/embed/?$\";s:58:\"index.php?lang=$matches[1]&pagename=$matches[2]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:25:\"(fr)/(.?.+?)/trackback/?$\";s:52:\"index.php?lang=$matches[1]&pagename=$matches[2]&tb=1\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:45:\"(fr)/(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:40:\"(fr)/(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&feed=$matches[3]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:33:\"(fr)/(.?.+?)/page/?([0-9]{1,})/?$\";s:65:\"index.php?lang=$matches[1]&pagename=$matches[2]&paged=$matches[3]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:40:\"(fr)/(.?.+?)/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?lang=$matches[1]&pagename=$matches[2]&cpage=$matches[3]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:29:\"(fr)/(.?.+?)(?:/([0-9]+))?/?$\";s:64:\"index.php?lang=$matches[1]&pagename=$matches[2]&page=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:32:\"(fr)/[^/]+/attachment/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"(fr)/[^/]+/attachment/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"(fr)/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(fr)/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"(fr)/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"(fr)/[^/]+/attachment/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"(fr)/([^/]+)/embed/?$\";s:54:\"index.php?lang=$matches[1]&name=$matches[2]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:25:\"(fr)/([^/]+)/trackback/?$\";s:48:\"index.php?lang=$matches[1]&name=$matches[2]&tb=1\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:45:\"(fr)/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:40:\"(fr)/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:60:\"index.php?lang=$matches[1]&name=$matches[2]&feed=$matches[3]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:33:\"(fr)/([^/]+)/page/?([0-9]{1,})/?$\";s:61:\"index.php?lang=$matches[1]&name=$matches[2]&paged=$matches[3]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:40:\"(fr)/([^/]+)/comment-page-([0-9]{1,})/?$\";s:61:\"index.php?lang=$matches[1]&name=$matches[2]&cpage=$matches[3]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:29:\"(fr)/([^/]+)(?:/([0-9]+))?/?$\";s:60:\"index.php?lang=$matches[1]&name=$matches[2]&page=$matches[3]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:21:\"(fr)/[^/]+/([^/]+)/?$\";s:49:\"index.php?lang=$matches[1]&attachment=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"(fr)/[^/]+/([^/]+)/trackback/?$\";s:54:\"index.php?lang=$matches[1]&attachment=$matches[2]&tb=1\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"(fr)/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"(fr)/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:66:\"index.php?lang=$matches[1]&attachment=$matches[2]&feed=$matches[3]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"(fr)/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:67:\"index.php?lang=$matches[1]&attachment=$matches[2]&cpage=$matches[3]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"(fr)/[^/]+/([^/]+)/embed/?$\";s:60:\"index.php?lang=$matches[1]&attachment=$matches[2]&embed=true\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(384, '_site_transient_timeout_php_check_133413fa91fc050a1c5770fd6e67cbaa', '1715857160', 'no'),
(385, '_site_transient_php_check_133413fa91fc050a1c5770fd6e67cbaa', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:3:\"7.0\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(388, '_site_transient_timeout_browser_a16ddaab909d2cf27fce353f26dd2ff2', '1715857186', 'no'),
(389, '_site_transient_browser_a16ddaab909d2cf27fce353f26dd2ff2', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:9:\"124.0.0.0\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(393, 'wordfence_ls_version', '1.1.11', 'yes'),
(394, 'wfls_last_role_change', '1715252414', 'no'),
(395, 'wordfence_version', '7.11.5', 'yes'),
(396, 'wordfence_case', '1', 'yes'),
(397, 'wordfence_installed', '1', 'yes'),
(398, 'wordfenceActivated', '1', 'yes'),
(399, 'wf_plugin_act_error', '', 'yes'),
(411, 'WPLANG', '', 'yes'),
(412, 'new_admin_email', 'elachhabjawad@gmail.com', 'yes'),
(413, 'whl_page', 'bo_elachhab', 'yes'),
(414, 'whl_redirect_admin', '404', 'yes'),
(423, 'rcpo_db_migration', '0', 'yes'),
(424, 'rcpo_db_version', '1.3.84', 'yes'),
(425, 'real-custom-post-order_language_pack_error-real-custom-post-order-expire', '1715265443', 'yes'),
(426, 'real-custom-post-order_language_pack_error-real-custom-post-order', '', 'yes'),
(427, 'real_utils-transients', '{\"RCPO\":{\"raa\":true,\"nr\":1717846660}}', 'no'),
(429, 'notice-devowl-rate-limit-expire', '1715265460', 'yes'),
(430, 'notice-devowl-rate-limit', '', 'yes'),
(431, 'rcpo-post-active-post', '1', 'yes'),
(432, 'rcpo-post-active-page', '1', 'yes'),
(445, 'rcpo-post-active-technologie', '', 'yes'),
(448, 'rcpo-post-active-acf-ui-options-page', '', 'yes'),
(450, 'rcpo-post-active-acf-field-group', '', 'yes'),
(452, 'rcpo_db_previous_version', 'a:1:{i:0;s:6:\"1.3.84\";}', 'yes'),
(460, 'current_theme', 'PortFolio', 'yes'),
(461, 'theme_mods_portfolio', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(462, 'theme_switched', '', 'yes'),
(663, '_transient_timeout_acf_plugin_updates', '1715641807', 'no'),
(664, '_transient_acf_plugin_updates', 'a:5:{s:7:\"plugins\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";a:11:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"6.2.9\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.5.3\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:8:\"requires\";s:3:\"5.8\";s:12:\"requires_php\";s:3:\"7.0\";s:12:\"release_date\";s:8:\"20240408\";}}s:9:\"no_update\";a:0:{}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"6.2.0\";}}', 'no'),
(746, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.5.3.zip\";s:6:\"locale\";s:5:\"en_US\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:59:\"https://downloads.wordpress.org/release/wordpress-6.5.3.zip\";s:10:\"no_content\";s:70:\"https://downloads.wordpress.org/release/wordpress-6.5.3-no-content.zip\";s:11:\"new_bundled\";s:71:\"https://downloads.wordpress.org/release/wordpress-6.5.3-new-bundled.zip\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"6.5.3\";s:7:\"version\";s:5:\"6.5.3\";s:11:\"php_version\";s:5:\"7.0.0\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"6.4\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1715522226;s:15:\"version_checked\";s:5:\"6.5.3\";s:12:\"translations\";a:0:{}}', 'no'),
(747, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1715522226;s:7:\"checked\";a:1:{s:9:\"portfolio\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:0:{}s:12:\"translations\";a:0:{}}', 'no');
INSERT INTO `portfolio_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(748, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1715522227;s:8:\"response\";a:1:{s:34:\"advanced-custom-fields-pro/acf.php\";O:8:\"stdClass\":11:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:34:\"advanced-custom-fields-pro/acf.php\";s:11:\"new_version\";s:5:\"6.2.9\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:5:\"6.5.3\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}s:8:\"requires\";s:3:\"5.8\";s:12:\"requires_php\";s:3:\"7.0\";s:12:\"release_date\";s:8:\"20240408\";}}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:16:\"post-types-order\";s:8:\"language\";s:5:\"en_GB\";s:7:\"version\";s:5:\"2.2.1\";s:7:\"updated\";s:19:\"2020-03-03 13:34:51\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/translation/plugin/post-types-order/2.2.1/en_GB.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:16:\"post-types-order\";s:8:\"language\";s:5:\"fr_FR\";s:7:\"version\";s:5:\"2.2.1\";s:7:\"updated\";s:19:\"2021-12-23 17:01:52\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/translation/plugin/post-types-order/2.2.1/fr_FR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:6:{s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:48:\"w.org/plugins/jwt-authentication-for-wp-rest-api\";s:4:\"slug\";s:34:\"jwt-authentication-for-wp-rest-api\";s:6:\"plugin\";s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";s:11:\"new_version\";s:5:\"1.3.4\";s:3:\"url\";s:65:\"https://wordpress.org/plugins/jwt-authentication-for-wp-rest-api/\";s:7:\"package\";s:83:\"https://downloads.wordpress.org/plugin/jwt-authentication-for-wp-rest-api.1.3.4.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:79:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/icon.svg?rev=2787935\";s:3:\"svg\";s:79:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/icon.svg?rev=2787935\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:89:\"https://ps.w.org/jwt-authentication-for-wp-rest-api/assets/banner-772x250.jpg?rev=2787935\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.2\";}s:21:\"polylang/polylang.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:22:\"w.org/plugins/polylang\";s:4:\"slug\";s:8:\"polylang\";s:6:\"plugin\";s:21:\"polylang/polylang.php\";s:11:\"new_version\";s:5:\"3.6.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/polylang/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/polylang.3.6.1.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:61:\"https://ps.w.org/polylang/assets/icon-256x256.png?rev=1331499\";s:2:\"1x\";s:61:\"https://ps.w.org/polylang/assets/icon-128x128.png?rev=1331499\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/polylang/assets/banner-1544x500.png?rev=1405299\";s:2:\"1x\";s:63:\"https://ps.w.org/polylang/assets/banner-772x250.png?rev=1405299\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"6.2\";}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:5:\"2.2.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/post-types-order.2.2.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=2870026\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"2.8\";}s:25:\"sucuri-scanner/sucuri.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/sucuri-scanner\";s:4:\"slug\";s:14:\"sucuri-scanner\";s:6:\"plugin\";s:25:\"sucuri-scanner/sucuri.php\";s:11:\"new_version\";s:6:\"1.8.44\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/sucuri-scanner/\";s:7:\"package\";s:64:\"https://downloads.wordpress.org/plugin/sucuri-scanner.1.8.44.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/sucuri-scanner/assets/icon-256x256.png?rev=2875755\";s:2:\"1x\";s:67:\"https://ps.w.org/sucuri-scanner/assets/icon-128x128.png?rev=2875755\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/sucuri-scanner/assets/banner-772x250.png?rev=2875755\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.6\";}s:23:\"wordfence/wordfence.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:23:\"w.org/plugins/wordfence\";s:4:\"slug\";s:9:\"wordfence\";s:6:\"plugin\";s:23:\"wordfence/wordfence.php\";s:11:\"new_version\";s:6:\"7.11.5\";s:3:\"url\";s:40:\"https://wordpress.org/plugins/wordfence/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/wordfence.7.11.5.zip\";s:5:\"icons\";a:2:{s:2:\"1x\";s:54:\"https://ps.w.org/wordfence/assets/icon.svg?rev=2070865\";s:3:\"svg\";s:54:\"https://ps.w.org/wordfence/assets/icon.svg?rev=2070865\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wordfence/assets/banner-1544x500.jpg?rev=2124102\";s:2:\"1x\";s:64:\"https://ps.w.org/wordfence/assets/banner-772x250.jpg?rev=2124102\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"3.9\";}s:33:\"wps-hide-login/wps-hide-login.php\";O:8:\"stdClass\":10:{s:2:\"id\";s:28:\"w.org/plugins/wps-hide-login\";s:4:\"slug\";s:14:\"wps-hide-login\";s:6:\"plugin\";s:33:\"wps-hide-login/wps-hide-login.php\";s:11:\"new_version\";s:8:\"1.9.15.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/wps-hide-login/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/wps-hide-login.1.9.15.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-256x256.png?rev=1820667\";s:2:\"1x\";s:67:\"https://ps.w.org/wps-hide-login/assets/icon-128x128.png?rev=1820667\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/wps-hide-login/assets/banner-1544x500.jpg?rev=1820667\";s:2:\"1x\";s:69:\"https://ps.w.org/wps-hide-login/assets/banner-772x250.jpg?rev=1820667\";}s:11:\"banners_rtl\";a:0:{}s:8:\"requires\";s:3:\"4.1\";}}s:7:\"checked\";a:7:{s:34:\"advanced-custom-fields-pro/acf.php\";s:5:\"6.2.0\";s:47:\"jwt-authentication-for-wp-rest-api/jwt-auth.php\";s:5:\"1.3.4\";s:21:\"polylang/polylang.php\";s:5:\"3.6.1\";s:37:\"post-types-order/post-types-order.php\";s:5:\"2.2.1\";s:25:\"sucuri-scanner/sucuri.php\";s:6:\"1.8.44\";s:23:\"wordfence/wordfence.php\";s:6:\"7.11.5\";s:33:\"wps-hide-login/wps-hide-login.php\";s:8:\"1.9.15.2\";}}', 'no'),
(750, '_site_transient_timeout_community-events-1aecf33ab8525ff212ebdffbb438372e', '1715565778', 'no'),
(751, '_site_transient_community-events-1aecf33ab8525ff212ebdffbb438372e', 'a:4:{s:9:\"sandboxed\";b:0;s:5:\"error\";N;s:8:\"location\";a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}s:6:\"events\";a:0:{}}', 'no'),
(786, '_transient_timeout_sucuriscan_online_users', '1715527935', 'no'),
(787, '_transient_sucuriscan_online_users', 'a:1:{i:0;a:6:{s:7:\"user_id\";i:1;s:10:\"user_login\";s:5:\"admin\";s:10:\"user_email\";s:23:\"elachhabjawad@gmail.com\";s:15:\"user_registered\";s:19:\"2024-04-30 08:50:10\";s:13:\"last_activity\";i:1715526135;s:11:\"remote_addr\";s:9:\"127.0.0.1\";}}', 'no'),
(788, '_site_transient_timeout_theme_roots', '1715527951', 'no'),
(789, '_site_transient_theme_roots', 'a:1:{s:9:\"portfolio\";s:7:\"/themes\";}', 'no'),
(839, '_transient_pll_languages_list', 'a:2:{i:0;a:22:{s:4:\"name\";s:7:\"English\";s:4:\"slug\";s:2:\"en\";s:10:\"term_group\";i:0;s:7:\"term_id\";i:2;s:6:\"locale\";s:5:\"en_GB\";s:6:\"is_rtl\";i:0;s:3:\"w3c\";s:5:\"en-GB\";s:8:\"facebook\";s:5:\"en_GB\";s:8:\"home_url\";s:29:\"http://dev.portfolio-api.com/\";s:10:\"search_url\";s:29:\"http://dev.portfolio-api.com/\";s:4:\"host\";N;s:13:\"page_on_front\";i:0;s:14:\"page_for_posts\";i:0;s:9:\"flag_code\";s:2:\"gb\";s:8:\"flag_url\";s:69:\"http://dev.portfolio-api.com/wp-content/plugins/polylang/flags/gb.png\";s:4:\"flag\";s:636:\"<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAt1BMVEWSmb66z+18msdig8La3u+tYX9IaLc7W7BagbmcUW+kqMr/q6n+//+hsNv/lIr/jIGMnNLJyOP9/fyQttT/wb3/////aWn+YWF5kNT0oqz0i4ueqtIZNJjhvt/8gn//WVr/6+rN1+o9RKZwgcMPJpX/VFT9UEn+RUX8Ozv2Ly+FGzdYZrfU1e/8LS/lQkG/mbVUX60AE231hHtcdMb0mp3qYFTFwNu3w9prcqSURGNDaaIUMX5FNW5wYt7AAAAAjklEQVR4AR3HNUJEMQCGwf+L8RR36ajR+1+CEuvRdd8kK9MNAiRQNgJmVDAt1yM6kSzYVJUsPNssAk5N7ZFKjVNFAY4co6TAOI+kyQm+LFUEBEKKzuWUNB7rSH/rSnvOulOGk+QlXTBqMIrfYX4tSe2nP3iRa/KNK7uTmWJ5a9+erZ3d+18od4ytiZdvZyuKWy8o3UpTVAAAAABJRU5ErkJggg==\" alt=\"English\" width=\"16\" height=\"11\" style=\"width: 16px; height: 11px;\" />\";s:15:\"custom_flag_url\";s:0:\"\";s:11:\"custom_flag\";s:0:\"\";s:6:\"active\";b:1;s:9:\"fallbacks\";a:0:{}s:10:\"is_default\";b:1;s:10:\"term_props\";a:2:{s:8:\"language\";a:3:{s:7:\"term_id\";i:2;s:16:\"term_taxonomy_id\";i:2;s:5:\"count\";i:42;}s:13:\"term_language\";a:3:{s:7:\"term_id\";i:3;s:16:\"term_taxonomy_id\";i:3;s:5:\"count\";i:1;}}}i:1;a:22:{s:4:\"name\";s:9:\"Français\";s:4:\"slug\";s:2:\"fr\";s:10:\"term_group\";i:0;s:7:\"term_id\";i:5;s:6:\"locale\";s:5:\"fr_FR\";s:6:\"is_rtl\";i:0;s:3:\"w3c\";s:5:\"fr-FR\";s:8:\"facebook\";s:5:\"fr_FR\";s:8:\"home_url\";s:32:\"http://dev.portfolio-api.com/fr/\";s:10:\"search_url\";s:32:\"http://dev.portfolio-api.com/fr/\";s:4:\"host\";N;s:13:\"page_on_front\";i:0;s:14:\"page_for_posts\";i:0;s:9:\"flag_code\";s:2:\"fr\";s:8:\"flag_url\";s:69:\"http://dev.portfolio-api.com/wp-content/plugins/polylang/flags/fr.png\";s:4:\"flag\";s:474:\"<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAALCAMAAABBPP0LAAAAbFBMVEVzldTg4ODS0tLxDwDtAwDjAADD0uz39/fy8vL3k4nzgna4yOixwuXu7u7s6+zn5+fyd2rvcGPtZljYAABrjNCpvOHrWkxegsqfs93NAADpUUFRd8THAABBa7wnVbERRKa8vLyxsLCoqKigoKClCvcsAAAAXklEQVR4AS3JxUEAQQAEwZo13Mk/R9w5/7UERJCIGIgj5qfRJZEpPyNfCgJTjMR1eRRnJiExFJz5Mf1PokWr/UztIjRGQ3V486u0HO55m634U6dMcf0RNPfkVCTvKjO16xHA8miowAAAAABJRU5ErkJggg==\" alt=\"Français\" width=\"16\" height=\"11\" style=\"width: 16px; height: 11px;\" />\";s:15:\"custom_flag_url\";s:0:\"\";s:11:\"custom_flag\";s:0:\"\";s:6:\"active\";b:1;s:9:\"fallbacks\";a:0:{}s:10:\"is_default\";b:0;s:10:\"term_props\";a:2:{s:8:\"language\";a:3:{s:7:\"term_id\";i:5;s:16:\"term_taxonomy_id\";i:5;s:5:\"count\";i:0;}s:13:\"term_language\";a:3:{s:7:\"term_id\";i:6;s:16:\"term_taxonomy_id\";i:6;s:5:\"count\";i:1;}}}}', 'yes');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_postmeta`
--

CREATE TABLE `portfolio_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_postmeta`
--

INSERT INTO `portfolio_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(13, 11, '_edit_last', '1'),
(14, 11, '_edit_lock', '1714468352:1'),
(15, 11, '_wp_page_template', 'default'),
(18, 14, '_wp_attached_file', '2024/04/top-10-features-of-es6.jpg.jpg'),
(19, 14, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1400;s:6:\"height\";i:788;s:4:\"file\";s:38:\"2024/04/top-10-features-of-es6.jpg.jpg\";s:8:\"filesize\";i:89598;s:5:\"sizes\";a:4:{s:6:\"medium\";a:5:{s:4:\"file\";s:38:\"top-10-features-of-es6.jpg-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:10939;}s:5:\"large\";a:5:{s:4:\"file\";s:39:\"top-10-features-of-es6.jpg-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:58606;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:38:\"top-10-features-of-es6.jpg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:7038;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:38:\"top-10-features-of-es6.jpg-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:38956;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(20, 11, '_thumbnail_id', '14'),
(22, 15, '_edit_last', '1'),
(23, 15, '_wp_page_template', 'default'),
(25, 15, '_edit_lock', '1714468219:1'),
(26, 17, '_wp_attached_file', '2024/04/http-request-methods.jpg'),
(27, 17, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1632;s:6:\"height\";i:865;s:4:\"file\";s:32:\"2024/04/http-request-methods.jpg\";s:8:\"filesize\";i:79562;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:32:\"http-request-methods-300x159.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:159;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:6947;}s:5:\"large\";a:5:{s:4:\"file\";s:33:\"http-request-methods-1024x543.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:543;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:37141;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:32:\"http-request-methods-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:5729;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:32:\"http-request-methods-768x407.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:407;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:25029;}s:9:\"1536x1536\";a:5:{s:4:\"file\";s:33:\"http-request-methods-1536x814.jpg\";s:5:\"width\";i:1536;s:6:\"height\";i:814;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:65777;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(28, 15, '_thumbnail_id', '17'),
(30, 18, '_edit_last', '1'),
(31, 18, '_edit_lock', '1714486819:1'),
(32, 18, '_wp_page_template', 'default'),
(34, 20, '_wp_attached_file', '2024/04/comment-uncoment-lines.gif'),
(35, 20, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:778;s:6:\"height\";i:531;s:4:\"file\";s:34:\"2024/04/comment-uncoment-lines.gif\";s:8:\"filesize\";i:238706;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:34:\"comment-uncoment-lines-300x205.gif\";s:5:\"width\";i:300;s:6:\"height\";i:205;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:17124;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:34:\"comment-uncoment-lines-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:8022;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:34:\"comment-uncoment-lines-768x524.gif\";s:5:\"width\";i:768;s:6:\"height\";i:524;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:38629;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(36, 21, '_wp_attached_file', '2024/04/format-document.gif'),
(37, 21, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1191;s:6:\"height\";i:635;s:4:\"file\";s:27:\"2024/04/format-document.gif\";s:8:\"filesize\";i:174449;s:5:\"sizes\";a:4:{s:6:\"medium\";a:5:{s:4:\"file\";s:27:\"format-document-300x160.gif\";s:5:\"width\";i:300;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:10315;}s:5:\"large\";a:5:{s:4:\"file\";s:28:\"format-document-1024x546.gif\";s:5:\"width\";i:1024;s:6:\"height\";i:546;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:38730;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:27:\"format-document-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:4200;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:27:\"format-document-768x409.gif\";s:5:\"width\";i:768;s:6:\"height\";i:409;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:29423;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(38, 22, '_wp_attached_file', '2024/04/multi-cursor-editing1.gif'),
(39, 22, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:660;s:6:\"height\";i:350;s:4:\"file\";s:33:\"2024/04/multi-cursor-editing1.gif\";s:8:\"filesize\";i:237286;s:5:\"sizes\";a:2:{s:6:\"medium\";a:5:{s:4:\"file\";s:33:\"multi-cursor-editing1-300x159.gif\";s:5:\"width\";i:300;s:6:\"height\";i:159;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:8916;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:33:\"multi-cursor-editing1-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:3754;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(40, 23, '_wp_attached_file', '2024/04/multi-cursor-editing2.gif'),
(41, 23, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:630;s:6:\"height\";i:350;s:4:\"file\";s:33:\"2024/04/multi-cursor-editing2.gif\";s:8:\"filesize\";i:266499;s:5:\"sizes\";a:2:{s:6:\"medium\";a:5:{s:4:\"file\";s:33:\"multi-cursor-editing2-300x167.gif\";s:5:\"width\";i:300;s:6:\"height\";i:167;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:11338;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:33:\"multi-cursor-editing2-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:7653;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 24, '_wp_attached_file', '2024/04/quick-open.gif'),
(43, 24, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1191;s:6:\"height\";i:635;s:4:\"file\";s:22:\"2024/04/quick-open.gif\";s:8:\"filesize\";i:487453;s:5:\"sizes\";a:4:{s:6:\"medium\";a:5:{s:4:\"file\";s:22:\"quick-open-300x160.gif\";s:5:\"width\";i:300;s:6:\"height\";i:160;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:16284;}s:5:\"large\";a:5:{s:4:\"file\";s:23:\"quick-open-1024x546.gif\";s:5:\"width\";i:1024;s:6:\"height\";i:546;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:72522;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:22:\"quick-open-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:8489;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:22:\"quick-open-768x409.gif\";s:5:\"width\";i:768;s:6:\"height\";i:409;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:55643;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 25, '_wp_attached_file', '2024/04/toggle-sideba.gif'),
(45, 25, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1575;s:6:\"height\";i:775;s:4:\"file\";s:25:\"2024/04/toggle-sideba.gif\";s:8:\"filesize\";i:722038;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:25:\"toggle-sideba-300x148.gif\";s:5:\"width\";i:300;s:6:\"height\";i:148;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:21697;}s:5:\"large\";a:5:{s:4:\"file\";s:26:\"toggle-sideba-1024x504.gif\";s:5:\"width\";i:1024;s:6:\"height\";i:504;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:141710;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:25:\"toggle-sideba-150x150.gif\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:14938;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:25:\"toggle-sideba-768x378.gif\";s:5:\"width\";i:768;s:6:\"height\";i:378;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:86859;}s:9:\"1536x1536\";a:5:{s:4:\"file\";s:26:\"toggle-sideba-1536x756.gif\";s:5:\"width\";i:1536;s:6:\"height\";i:756;s:9:\"mime-type\";s:9:\"image/gif\";s:8:\"filesize\";i:275318;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(46, 26, '_wp_attached_file', '2024/04/visual-studio-code.png'),
(47, 26, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:30:\"2024/04/visual-studio-code.png\";s:8:\"filesize\";i:59198;s:5:\"sizes\";a:5:{s:6:\"medium\";a:5:{s:4:\"file\";s:30:\"visual-studio-code-300x169.png\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:8239;}s:5:\"large\";a:5:{s:4:\"file\";s:31:\"visual-studio-code-1024x576.png\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:41233;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:30:\"visual-studio-code-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:6853;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:30:\"visual-studio-code-768x432.png\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:27990;}s:9:\"1536x1536\";a:5:{s:4:\"file\";s:31:\"visual-studio-code-1536x864.png\";s:5:\"width\";i:1536;s:6:\"height\";i:864;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:71195;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(48, 18, '_thumbnail_id', '26'),
(50, 29, '_edit_last', '1'),
(51, 29, '_edit_lock', '1715356425:1'),
(52, 40, '_edit_last', '1'),
(53, 40, '_edit_lock', '1714922160:1'),
(88, 62, '_edit_last', '1'),
(89, 62, '_edit_lock', '1715527338:1'),
(94, 62, 'startdate', '20240303'),
(95, 62, '_startdate', 'field_6635094313c6c'),
(96, 64, '_edit_last', '1'),
(97, 64, '_edit_lock', '1715527351:1'),
(99, 64, 'startdate', '20240503'),
(100, 64, '_startdate', 'field_6635094313c6c'),
(101, 65, '_edit_last', '1'),
(102, 65, '_edit_lock', '1715527435:1'),
(104, 65, 'startdate', '20240503'),
(105, 65, '_startdate', 'field_6635094313c6c'),
(106, 66, '_edit_last', '1'),
(107, 66, '_edit_lock', '1715527541:1'),
(109, 66, 'startdate', '20240503'),
(110, 66, '_startdate', 'field_6635094313c6c'),
(111, 67, '_edit_last', '1'),
(112, 67, '_edit_lock', '1715527542:1'),
(114, 67, 'startdate', '20240503'),
(115, 67, '_startdate', 'field_6635094313c6c'),
(116, 68, '_edit_last', '1'),
(117, 68, '_edit_lock', '1715527584:1'),
(119, 68, 'startdate', '20240503'),
(120, 68, '_startdate', 'field_6635094313c6c'),
(121, 69, '_edit_last', '1'),
(122, 69, '_edit_lock', '1715527627:1'),
(124, 69, 'startdate', '20240503'),
(125, 69, '_startdate', 'field_6635094313c6c'),
(126, 70, '_edit_last', '1'),
(127, 70, '_edit_lock', '1715527640:1'),
(129, 70, 'startdate', '20240503'),
(130, 70, '_startdate', 'field_6635094313c6c'),
(131, 71, '_edit_last', '1'),
(132, 71, '_edit_lock', '1715527651:1'),
(134, 71, 'startdate', '20240503'),
(135, 71, '_startdate', 'field_6635094313c6c'),
(136, 72, '_edit_last', '1'),
(137, 72, '_edit_lock', '1715527664:1'),
(139, 72, 'startdate', '20240503'),
(140, 72, '_startdate', 'field_6635094313c6c'),
(141, 73, '_edit_last', '1'),
(142, 73, '_edit_lock', '1715527759:1'),
(144, 73, 'startdate', '20240503'),
(145, 73, '_startdate', 'field_6635094313c6c'),
(146, 74, '_edit_last', '1'),
(147, 74, '_edit_lock', '1715527691:1'),
(149, 74, 'startdate', '20240503'),
(150, 74, '_startdate', 'field_6635094313c6c'),
(151, 75, '_edit_last', '1'),
(152, 75, '_edit_lock', '1715527881:1'),
(154, 75, 'startdate', '20240503'),
(155, 75, '_startdate', 'field_6635094313c6c'),
(156, 75, 'tech_start_date', '20190502'),
(157, 75, '_tech_start_date', 'field_6635094313c6c'),
(158, 74, 'tech_start_date', '20190502'),
(159, 74, '_tech_start_date', 'field_6635094313c6c'),
(160, 73, 'tech_start_date', '20190504'),
(161, 73, '_tech_start_date', 'field_6635094313c6c'),
(162, 72, 'tech_start_date', '20230501'),
(163, 72, '_tech_start_date', 'field_6635094313c6c'),
(164, 71, 'tech_start_date', '20220506'),
(165, 71, '_tech_start_date', 'field_6635094313c6c'),
(166, 70, 'tech_start_date', '20190501'),
(167, 70, '_tech_start_date', 'field_6635094313c6c'),
(168, 69, 'tech_start_date', '20190501'),
(169, 69, '_tech_start_date', 'field_6635094313c6c'),
(170, 67, 'tech_start_date', '20220502'),
(171, 67, '_tech_start_date', 'field_6635094313c6c'),
(172, 66, 'tech_start_date', '20220501'),
(173, 66, '_tech_start_date', 'field_6635094313c6c'),
(174, 65, 'tech_start_date', '20180501'),
(175, 65, '_tech_start_date', 'field_6635094313c6c'),
(176, 64, 'tech_start_date', '20180501'),
(177, 64, '_tech_start_date', 'field_6635094313c6c'),
(178, 62, 'tech_start_date', '20180501'),
(179, 62, '_tech_start_date', 'field_6635094313c6c'),
(180, 68, 'tech_start_date', '20230501'),
(181, 68, '_tech_start_date', 'field_6635094313c6c'),
(182, 79, '_edit_last', '1'),
(183, 79, '_edit_lock', '1715357098:1'),
(184, 80, '_wp_attached_file', '2024/05/fornet.jpg'),
(185, 80, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:200;s:6:\"height\";i:200;s:4:\"file\";s:18:\"2024/05/fornet.jpg\";s:8:\"filesize\";i:8857;s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:5:{s:4:\"file\";s:18:\"fornet-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:4422;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(186, 79, '_thumbnail_id', '80'),
(187, 79, 'experience_company_name', 'FORNET'),
(188, 79, '_experience_company_name', 'field_6635033f35d80'),
(189, 79, 'experience_company_url', 'https://www.fornetmaroc.com/'),
(190, 79, '_experience_company_url', 'field_6635037f35d81'),
(191, 79, 'experience_start_date', '20220314'),
(192, 79, '_experience_start_date', 'field_6635039635d82'),
(193, 79, 'experience_end_date', ''),
(194, 79, '_experience_end_date', 'field_663503b635d83'),
(195, 79, 'experience_technologies', 'a:15:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"132\";i:3;s:3:\"131\";i:4;s:2:\"65\";i:5;s:3:\"135\";i:6;s:2:\"69\";i:7;s:2:\"71\";i:8;s:2:\"72\";i:9;s:2:\"73\";i:10;s:2:\"74\";i:11;s:2:\"75\";i:12;s:3:\"138\";i:13;s:3:\"139\";i:14;s:3:\"137\";}'),
(196, 79, '_experience_technologies', 'field_663503d535d84'),
(197, 81, '_edit_last', '1'),
(198, 81, '_edit_lock', '1715357259:1'),
(199, 82, '_wp_attached_file', '2024/05/ulteos.jpg'),
(200, 82, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:200;s:6:\"height\";i:200;s:4:\"file\";s:18:\"2024/05/ulteos.jpg\";s:8:\"filesize\";i:3657;s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:5:{s:4:\"file\";s:18:\"ulteos-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:2383;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(201, 81, '_thumbnail_id', '82'),
(202, 81, 'experience_company_name', 'ULTEOS'),
(203, 81, '_experience_company_name', 'field_6635033f35d80'),
(204, 81, 'experience_company_url', 'https://gamma.app/docs/Information-Technology-Business-Expansion-9axvdsfzrvhjlvz?mode=doc'),
(205, 81, '_experience_company_url', 'field_6635037f35d81'),
(206, 81, 'experience_start_date', '20210321'),
(207, 81, '_experience_start_date', 'field_6635039635d82'),
(208, 81, 'experience_end_date', '20211010'),
(209, 81, '_experience_end_date', 'field_663503b635d83'),
(210, 81, 'experience_technologies', 'a:12:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"131\";i:3;s:2:\"65\";i:4;s:3:\"135\";i:5;s:2:\"69\";i:6;s:2:\"70\";i:7;s:2:\"71\";i:8;s:2:\"73\";i:9;s:2:\"74\";i:10;s:3:\"134\";i:11;s:3:\"133\";}'),
(211, 81, '_experience_technologies', 'field_663503d535d84'),
(212, 84, '_edit_last', '1'),
(213, 84, '_edit_lock', '1715357494:1'),
(214, 85, '_wp_attached_file', '2024/05/asic.jpg'),
(215, 85, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:186;s:6:\"height\";i:186;s:4:\"file\";s:16:\"2024/05/asic.jpg\";s:8:\"filesize\";i:4050;s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:5:{s:4:\"file\";s:16:\"asic-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:8:\"filesize\";i:4667;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(216, 84, '_thumbnail_id', '85'),
(217, 84, 'experience_company_name', 'ASIC'),
(218, 84, '_experience_company_name', 'field_6635033f35d80'),
(219, 84, 'experience_company_url', ''),
(220, 84, '_experience_company_url', 'field_6635037f35d81'),
(221, 84, 'experience_start_date', '20191014'),
(222, 84, '_experience_start_date', 'field_6635039635d82'),
(223, 84, 'experience_end_date', '20200615'),
(224, 84, '_experience_end_date', 'field_663503b635d83'),
(225, 84, 'experience_technologies', 'a:8:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"131\";i:3;s:2:\"65\";i:4;s:2:\"69\";i:5;s:2:\"73\";i:6;s:3:\"134\";i:7;s:3:\"133\";}'),
(226, 84, '_experience_technologies', 'field_663503d535d84'),
(227, 87, '_edit_last', '1'),
(228, 87, '_edit_lock', '1714826526:1'),
(229, 94, '_edit_last', '1'),
(230, 94, '_edit_lock', '1715526481:1'),
(231, 94, 'project_sections', ''),
(232, 94, '_project_sections', 'field_66361fdaecf6d'),
(233, 94, 'project_technologies', 'a:6:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"67\";i:4;s:2:\"74\";i:5;s:3:\"134\";}'),
(234, 94, '_project_technologies', 'field_66361e51b6c7a'),
(235, 94, 'project_live_project_url', 'https://electronic-store34.vercel.app/'),
(236, 94, '_project_live_project_url', 'field_66361edfb6c7b'),
(237, 94, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/electronic-store'),
(238, 94, '_project_gitlab_url', 'field_66361f1db6c7c'),
(239, 95, '_wp_attached_file', '2024/05/leon.png'),
(240, 95, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:16:\"2024/05/leon.png\";s:8:\"filesize\";i:430382;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:16:\"leon-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:58480;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:16:\"leon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:23489;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:16:\"leon-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:275487;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(241, 96, '_wp_attached_file', '2024/05/my-agency.png'),
(242, 96, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:21:\"2024/05/my-agency.png\";s:8:\"filesize\";i:135368;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:21:\"my-agency-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:30122;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:21:\"my-agency-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:13570;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:21:\"my-agency-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:120317;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(243, 97, '_wp_attached_file', '2024/05/netflix.png'),
(244, 97, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:19:\"2024/05/netflix.png\";s:8:\"filesize\";i:949929;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:19:\"netflix-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:128079;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:19:\"netflix-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:47023;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:19:\"netflix-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:643022;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(245, 98, '_wp_attached_file', '2024/05/nike.png'),
(246, 98, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:16:\"2024/05/nike.png\";s:8:\"filesize\";i:57381;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:16:\"nike-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:17945;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:16:\"nike-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:8792;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:16:\"nike-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:59780;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(247, 99, '_wp_attached_file', '2024/05/portfolio.png'),
(248, 99, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:21:\"2024/05/portfolio.png\";s:8:\"filesize\";i:435954;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:21:\"portfolio-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:56867;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:21:\"portfolio-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:23601;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:21:\"portfolio-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:311871;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(249, 100, '_wp_attached_file', '2024/05/dashboard.png'),
(250, 100, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:21:\"2024/05/dashboard.png\";s:8:\"filesize\";i:63648;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:21:\"dashboard-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:15540;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:21:\"dashboard-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:5352;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:21:\"dashboard-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:60363;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(251, 101, '_wp_attached_file', '2024/05/electro-store.png'),
(252, 101, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:25:\"2024/05/electro-store.png\";s:8:\"filesize\";i:177042;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:25:\"electro-store-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:33851;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:25:\"electro-store-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:16036;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:25:\"electro-store-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:139168;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(253, 102, '_wp_attached_file', '2024/05/gym.png'),
(254, 102, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:15:\"2024/05/gym.png\";s:8:\"filesize\";i:189500;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:15:\"gym-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:40621;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:15:\"gym-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:16769;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:15:\"gym-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:161921;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(255, 103, '_wp_attached_file', '2024/05/kasper.png'),
(256, 103, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:18:\"2024/05/kasper.png\";s:8:\"filesize\";i:692616;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:18:\"kasper-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:84786;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:18:\"kasper-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:30919;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:18:\"kasper-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:454053;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(257, 94, '_thumbnail_id', '101'),
(262, 105, '_wp_attached_file', '2024/05/real-estate.png'),
(263, 105, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:23:\"2024/05/real-estate.png\";s:8:\"filesize\";i:335821;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:23:\"real-estate-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:61522;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:23:\"real-estate-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:26003;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:23:\"real-estate-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:265437;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(264, 106, '_edit_last', '1'),
(265, 106, '_edit_lock', '1715526466:1'),
(266, 106, '_thumbnail_id', '105'),
(269, 105, '_wp_old_slug', 'real-estate'),
(272, 106, 'project_sections', ''),
(273, 106, '_project_sections', 'field_66361fdaecf6d'),
(274, 106, 'project_technologies', 'a:6:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"67\";i:4;s:2:\"74\";i:5;s:2:\"75\";}'),
(275, 106, '_project_technologies', 'field_66361e51b6c7a'),
(276, 106, 'project_live_project_url', 'https://real-estate-landing-page34.vercel.app/'),
(277, 106, '_project_live_project_url', 'field_66361edfb6c7b'),
(278, 106, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/real-estate-landing-page'),
(279, 106, '_project_gitlab_url', 'field_66361f1db6c7c'),
(280, 107, '_edit_last', '1'),
(281, 107, '_edit_lock', '1715526453:1'),
(282, 107, '_thumbnail_id', '99'),
(285, 99, '_wp_old_slug', 'portfolio'),
(288, 107, 'project_sections', ''),
(289, 107, '_project_sections', 'field_66361fdaecf6d'),
(290, 107, 'project_technologies', 'a:7:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"136\";i:3;s:2:\"65\";i:4;s:2:\"68\";i:5;s:2:\"74\";i:6;s:2:\"75\";}'),
(291, 107, '_project_technologies', 'field_66361e51b6c7a'),
(292, 107, 'project_live_project_url', 'https://portfolio34.vercel.app/'),
(293, 107, '_project_live_project_url', 'field_66361edfb6c7b'),
(294, 107, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/portfolio-with-nextjs'),
(295, 107, '_project_gitlab_url', 'field_66361f1db6c7c'),
(296, 108, '_edit_last', '1'),
(297, 108, '_edit_lock', '1715526440:1'),
(298, 108, '_thumbnail_id', '98'),
(301, 98, '_wp_old_slug', 'nike'),
(304, 108, 'project_sections', ''),
(305, 108, '_project_sections', 'field_66361fdaecf6d'),
(306, 108, 'project_technologies', 'a:7:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"136\";i:3;s:2:\"65\";i:4;s:2:\"67\";i:5;s:2:\"74\";i:6;s:2:\"75\";}'),
(307, 108, '_project_technologies', 'field_66361e51b6c7a'),
(308, 108, 'project_live_project_url', 'https://nike-landing-page34.vercel.app/'),
(309, 108, '_project_live_project_url', 'field_66361edfb6c7b'),
(310, 108, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/nike-landing-page'),
(311, 108, '_project_gitlab_url', 'field_66361f1db6c7c'),
(312, 109, '_edit_last', '1'),
(313, 109, '_edit_lock', '1715526415:1'),
(314, 109, '_thumbnail_id', '96'),
(317, 96, '_wp_old_slug', 'my-agency'),
(320, 109, 'project_sections', ''),
(321, 109, '_project_sections', 'field_66361fdaecf6d'),
(322, 109, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(323, 109, '_project_technologies', 'field_66361e51b6c7a'),
(324, 109, 'project_live_project_url', 'https://elzero-template34.vercel.app/'),
(325, 109, '_project_live_project_url', 'field_66361edfb6c7b'),
(326, 109, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/elzero-template'),
(327, 109, '_project_gitlab_url', 'field_66361f1db6c7c'),
(328, 110, '_edit_last', '1'),
(329, 110, '_edit_lock', '1715526400:1'),
(330, 110, '_thumbnail_id', '95'),
(333, 95, '_wp_old_slug', 'leon'),
(336, 110, 'project_sections', ''),
(337, 110, '_project_sections', 'field_66361fdaecf6d'),
(338, 110, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(339, 110, '_project_technologies', 'field_66361e51b6c7a'),
(340, 110, 'project_live_project_url', 'https://leon-landing-page34.vercel.app/'),
(341, 110, '_project_live_project_url', 'field_66361edfb6c7b'),
(342, 110, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/leon-landing-page'),
(343, 110, '_project_gitlab_url', 'field_66361f1db6c7c'),
(344, 111, '_edit_last', '1'),
(345, 111, '_edit_lock', '1715526426:1'),
(346, 111, '_thumbnail_id', '103'),
(349, 103, '_wp_old_slug', 'kasper'),
(352, 111, 'project_sections', ''),
(353, 111, '_project_sections', 'field_66361fdaecf6d'),
(354, 111, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(355, 111, '_project_technologies', 'field_66361e51b6c7a'),
(356, 111, 'project_live_project_url', 'https://kasper-landing-page34.vercel.app/'),
(357, 111, '_project_live_project_url', 'field_66361edfb6c7b'),
(358, 111, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/kasper-landing-page'),
(359, 111, '_project_gitlab_url', 'field_66361f1db6c7c'),
(360, 112, '_edit_last', '1'),
(361, 112, '_edit_lock', '1715526381:1'),
(362, 112, '_thumbnail_id', '97'),
(367, 112, 'project_sections', ''),
(368, 112, '_project_sections', 'field_66361fdaecf6d'),
(369, 112, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(370, 112, '_project_technologies', 'field_66361e51b6c7a'),
(371, 112, 'project_live_project_url', 'https://clone-netflix34.vercel.app/'),
(372, 112, '_project_live_project_url', 'field_66361edfb6c7b'),
(373, 112, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/clone-netflix'),
(374, 112, '_project_gitlab_url', 'field_66361f1db6c7c'),
(375, 113, '_edit_last', '1'),
(376, 113, '_edit_lock', '1715526355:1'),
(377, 113, '_thumbnail_id', '100'),
(380, 100, '_wp_old_slug', 'dashboard'),
(383, 113, 'project_sections', ''),
(384, 113, '_project_sections', 'field_66361fdaecf6d'),
(385, 113, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(386, 113, '_project_technologies', 'field_66361e51b6c7a'),
(387, 113, 'project_live_project_url', 'https://elzero-dashboard34.vercel.app/'),
(388, 113, '_project_live_project_url', 'field_66361edfb6c7b'),
(389, 113, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/elzero-dashboard'),
(390, 113, '_project_gitlab_url', 'field_66361f1db6c7c'),
(396, 102, '_wp_old_slug', 'gym'),
(407, 115, '_edit_last', '1'),
(408, 115, '_edit_lock', '1714920835:1'),
(409, 120, '_edit_last', '1'),
(410, 120, '_edit_lock', '1715280769:1'),
(413, 120, '_thumbnail_id', '128'),
(414, 120, 'profile_social_0_profile_social_title', 'Gmail'),
(415, 120, '_profile_social_0_profile_social_title', 'field_66363fcca660e'),
(416, 120, 'profile_social_0_profile_social_url', 'mailto:elachhabjawad@gmail.com'),
(417, 120, '_profile_social_0_profile_social_url', 'field_66363f94a660c'),
(418, 120, 'profile_social_0_profile_social_iconsvg', ''),
(419, 120, '_profile_social_0_profile_social_iconsvg', 'field_66363faea660d'),
(420, 120, 'profile_social', '6'),
(421, 120, '_profile_social', 'field_66363dfea660b'),
(422, 120, '_wp_old_slug', '120'),
(423, 120, 'profile_social_0_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 512 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M437.332 80H74.668C51.199 80 32 99.198 32 122.667v266.666C32 412.802 51.199 432 74.668 432h362.664C460.801 432 480 412.802 480 389.333V122.667C480 99.198 460.801 80 437.332 80zM432 170.667L256 288 80 170.667V128l176 117.333L432 128v42.667z\"></path></svg>'),
(424, 120, '_profile_social_0_profile_social_icon_svg', 'field_66363faea660d'),
(425, 120, 'profile_social_1_profile_social_title', 'Gitlab'),
(426, 120, '_profile_social_1_profile_social_title', 'field_66363fcca660e'),
(427, 120, 'profile_social_1_profile_social_url', 'https://gitlab.com/elachhabjawad'),
(428, 120, '_profile_social_1_profile_social_url', 'field_66363f94a660c'),
(429, 120, 'profile_social_1_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 480 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M186.1 328.7c0 20.9-10.9 55.1-36.7 55.1s-36.7-34.2-36.7-55.1 10.9-55.1 36.7-55.1 36.7 34.2 36.7 55.1zM480 278.2c0 31.9-3.2 65.7-17.5 95-37.9 76.6-142.1 74.8-216.7 74.8-75.8 0-186.2 2.7-225.6-74.8-14.6-29-20.2-63.1-20.2-95 0-41.9 13.9-81.5 41.5-113.6-5.2-15.8-7.7-32.4-7.7-48.8 0-21.5 4.9-32.3 14.6-51.8 45.3 0 74.3 9 108.8 36 29-6.9 58.8-10 88.7-10 27 0 54.2 2.9 80.4 9.2 34-26.7 63-35.2 107.8-35.2 9.8 19.5 14.6 30.3 14.6 51.8 0 16.4-2.6 32.7-7.7 48.2 27.5 32.4 39 72.3 39 114.2zm-64.3 50.5c0-43.9-26.7-82.6-73.5-82.6-18.9 0-37 3.4-56 6-14.9 2.3-29.8 3.2-45.1 3.2-15.2 0-30.1-.9-45.1-3.2-18.7-2.6-37-6-56-6-46.8 0-73.5 38.7-73.5 82.6 0 87.8 80.4 101.3 150.4 101.3h48.2c70.3 0 150.6-13.4 150.6-101.3zm-82.6-55.1c-25.8 0-36.7 34.2-36.7 55.1s10.9 55.1 36.7 55.1 36.7-34.2 36.7-55.1-10.9-55.1-36.7-55.1z\"></path></svg>'),
(430, 120, '_profile_social_1_profile_social_icon_svg', 'field_66363faea660d'),
(431, 120, 'profile_social_2_profile_social_title', 'Linkedin'),
(432, 120, '_profile_social_2_profile_social_title', 'field_66363fcca660e'),
(433, 120, 'profile_social_2_profile_social_url', 'https://ma.linkedin.com/in/jawad-el-achhab-a64532160'),
(434, 120, '_profile_social_2_profile_social_url', 'field_66363f94a660c'),
(435, 120, 'profile_social_2_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M4 4m0 2a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-12a2 2 0 0 1 -2 -2z\"></path> <path d=\"M8 11l0 5\"></path> <path d=\"M8 8l0 .01\"></path> <path d=\"M12 16l0 -5\"></path> <path d=\"M16 16v-3a2 2 0 0 0 -4 0\"></path></svg>'),
(436, 120, '_profile_social_2_profile_social_icon_svg', 'field_66363faea660d'),
(437, 120, 'profile_social_3_profile_social_title', 'Codesandbox'),
(438, 120, '_profile_social_3_profile_social_title', 'field_66363fcca660e'),
(439, 120, 'profile_social_3_profile_social_url', 'https://codesandbox.io/u/elachhabjawad'),
(440, 120, '_profile_social_3_profile_social_url', 'field_66363f94a660c'),
(441, 120, 'profile_social_3_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 640 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M392.8 1.2c-17-4.9-34.7 5-39.6 22l-128 448c-4.9 17 5 34.7 22 39.6s34.7-5 39.6-22l128-448c4.9-17-5-34.7-22-39.6zm80.6 120.1c-12.5 12.5-12.5 32.8 0 45.3L562.7 256l-89.4 89.4c-12.5 12.5-12.5 32.8 0 45.3s32.8 12.5 45.3 0l112-112c12.5-12.5 12.5-32.8 0-45.3l-112-112c-12.5-12.5-32.8-12.5-45.3 0zm-306.7 0c-12.5-12.5-32.8-12.5-45.3 0l-112 112c-12.5 12.5-12.5 32.8 0 45.3l112 112c12.5 12.5 32.8 12.5 45.3 0s12.5-32.8 0-45.3L77.3 256l89.4-89.4c12.5-12.5 12.5-32.8 0-45.3z\"></path></svg>'),
(442, 120, '_profile_social_3_profile_social_icon_svg', 'field_66363faea660d'),
(443, 120, 'profile_social_4_profile_social_title', 'X'),
(444, 120, '_profile_social_4_profile_social_title', 'field_66363fcca660e'),
(445, 120, 'profile_social_4_profile_social_url', 'https://twitter.com/JawadELACHHAB2'),
(446, 120, '_profile_social_4_profile_social_url', 'field_66363f94a660c'),
(447, 120, 'profile_social_4_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 512 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M389.2 48h70.6L305.6 224.2 487 464H345L233.7 318.6 106.5 464H35.8L200.7 275.5 26.8 48H172.4L272.9 180.9 389.2 48zM364.4 421.8h39.1L151.1 88h-42L364.4 421.8z\"></path></svg>'),
(448, 120, '_profile_social_4_profile_social_icon_svg', 'field_66363faea660d'),
(449, 120, 'profile_social_5_profile_social_title', 'Instagram'),
(450, 120, '_profile_social_5_profile_social_title', 'field_66363fcca660e'),
(451, 120, 'profile_social_5_profile_social_url', 'https://instagram.com/elachhab.dev'),
(452, 120, '_profile_social_5_profile_social_url', 'field_66363f94a660c'),
(453, 120, 'profile_social_5_profile_social_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 448 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z\"></path></svg>'),
(454, 120, '_profile_social_5_profile_social_icon_svg', 'field_66363faea660d'),
(455, 62, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M20 4l-2 14.5l-6 2l-6 -2l-2 -14.5z\"></path> <path d=\"M15.5 8h-7l.5 4h6l-.5 3.5l-2.5 .75l-2.5 -.75l-.1 -.5\"></path></svg>'),
(456, 62, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(457, 64, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M20 4l-2 14.5l-6 2l-6 -2l-2 -14.5z\"></path> <path d=\"M8.5 8h7l-4.5 4h4l-.5 3.5l-2.5 .75l-2.5 -.75l-.1 -.5\"></path></svg>'),
(458, 64, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(459, 65, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M20 4l-2 14.5l-6 2l-6 -2l-2 -14.5z\"></path> <path d=\"M7.5 8h3v8l-2 -1\"></path> <path d=\"M16.5 8h-2.5a.5 .5 0 0 0 -.5 .5v3a.5 .5 0 0 0 .5 .5h1.423a.5 .5 0 0 1 .495 .57l-.418 2.93l-2 .5\"></path></svg>'),
(460, 65, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(461, 66, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M15 17.5c.32 .32 .754 .5 1.207 .5h.543c.69 0 1.25 -.56 1.25 -1.25v-.25a1.5 1.5 0 0 0 -1.5 -1.5a1.5 1.5 0 0 1 -1.5 -1.5v-.25c0 -.69 .56 -1.25 1.25 -1.25h.543c.453 0 .887 .18 1.207 .5\"></path> <path d=\"M9 12h4\"></path> <path d=\"M11 12v6\"></path> <path d=\"M21 19v-14a2 2 0 0 0 -2 -2h-14a2 2 0 0 0 -2 2v14a2 2 0 0 0 2 2h14a2 2 0 0 0 2 -2z\"></path></svg>'),
(462, 66, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(463, 67, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M6.306 8.711c-2.602 .723 -4.306 1.926 -4.306 3.289c0 2.21 4.477 4 10 4c.773 0 1.526 -.035 2.248 -.102\"> </path> <path d=\"M17.692 15.289c2.603 -.722 4.308 -1.926 4.308 -3.289c0 -2.21 -4.477 -4 -10 -4c-.773 0 -1.526 .035 -2.25 .102\"></path> <path d=\"M6.305 15.287c-.676 2.615 -.485 4.693 .695 5.373c1.913 1.105 5.703 -1.877 8.464 -6.66c.387 -.67 .733 -1.339 1.036 -2\"></path> <path d=\"M17.694 8.716c.677 -2.616 .487 -4.696 -.694 -5.376c-1.913 -1.105 -5.703 1.877 -8.464 6.66c-.387 .67 -.733 1.34 -1.037 2\"></path> <path d=\"M12 5.424c-1.925 -1.892 -3.82 -2.766 -5 -2.084c-1.913 1.104 -1.226 5.877 1.536 10.66c.386 .67 .793 1.304 1.212 1.896\"></path> <path d=\"M12 18.574c1.926 1.893 3.821 2.768 5 2.086c1.913 -1.104 1.226 -5.877 -1.536 -10.66c-.386 -.67 -.793 -1.304 -1.212 -1.896\"></path> <path d=\"M12 5.424v13.15\"></path> <path d=\"M5.848 9.739l12.306 5.122\"></path> <path d=\"M6.305 9.713l12.306 -5.122\"></path></svg>'),
(464, 67, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(465, 68, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"none\" stroke-width=\"2\" viewBox=\"0 0 24 24\" stroke-linecap=\"round\" stroke-linejoin=\"round\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path stroke=\"none\" d=\"M0 0h24v24H0z\" fill=\"none\"></path> <path d=\"M9 15v-6l7.745 10.65a9 9 0 1 1 2.255 -1.993\"></path> <path d=\"M15 12v-3\"></path></svg>'),
(466, 68, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(467, 69, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 640 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M320 104.5c171.4 0 303.2 72.2 303.2 151.5S491.3 407.5 320 407.5c-171.4 0-303.2-72.2-303.2-151.5S148.7 104.5 320 104.5m0-16.8C143.3 87.7 0 163 0 256s143.3 168.3 320 168.3S640 349 640 256 496.7 87.7 320 87.7zM218.2 242.5c-7.9 40.5-35.8 36.3-70.1 36.3l13.7-70.6c38 0 63.8-4.1 56.4 34.3zM97.4 350.3h36.7l8.7-44.8c41.1 0 66.6 3 90.2-19.1 26.1-24 32.9-66.7 14.3-88.1-9.7-11.2-25.3-16.7-46.5-16.7h-70.7L97.4 350.3zm185.7-213.6h36.5l-8.7 44.8c31.5 0 60.7-2.3 74.8 10.7 14.8 13.6 7.7 31-8.3 113.1h-37c15.4-79.4 18.3-86 12.7-92-5.4-5.8-17.7-4.6-47.4-4.6l-18.8 96.6h-36.5l32.7-168.6zM505 242.5c-8 41.1-36.7 36.3-70.1 36.3l13.7-70.6c38.2 0 63.8-4.1 56.4 34.3zM384.2 350.3H421l8.7-44.8c43.2 0 67.1 2.5 90.2-19.1 26.1-24 32.9-66.7 14.3-88.1-9.7-11.2-25.3-16.7-46.5-16.7H417l-32.8 168.7z\"> </path></svg>'),
(468, 69, '_tech_icon_svg', 'field_6637a2b8cb1ea');
INSERT INTO `portfolio_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(469, 70, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 512 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M504.4,115.83a5.72,5.72,0,0,0-.28-.68,8.52,8.52,0,0,0-.53-1.25,6,6,0,0,0-.54-.71,9.36,9.36,0,0,0-.72-.94c-.23-.22-.52-.4-.77-.6a8.84,8.84,0,0,0-.9-.68L404.4,55.55a8,8,0,0,0-8,0L300.12,111h0a8.07,8.07,0,0,0-.88.69,7.68,7.68,0,0,0-.78.6,8.23,8.23,0,0,0-.72.93c-.17.24-.39.45-.54.71a9.7,9.7,0,0,0-.52,1.25c-.08.23-.21.44-.28.68a8.08,8.08,0,0,0-.28,2.08V223.18l-80.22,46.19V63.44a7.8,7.8,0,0,0-.28-2.09c-.06-.24-.2-.45-.28-.68a8.35,8.35,0,0,0-.52-1.24c-.14-.26-.37-.47-.54-.72a9.36,9.36,0,0,0-.72-.94,9.46,9.46,0,0,0-.78-.6,9.8,9.8,0,0,0-.88-.68h0L115.61,1.07a8,8,0,0,0-8,0L11.34,56.49h0a6.52,6.52,0,0,0-.88.69,7.81,7.81,0,0,0-.79.6,8.15,8.15,0,0,0-.71.93c-.18.25-.4.46-.55.72a7.88,7.88,0,0,0-.51,1.24,6.46,6.46,0,0,0-.29.67,8.18,8.18,0,0,0-.28,2.1v329.7a8,8,0,0,0,4,6.95l192.5,110.84a8.83,8.83,0,0,0,1.33.54c.21.08.41.2.63.26a7.92,7.92,0,0,0,4.1,0c.2-.05.37-.16.55-.22a8.6,8.6,0,0,0,1.4-.58L404.4,400.09a8,8,0,0,0,4-6.95V287.88l92.24-53.11a8,8,0,0,0,4-7V117.92A8.63,8.63,0,0,0,504.4,115.83ZM111.6,17.28h0l80.19,46.15-80.2,46.18L31.41,63.44Zm88.25,60V278.6l-46.53,26.79-33.69,19.4V123.5l46.53-26.79Zm0,412.78L23.37,388.5V77.32L57.06,96.7l46.52,26.8V338.68a6.94,6.94,0,0,0,.12.9,8,8,0,0,0,.16,1.18h0a5.92,5.92,0,0,0,.38.9,6.38,6.38,0,0,0,.42,1v0a8.54,8.54,0,0,0,.6.78,7.62,7.62,0,0,0,.66.84l0,0c.23.22.52.38.77.58a8.93,8.93,0,0,0,.86.66l0,0,0,0,92.19,52.18Zm8-106.17-80.06-45.32,84.09-48.41,92.26-53.11,80.13,46.13-58.8,33.56Zm184.52,4.57L215.88,490.11V397.8L346.6,323.2l45.77-26.15Zm0-119.13L358.68,250l-46.53-26.79V131.79l33.69,19.4L392.37,178Zm8-105.28-80.2-46.17,80.2-46.16,80.18,46.15Zm8,105.28V178L455,151.19l33.68-19.4v91.39h0Z\"> </path></svg>'),
(470, 70, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(471, 71, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 512 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M61.7 169.4l101.5 278C92.2 413 43.3 340.2 43.3 256c0-30.9 6.6-60.1 18.4-86.6zm337.9 75.9c0-26.3-9.4-44.5-17.5-58.7-10.8-17.5-20.9-32.4-20.9-49.9 0-19.6 14.8-37.8 35.7-37.8.9 0 1.8.1 2.8.2-37.9-34.7-88.3-55.9-143.7-55.9-74.3 0-139.7 38.1-177.8 95.9 5 .2 9.7.3 13.7.3 22.2 0 56.7-2.7 56.7-2.7 11.5-.7 12.8 16.2 1.4 17.5 0 0-11.5 1.3-24.3 2l77.5 230.4L249.8 247l-33.1-90.8c-11.5-.7-22.3-2-22.3-2-11.5-.7-10.1-18.2 1.3-17.5 0 0 35.1 2.7 56 2.7 22.2 0 56.7-2.7 56.7-2.7 11.5-.7 12.8 16.2 1.4 17.5 0 0-11.5 1.3-24.3 2l76.9 228.7 21.2-70.9c9-29.4 16-50.5 16-68.7zm-139.9 29.3l-63.8 185.5c19.1 5.6 39.2 8.7 60.1 8.7 24.8 0 48.5-4.3 70.6-12.1-.6-.9-1.1-1.9-1.5-2.9l-65.4-179.2zm183-120.7c.9 6.8 1.4 14 1.4 21.9 0 21.6-4 45.8-16.2 76.2l-65 187.9C426.2 403 468.7 334.5 468.7 256c0-37-9.4-71.8-26-102.1zM504 256c0 136.8-111.3 248-248 248C119.2 504 8 392.7 8 256 8 119.2 119.2 8 256 8c136.7 0 248 111.2 248 248zm-11.4 0c0-130.5-106.2-236.6-236.6-236.6C125.5 19.4 19.4 125.5 19.4 256S125.6 492.6 256 492.6c130.5 0 236.6-106.1 236.6-236.6z\"> </path></svg>'),
(472, 71, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(473, 72, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 448 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M303.973,108.136C268.2,72.459,234.187,38.35,224.047,0c-9.957,38.35-44.25,72.459-80.019,108.136C90.467,161.7,29.716,222.356,29.716,313.436c-2.337,107.3,82.752,196.18,190.053,198.517S415.948,429.2,418.285,321.9q.091-4.231,0-8.464C418.285,222.356,357.534,161.7,303.973,108.136Zm-174.326,223a130.282,130.282,0,0,0-15.211,24.153,4.978,4.978,0,0,1-3.319,2.766h-1.659c-4.333,0-9.219-8.481-9.219-8.481h0c-1.29-2.028-2.489-4.149-3.687-6.361l-.83-1.752c-11.247-25.72-1.475-62.318-1.475-62.318h0a160.585,160.585,0,0,1,23.231-49.873A290.8,290.8,0,0,1,138.5,201.613l9.219,9.219,43.512,44.434a4.979,4.979,0,0,1,0,6.638L145.78,312.33h0Zm96.612,127.311a67.2,67.2,0,0,1-49.781-111.915c14.2-16.871,31.528-33.464,50.334-55.313,22.309,23.785,36.875,40.1,51.164,57.986a28.413,28.413,0,0,1,2.95,4.425,65.905,65.905,0,0,1,11.984,37.981,66.651,66.651,0,0,1-66.466,66.836ZM352.371,351.6h0a7.743,7.743,0,0,1-6.176,5.347H344.9a11.249,11.249,0,0,1-6.269-5.07h0a348.21,348.21,0,0,0-39.456-48.952L281.387,284.49,222.3,223.185a497.888,497.888,0,0,1-35.4-36.322,12.033,12.033,0,0,0-.922-1.382,35.4,35.4,0,0,1-4.7-9.219V174.51a31.346,31.346,0,0,1,9.218-27.656c11.432-11.431,22.955-22.954,33.833-34.939,11.984,13.275,24.8,26,37.428,38.627h0a530.991,530.991,0,0,1,69.6,79.1,147.494,147.494,0,0,1,27.011,83.8A134.109,134.109,0,0,1,352.371,351.6Z\"> </path></svg>'),
(474, 72, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(475, 73, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 448 512\" height=\"1em\" width=\"1em\" xmlns=\"http: //www.w3.org/2000/svg\"> <path d=\"M448 80v48c0 44.2-100.3 80-224 80S0 172.2 0 128V80C0 35.8 100.3 0 224 0S448 35.8 448 80zM393.2 214.7c20.8-7.4 39.9-16.9 54.8-28.6V288c0 44.2-100.3 80-224 80S0 332.2 0 288V186.1c14.9 11.8 34 21.2 54.8 28.6C99.7 230.7 159.5 240 224 240s124.3-9.3 169.2-25.3zM0 346.1c14.9 11.8 34 21.2 54.8 28.6C99.7 390.7 159.5 400 224 400s124.3-9.3 169.2-25.3c20.8-7.4 39.9-16.9 54.8-28.6V432c0 44.2-100.3 80-224 80S0 476.2 0 432V346.1z\"> </path></svg>'),
(476, 73, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(477, 74, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 448 512\" height=\"1em\" width=\"1em\"xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M439.55 236.05L244 40.45a28.87 28.87 0 0 0-40.81 0l-40.66 40.63 51.52 51.52c27.06-9.14 52.68 16.77 43.39 43.68l49.66 49.66c34.23-11.8 61.18 31 35.47 56.69-26.49 26.49-70.21-2.87-56-37.34L240.22 199v121.85c25.3 12.54 22.26 41.85 9.08 55a34.34 34.34 0 0 1-48.55 0c-17.57-17.6-11.07-46.91 11.25-56v-123c-20.8-8.51-24.6-30.74-18.64-45L142.57 101 8.45 235.14a28.86 28.86 0 0 0 0 40.81l195.61 195.6a28.86 28.86 0 0 0 40.8 0l194.69-194.69a28.86 28.86 0 0 0 0-40.81z\"></path></svg>'),
(478, 74, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(479, 75, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 480 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"> <path d=\"M186.1 328.7c0 20.9-10.9 55.1-36.7 55.1s-36.7-34.2-36.7-55.1 10.9-55.1 36.7-55.1 36.7 34.2 36.7 55.1zM480 278.2c0 31.9-3.2 65.7-17.5 95-37.9 76.6-142.1 74.8-216.7 74.8-75.8 0-186.2 2.7-225.6-74.8-14.6-29-20.2-63.1-20.2-95 0-41.9 13.9-81.5 41.5-113.6-5.2-15.8-7.7-32.4-7.7-48.8 0-21.5 4.9-32.3 14.6-51.8 45.3 0 74.3 9 108.8 36 29-6.9 58.8-10 88.7-10 27 0 54.2 2.9 80.4 9.2 34-26.7 63-35.2 107.8-35.2 9.8 19.5 14.6 30.3 14.6 51.8 0 16.4-2.6 32.7-7.7 48.2 27.5 32.4 39 72.3 39 114.2zm-64.3 50.5c0-43.9-26.7-82.6-73.5-82.6-18.9 0-37 3.4-56 6-14.9 2.3-29.8 3.2-45.1 3.2-15.2 0-30.1-.9-45.1-3.2-18.7-2.6-37-6-56-6-46.8 0-73.5 38.7-73.5 82.6 0 87.8 80.4 101.3 150.4 101.3h48.2c70.3 0 150.6-13.4 150.6-101.3zm-82.6-55.1c-25.8 0-36.7 34.2-36.7 55.1s10.9 55.1 36.7 55.1 36.7-34.2 36.7-55.1-10.9-55.1-36.7-55.1z\"></path></svg>'),
(480, 75, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(481, 128, '_wp_attached_file', '2024/05/Untitled-design.png'),
(482, 128, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:400;s:6:\"height\";i:400;s:4:\"file\";s:27:\"2024/05/Untitled-design.png\";s:8:\"filesize\";i:91000;s:5:\"sizes\";a:2:{s:6:\"medium\";a:5:{s:4:\"file\";s:27:\"Untitled-design-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:53959;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:27:\"Untitled-design-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:16722;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(483, 81, '_wp_old_slug', 'php-laravel-developer'),
(484, 84, '_wp_old_slug', 'php-developer'),
(485, 131, '_edit_last', '1'),
(486, 131, '_edit_lock', '1715527417:1'),
(487, 131, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 576 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M333.5,201.4c0-22.1-15.6-34.3-43-34.3h-50.4v71.2h42.5C315.4,238.2,333.5,225,333.5,201.4z M517,188.6 c-9.5-30.9-10.9-68.8-9.8-98.1c1.1-30.5-22.7-58.5-54.7-58.5H123.7c-32.1,0-55.8,28.1-54.7,58.5c1,29.3-0.3,67.2-9.8,98.1 c-9.6,31-25.7,50.6-52.2,53.1v28.5c26.4,2.5,42.6,22.1,52.2,53.1c9.5,30.9,10.9,68.8,9.8,98.1c-1.1,30.5,22.7,58.5,54.7,58.5h328.7 c32.1,0,55.8-28.1,54.7-58.5c-1-29.3,0.3-67.2,9.8-98.1c9.6-31,25.7-50.6,52.1-53.1v-28.5C542.7,239.2,526.5,219.6,517,188.6z M300.2,375.1h-97.9V136.8h97.4c43.3,0,71.7,23.4,71.7,59.4c0,25.3-19.1,47.9-43.5,51.8v1.3c33.2,3.6,55.5,26.6,55.5,58.3 C383.4,349.7,352.1,375.1,300.2,375.1z M290.2,266.4h-50.1v78.4h52.3c34.2,0,52.3-13.7,52.3-39.5 C344.7,279.6,326.1,266.4,290.2,266.4z\"></path></svg>'),
(488, 131, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(489, 131, 'tech_start_date', '20180510'),
(490, 131, '_tech_start_date', 'field_6635094313c6c'),
(491, 132, '_edit_last', '1'),
(492, 132, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 640 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M301.84 378.92c-.3.6-.6 1.08 0 0zm249.13-87a131.16 131.16 0 0 0-58 13.5c-5.9-11.9-12-22.3-13-30.1-1.2-9.1-2.5-14.5-1.1-25.3s7.7-26.1 7.6-27.2-1.4-6.6-14.3-6.7-24 2.5-25.29 5.9a122.83 122.83 0 0 0-5.3 19.1c-2.3 11.7-25.79 53.5-39.09 75.3-4.4-8.5-8.1-16-8.9-22-1.2-9.1-2.5-14.5-1.1-25.3s7.7-26.1 7.6-27.2-1.4-6.6-14.29-6.7-24 2.5-25.3 5.9-2.7 11.4-5.3 19.1-33.89 77.3-42.08 95.4c-4.2 9.2-7.8 16.6-10.4 21.6-.4.8-.7 1.3-.9 1.7.3-.5.5-1 .5-.8-2.2 4.3-3.5 6.7-3.5 6.7v.1c-1.7 3.2-3.6 6.1-4.5 6.1-.6 0-1.9-8.4.3-19.9 4.7-24.2 15.8-61.8 15.7-63.1-.1-.7 2.1-7.2-7.3-10.7-9.1-3.3-12.4 2.2-13.2 2.2s-1.4 2-1.4 2 10.1-42.4-19.39-42.4c-18.4 0-44 20.2-56.58 38.5-7.9 4.3-25 13.6-43 23.5-6.9 3.8-14 7.7-20.7 11.4-.5-.5-.9-1-1.4-1.5-35.79-38.2-101.87-65.2-99.07-116.5 1-18.7 7.5-67.8 127.07-127.4 98-48.8 176.35-35.4 189.84-5.6 19.4 42.5-41.89 121.6-143.66 133-38.79 4.3-59.18-10.7-64.28-16.3-5.3-5.9-6.1-6.2-8.1-5.1-3.3 1.8-1.2 7 0 10.1 3 7.9 15.5 21.9 36.79 28.9 18.7 6.1 64.18 9.5 119.17-11.8 61.78-23.8 109.87-90.1 95.77-145.6C386.52 18.32 293-.18 204.57 31.22c-52.69 18.7-109.67 48.1-150.66 86.4-48.69 45.6-56.48 85.3-53.28 101.9 11.39 58.9 92.57 97.3 125.06 125.7-1.6.9-3.1 1.7-4.5 2.5-16.29 8.1-78.18 40.5-93.67 74.7-17.5 38.8 2.9 66.6 16.29 70.4 41.79 11.6 84.58-9.3 107.57-43.6s20.2-79.1 9.6-99.5c-.1-.3-.3-.5-.4-.8 4.2-2.5 8.5-5 12.8-7.5 8.29-4.9 16.39-9.4 23.49-13.3-4 10.8-6.9 23.8-8.4 42.6-1.8 22 7.3 50.5 19.1 61.7 5.2 4.9 11.49 5 15.39 5 13.8 0 20-11.4 26.89-25 8.5-16.6 16-35.9 16-35.9s-9.4 52.2 16.3 52.2c9.39 0 18.79-12.1 23-18.3v.1s.2-.4.7-1.2c1-1.5 1.5-2.4 1.5-2.4v-.3c3.8-6.5 12.1-21.4 24.59-46 16.2-31.8 31.69-71.5 31.69-71.5a201.24 201.24 0 0 0 6.2 25.8c2.8 9.5 8.7 19.9 13.4 30-3.8 5.2-6.1 8.2-6.1 8.2a.31.31 0 0 0 .1.2c-3 4-6.4 8.3-9.9 12.5-12.79 15.2-28 32.6-30 37.6-2.4 5.9-1.8 10.3 2.8 13.7 3.4 2.6 9.4 3 15.69 2.5 11.5-.8 19.6-3.6 23.5-5.4a82.2 82.2 0 0 0 20.19-10.6c12.5-9.2 20.1-22.4 19.4-39.8-.4-9.6-3.5-19.2-7.3-28.2 1.1-1.6 2.3-3.3 3.4-5C434.8 301.72 450.1 270 450.1 270a201.24 201.24 0 0 0 6.2 25.8c2.4 8.1 7.09 17 11.39 25.7-18.59 15.1-30.09 32.6-34.09 44.1-7.4 21.3-1.6 30.9 9.3 33.1 4.9 1 11.9-1.3 17.1-3.5a79.46 79.46 0 0 0 21.59-11.1c12.5-9.2 24.59-22.1 23.79-39.6-.3-7.9-2.5-15.8-5.4-23.4 15.7-6.6 36.09-10.2 62.09-7.2 55.68 6.5 66.58 41.3 64.48 55.8s-13.8 22.6-17.7 25-5.1 3.3-4.8 5.1c.5 2.6 2.3 2.5 5.6 1.9 4.6-.8 29.19-11.8 30.29-38.7 1.6-34-31.09-71.4-89-71.1zm-429.18 144.7c-18.39 20.1-44.19 27.7-55.28 21.3C54.61 451 59.31 421.42 82 400c13.8-13 31.59-25 43.39-32.4 2.7-1.6 6.6-4 11.4-6.9.8-.5 1.2-.7 1.2-.7.9-.6 1.9-1.1 2.9-1.7 8.29 30.4.3 57.2-19.1 78.3zm134.36-91.4c-6.4 15.7-19.89 55.7-28.09 53.6-7-1.8-11.3-32.3-1.4-62.3 5-15.1 15.6-33.1 21.9-40.1 10.09-11.3 21.19-14.9 23.79-10.4 3.5 5.9-12.2 49.4-16.2 59.2zm111 53c-2.7 1.4-5.2 2.3-6.4 1.6-.9-.5 1.1-2.4 1.1-2.4s13.9-14.9 19.4-21.7c3.2-4 6.9-8.7 10.89-13.9 0 .5.1 1 .1 1.6-.13 17.9-17.32 30-25.12 34.8zm85.58-19.5c-2-1.4-1.7-6.1 5-20.7 2.6-5.7 8.59-15.3 19-24.5a36.18 36.18 0 0 1 1.9 10.8c-.1 22.5-16.2 30.9-25.89 34.4z\"></path></svg>'),
(493, 132, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(494, 132, 'tech_start_date', '20200510'),
(495, 132, '_tech_start_date', 'field_6635094313c6c'),
(496, 132, '_edit_lock', '1715527418:1'),
(497, 133, '_edit_last', '1'),
(498, 133, '_edit_lock', '1715356221:1'),
(499, 133, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 448 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M392.3 32H56.1C25.1 32 0 57.1 0 88c-.1 0 0-4 0 336 0 30.9 25.1 56 56 56h336.2c30.8-.2 55.7-25.2 55.7-56V88c.1-30.8-24.8-55.8-55.6-56zM197 371.3c-.2 14.7-12.1 26.6-26.9 26.6H87.4c-14.8.1-26.9-11.8-27-26.6V117.1c0-14.8 12-26.9 26.9-26.9h82.9c14.8 0 26.9 12 26.9 26.9v254.2zm193.1-112c0 14.8-12 26.9-26.9 26.9h-81c-14.8 0-26.9-12-26.9-26.9V117.2c0-14.8 12-26.9 26.8-26.9h81.1c14.8 0 26.9 12 26.9 26.9v142.1z\"></path></svg>'),
(500, 133, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(501, 133, 'tech_start_date', '20240510'),
(502, 133, '_tech_start_date', 'field_6635094313c6c'),
(503, 134, '_edit_last', '1'),
(504, 134, '_edit_lock', '1715356255:1'),
(505, 134, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 496 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z\"></path></svg>'),
(506, 134, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(507, 134, 'tech_start_date', '20240510'),
(508, 134, '_tech_start_date', 'field_6635094313c6c'),
(509, 135, '_edit_last', '1'),
(510, 135, '_edit_lock', '1715527468:1'),
(511, 135, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 24 24\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\" role=\"img\"><path d=\"M1.525 5.87c-2.126 3.054-1.862 7.026-.237 10.269.037.079.078.154.118.229.023.052.049.1.077.15.013.027.031.056.047.082.026.052.054.102.081.152l.157.266c.03.049.057.097.09.146.056.094.12.187.178.281.026.04.05.078.079.117a6.368 6.368 0 00.31.445c.078.107.156.211.24.315.027.038.058.076.086.115l.22.269c.028.03.055.067.084.099.098.118.202.233.306.35l.005.006a3.134 3.134 0 00.425.44c.08.083.16.165.245.245l.101.097c.111.105.223.209.34.309.002 0 .003.002.005.003l.057.05c.102.089.205.178.31.26l.125.105c.085.068.174.133.26.2l.137.105c.093.07.192.139.287.207.035.025.07.05.106.073l.03.023.28.185.12.08c.148.094.294.184.44.272.041.02.084.044.123.068.108.062.22.125.329.183.06.034.122.063.184.094.075.042.153.083.234.125a.324.324 0 01.056.023c.033.015.064.031.096.047.12.06.245.118.375.175.024.01.05.02.076.034.144.063.289.123.438.182.034.01.07.027.105.04.135.051.274.103.411.152l.05.018c.154.052.305.102.46.15.036.01.073.023.111.033.16.048.314.105.474.137 10.273 1.872 13.258-6.177 13.258-6.177-2.508 3.266-6.958 4.127-11.174 3.169-.156-.036-.312-.086-.47-.132a13.539 13.539 0 01-.567-.182l-.062-.024c-.136-.046-.267-.097-.4-.148a1.615 1.615 0 00-.11-.04c-.148-.06-.29-.121-.433-.184-.031-.01-.057-.024-.088-.036a23.44 23.44 0 01-.362-.17 1.485 1.485 0 01-.106-.052c-.094-.044-.188-.095-.28-.143a3.947 3.947 0 01-.187-.096c-.114-.06-.227-.125-.34-.187-.034-.024-.073-.044-.112-.066a15.922 15.922 0 01-.439-.27 2.107 2.107 0 01-.118-.078 6.01 6.01 0 01-.312-.207c-.035-.023-.067-.048-.103-.073a9.553 9.553 0 01-.295-.212c-.042-.034-.087-.066-.132-.1-.088-.07-.177-.135-.265-.208l-.118-.095a10.593 10.593 0 01-.335-.28.258.258 0 00-.037-.031l-.347-.316-.1-.094c-.082-.084-.166-.164-.25-.246l-.098-.1a9.081 9.081 0 01-.309-.323l-.015-.016c-.106-.116-.21-.235-.313-.355-.027-.03-.053-.064-.08-.097l-.227-.277a21.275 21.275 0 01-.34-.449C2.152 11.79 1.306 7.384 3.177 3.771m4.943-.473c-1.54 2.211-1.454 5.169-.254 7.508a9.111 9.111 0 00.678 1.133c.23.33.484.721.793.988.107.122.223.24.344.36l.09.09c.114.11.232.217.35.325l.016.013a9.867 9.867 0 00.414.342c.034.023.063.05.096.073.14.108.282.212.428.316l.015.009c.062.045.128.086.198.13.028.018.06.042.09.06.106.068.21.132.318.197.017.007.032.016.048.023.09.055.188.108.282.157.033.02.065.035.1.054.066.033.132.068.197.102l.032.014c.135.067.273.129.408.19.034.014.063.025.092.039.111.048.224.094.336.137.05.017.097.037.144.052.102.038.21.073.31.108l.14.045c.147.045.295.104.449.13C22.164 17.206 24 11.098 24 11.098c-1.653 2.38-4.852 3.513-8.261 2.628a8.04 8.04 0 01-.449-.13c-.048-.014-.09-.029-.136-.043-.104-.036-.211-.07-.312-.109l-.144-.054c-.113-.045-.227-.087-.336-.135-.034-.015-.065-.025-.091-.04-.14-.063-.281-.125-.418-.192l-.206-.107-.119-.06a5.673 5.673 0 01-.265-.15.62.62 0 01-.062-.035c-.106-.066-.217-.13-.318-.198-.034-.019-.065-.042-.097-.062l-.208-.136c-.144-.1-.285-.208-.428-.313-.032-.029-.063-.053-.094-.079-1.499-1.178-2.681-2.79-3.242-4.613-.59-1.897-.46-4.023.56-5.75m4.292-.147c-.909 1.334-.996 2.99-.37 4.46.665 1.563 2.024 2.79 3.608 3.37.065.025.128.046.196.07l.088.027c.092.03.185.063.28.084 4.381.845 5.567-2.25 5.886-2.704-1.043 1.498-2.792 1.857-4.938 1.335a4.85 4.85 0 01-.516-.16 6.352 6.352 0 01-.618-.254 6.53 6.53 0 01-1.082-.66c-1.922-1.457-3.113-4.236-1.859-6.5\"></path></svg>'),
(512, 135, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(513, 135, 'tech_start_date', '20180510'),
(514, 135, '_tech_start_date', 'field_6635094313c6c'),
(515, 136, '_edit_last', '1'),
(516, 136, '_edit_lock', '1715356365:1'),
(517, 136, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 24 24\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\" role=\"img\"><path d=\"M12.001,4.8c-3.2,0-5.2,1.6-6,4.8c1.2-1.6,2.6-2.2,4.2-1.8c0.913,0.228,1.565,0.89,2.288,1.624 C13.666,10.618,15.027,12,18.001,12c3.2,0,5.2-1.6,6-4.8c-1.2,1.6-2.6,2.2-4.2,1.8c-0.913-0.228-1.565-0.89-2.288-1.624 C16.337,6.182,14.976,4.8,12.001,4.8z M6.001,12c-3.2,0-5.2,1.6-6,4.8c1.2-1.6,2.6-2.2,4.2-1.8c0.913,0.228,1.565,0.89,2.288,1.624 c1.177,1.194,2.538,2.576,5.512,2.576c3.2,0,5.2-1.6,6-4.8c-1.2,1.6-2.6,2.2-4.2,1.8c-0.913-0.228-1.565-0.89-2.288-1.624 C10.337,13.382,8.976,12,6.001,12z\"></path></svg>'),
(518, 136, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(519, 136, 'tech_start_date', '20240510'),
(520, 136, '_tech_start_date', 'field_6635094313c6c'),
(521, 137, '_edit_last', '1'),
(522, 137, '_edit_lock', '1715356488:1'),
(523, 137, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 24 24\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\" role=\"img\"><path d=\"m1.092 15.088c.789.243 4.098 1.005 4.098 1.005.198.061.139.21.139.21-.228 1.798-.178 3.17-.178 3.644 0 .21-.153.18-.153.18h-4.83c-.209 0-.164-.19-.164-.19.04-.599.212-2.303.878-4.746 0 0 .033-.157.21-.103zm21.816 0c-.789.243-4.098 1.005-4.098 1.005-.198.061-.139.21-.139.21.228 1.798.178 3.17.178 3.644 0 .21.153.18.153.18h4.83c.21 0 .164-.19.164-.19-.04-.599-.212-2.303-.878-4.746 0 0-.034-.157-.21-.103zm-1.929-5.354-3.448 1.667c-.164.063-.082.212-.082.212.476 1.134.766 2.091.99 3.251.038.194.169.132.169.132l3.879-1.684s.116-.044.068-.193c-.172-.531-1.05-2.649-1.402-3.341 0 0-.062-.105-.174-.044zm-17.958 0 3.448 1.667c.164.063.082.212.082.212-.476 1.134-.766 2.091-.991 3.251-.037.194-.169.132-.169.132l-3.878-1.684s-.116-.044-.068-.193c.172-.531 1.05-2.649 1.402-3.341 0 0 .062-.105.174-.044zm4.085-4.368 2.302 2.681c.099.128-.032.222-.032.222-.923.498-1.59 1.25-2.161 2.111-.114.17-.236.046-.236.046l-2.917-2.184s-.126-.074-.016-.22c.854-1.134 1.63-1.934 2.871-2.689 0 0 .094-.089.189.033zm9.788 0-2.302 2.681c-.099.128.032.222.032.222.923.498 1.59 1.25 2.161 2.111.114.17.236.046.236.046l2.917-2.184s.126-.074.016-.22c-.854-1.134-1.63-1.934-2.871-2.689 0 0-.094-.089-.189.033zm-4.894 2.295c.388 0 1.105.037 1.444.093.177.03.221-.088.221-.088l1.449-3.028s.097-.114-.106-.188c-1.082-.396-1.657-.578-3.008-.578-1.335 0-1.926.182-3.008.578-.203.074-.106.188-.106.188l1.449 3.028s.044.118.221.088c.339-.056 1.056-.093 1.444-.093z\"></path></svg>'),
(524, 137, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(525, 137, 'tech_start_date', '20240510'),
(526, 137, '_tech_start_date', 'field_6635094313c6c'),
(527, 138, '_edit_last', '1'),
(528, 138, '_edit_lock', '1715357290:1'),
(529, 138, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" viewBox=\"0 0 256 512\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M209.8 391.1l-14.1 24.6-4.6 80.2c0 8.9-28.3 16.1-63.1 16.1s-63.1-7.2-63.1-16.1l-5.8-79.4-14.9-25.4c41.2 17.3 126 16.7 165.6 0zm-196-253.3l13.6 125.5c5.9-20 20.8-47 40-55.2 6.3-2.7 12.7-2.7 18.7.9 5.2 3 9.6 9.3 10.1 11.8 1.2 6.5-2 9.1-4.5 9.1-3 0-5.3-4.6-6.8-7.3-4.1-7.3-10.3-7.6-16.9-2.8-6.9 5-12.9 13.4-17.1 20.7-5.1 8.8-9.4 18.5-12 28.2-1.5 5.6-2.9 14.6-.6 19.9 1 2.2 2.5 3.6 4.9 3.6 5 0 12.3-6.6 15.8-10.1 4.5-4.5 10.3-11.5 12.5-16l5.2-15.5c2.6-6.8 9.9-5.6 9.9 0 0 10.2-3.7 13.6-10 34.7-5.8 19.5-7.6 25.8-7.6 25.8-.7 2.8-3.4 7.5-6.3 7.5-1.2 0-2.1-.4-2.6-1.2-1-1.4-.9-5.3-.8-6.3.2-3.2 6.3-22.2 7.3-25.2-2 2.2-4.1 4.4-6.4 6.6-5.4 5.1-14.1 11.8-21.5 11.8-3.4 0-5.6-.9-7.7-2.4l7.6 79.6c2 5 39.2 17.1 88.2 17.1 49.1 0 86.3-12.2 88.2-17.1l10.9-94.6c-5.7 5.2-12.3 11.6-19.6 14.8-5.4 2.3-17.4 3.8-17.4-5.7 0-5.2 9.1-14.8 14.4-21.5 1.4-1.7 4.7-5.9 4.7-8.1 0-2.9-6-2.2-11.7 2.5-3.2 2.7-6.2 6.3-8.7 9.7-4.3 6-6.6 11.2-8.5 15.5-6.2 14.2-4.1 8.6-9.1 22-5 13.3-4.2 11.8-5.2 14-.9 1.9-2.2 3.5-4 4.5-1.9 1-4.5.9-6.1-.3-.9-.6-1.3-1.9-1.3-3.7 0-.9.1-1.8.3-2.7 1.5-6.1 7.8-18.1 15-34.3 1.6-3.7 1-2.6.8-2.3-6.2 6-10.9 8.9-14.4 10.5-5.8 2.6-13 2.6-14.5-4.1-.1-.4-.1-.8-.2-1.2-11.8 9.2-24.3 11.7-20-8.1-4.6 8.2-12.6 14.9-22.4 14.9-4.1 0-7.1-1.4-8.6-5.1-2.3-5.5 1.3-14.9 4.6-23.8 1.7-4.5 4-9.9 7.1-16.2 1.6-3.4 4.2-5.4 7.6-4.5.6.2 1.1.4 1.6.7 2.6 1.8 1.6 4.5.3 7.2-3.8 7.5-7.1 13-9.3 20.8-.9 3.3-2 9 1.5 9 2.4 0 4.7-.8 6.9-2.4 4.6-3.4 8.3-8.5 11.1-13.5 2-3.6 4.4-8.3 5.6-12.3.5-1.7 1.1-3.3 1.8-4.8 1.1-2.5 2.6-5.1 5.2-5.1 1.3 0 2.4.5 3.2 1.5 1.7 2.2 1.3 4.5.4 6.9-2 5.6-4.7 10.6-6.9 16.7-1.3 3.5-2.7 8-2.7 11.7 0 3.4 3.7 2.6 6.8 1.2 2.4-1.1 4.8-2.8 6.8-4.5 1.2-4.9.9-3.8 26.4-68.2 1.3-3.3 3.7-4.7 6.1-4.7 1.2 0 2.2.4 3.2 1.1 1.7 1.3 1.7 4.1 1 6.2-.7 1.9-.6 1.3-4.5 10.5-5.2 12.1-8.6 20.8-13.2 31.9-1.9 4.6-7.7 18.9-8.7 22.3-.6 2.2-1.3 5.8 1 5.8 5.4 0 19.3-13.1 23.1-17 .2-.3.5-.4.9-.6.6-1.9 1.2-3.7 1.7-5.5 1.4-3.8 2.7-8.2 5.3-11.3.8-1 1.7-1.6 2.7-1.6 2.8 0 4.2 1.2 4.2 4 0 1.1-.7 5.1-1.1 6.2 1.4-1.5 2.9-3 4.5-4.5 15-13.9 25.7-6.8 25.7.2 0 7.4-8.9 17.7-13.8 23.4-1.6 1.9-4.9 5.4-5 6.4 0 1.3.9 1.8 2.2 1.8 2 0 6.4-3.5 8-4.7 5-3.9 11.8-9.9 16.6-14.1l14.8-136.8c-30.5 17.1-197.6 17.2-228.3.2zm229.7-8.5c0 21-231.2 21-231.2 0 0-8.8 51.8-15.9 115.6-15.9 9 0 17.8.1 26.3.4l12.6-48.7L228.1.6c1.4-1.4 5.8-.2 9.9 3.5s6.6 7.9 5.3 9.3l-.1.1L185.9 74l-10 40.7c39.9 2.6 67.6 8.1 67.6 14.6zm-69.4 4.6c0-.8-.9-1.5-2.5-2.1l-.2.8c0 1.3-5 2.4-11.1 2.4s-11.1-1.1-11.1-2.4c0-.1 0-.2.1-.3l.2-.7c-1.8.6-3 1.4-3 2.3 0 2.1 6.2 3.7 13.7 3.7 7.7.1 13.9-1.6 13.9-3.7z\"></path></svg>'),
(530, 138, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(531, 138, 'tech_start_date', '20240510'),
(532, 138, '_tech_start_date', 'field_6635094313c6c'),
(533, 139, '_edit_last', '1'),
(534, 139, '_edit_lock', '1715476182:1'),
(535, 139, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" role=\"img\" viewBox=\"0 0 24 24\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M4.25.3C1.9.3 0 2.2 0 4.55v14.9c0 2.35 1.9 4.25 4.25 4.25h15.5c2.35 0 4.25-1.9 4.25-4.25V4.55C24 2.2 22.1.3 19.75.3Zm14.07 5.13h2.03c.05-.01.09.03.1.07v9.54c0 .18.01.38.02.6.02.21.03.41.04.58 0 .07-.03.13-.1.16-.52.22-1.07.38-1.63.48-.51.09-1.02.14-1.54.14-.74.01-1.48-.14-2.15-.45-.63-.29-1.15-.77-1.51-1.36-.37-.61-.55-1.37-.55-2.28a4.107 4.107 0 0 1 2.14-3.66c.7-.39 1.54-.58 2.53-.58.05 0 .12 0 .21.01s.19.01.31.02V5.54c0-.07.03-.11.1-.11zM3.68 6.3h2.27c.05 0 .1.01.14.02.04.02.07.05.1.09.19.43.41.86.64 1.29.24.43.47.85.72 1.27.24.42.46.84.67 1.27h.02c.21-.44.43-.87.65-1.29.22-.42.45-.84.68-1.26.23-.42.45-.85.67-1.26.01-.04.03-.08.06-.1a.19.19 0 0 1 .13-.02h2.11c.05-.01.1.02.11.07.01.01-.01.05-.03.07l-3 4.95 3.2 5.25c.02.04.03.08.02.12-.01.04-.05.01-.11.02h-2.29c-.16 0-.27-.01-.34-.11-.21-.42-.43-.83-.64-1.25-.21-.41-.44-.83-.68-1.26-.24-.43-.48-.86-.72-1.3h-.02c-.21.43-.44.86-.67 1.29-.23.43-.46.86-.68 1.28-.23.42-.46.85-.69 1.26-.04.1-.12.11-.23.11h-2.2c-.04 0-.07.02-.07-.03a.14.14 0 0 1 .02-.11l3.11-5.1L3.6 6.44c-.03-.04-.04-.08-.02-.1.02-.03.06-.04.1-.04zm13.94 4.23c-.39 0-.78.08-1.13.26-.34.17-.63.42-.85.74-.22.32-.33.75-.33 1.27-.01.35.05.7.17 1.03.1.27.25.51.45.71.19.18.42.32.68.4.27.09.55.13.83.13.15 0 .29-.01.42-.02.13.01.24-.01.36-.05v-4.4c-.09-.02-.18-.04-.27-.05-.11-.01-.22-.02-.33-.02Z\"></path></svg>'),
(536, 139, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(537, 139, 'tech_start_date', '20240510'),
(538, 139, '_tech_start_date', 'field_6635094313c6c'),
(539, 113, '_wp_old_slug', 'dashboard'),
(540, 111, '_wp_old_slug', 'kasper'),
(541, 110, '_wp_old_slug', 'leon'),
(542, 109, '_wp_old_slug', 'my-agency'),
(543, 106, '_wp_old_slug', 'real-estate'),
(544, 108, '_wp_old_slug', 'nike'),
(545, 145, '_edit_lock', '1715527310:1'),
(546, 145, '_edit_last', '1'),
(547, 145, 'project_sections', ''),
(548, 145, '_project_sections', 'field_66361fdaecf6d'),
(549, 145, 'project_technologies', 'a:7:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:3:\"136\";i:3;s:2:\"65\";i:4;s:2:\"67\";i:5;s:2:\"74\";i:6;s:2:\"75\";}'),
(550, 145, '_project_technologies', 'field_66361e51b6c7a'),
(551, 145, 'project_live_project_url', 'https://gym-landing-page34.vercel.app/'),
(552, 145, '_project_live_project_url', 'field_66361edfb6c7b'),
(553, 145, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/gym-landing-page'),
(554, 145, '_project_gitlab_url', 'field_66361f1db6c7c'),
(558, 146, '_edit_lock', '1715526009:1'),
(559, 146, '_edit_last', '1'),
(560, 146, 'project_sections', ''),
(561, 146, '_project_sections', 'field_66361fdaecf6d'),
(562, 146, 'project_technologies', 'a:6:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"67\";i:4;s:2:\"74\";i:5;s:2:\"75\";}'),
(563, 146, '_project_technologies', 'field_66361e51b6c7a'),
(564, 146, 'project_live_project_url', 'https://book-store34.vercel.app/'),
(565, 146, '_project_live_project_url', 'field_66361edfb6c7b'),
(566, 146, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/book-store'),
(567, 146, '_project_gitlab_url', 'field_66361f1db6c7c'),
(568, 147, '_edit_lock', '1715526542:1'),
(569, 147, '_edit_last', '1'),
(570, 147, 'project_sections', ''),
(571, 147, '_project_sections', 'field_66361fdaecf6d'),
(572, 147, 'project_technologies', 'a:5:{i:0;s:2:\"62\";i:1;s:2:\"64\";i:2;s:2:\"65\";i:3;s:2:\"74\";i:4;s:2:\"75\";}'),
(573, 147, '_project_technologies', 'field_66361e51b6c7a'),
(574, 147, 'project_live_project_url', 'https://clone-youtube34.vercel.app/'),
(575, 147, '_project_live_project_url', 'field_66361edfb6c7b'),
(576, 147, 'project_gitlab_url', 'https://gitlab.com/elachhabjawad/clone-youtube'),
(577, 147, '_project_gitlab_url', 'field_66361f1db6c7c'),
(578, 148, '_edit_last', '1'),
(579, 148, '_edit_lock', '1715476024:1'),
(580, 148, 'tech_icon_svg', '<svg stroke=\"currentColor\" fill=\"currentColor\" stroke-width=\"0\" role=\"img\" viewBox=\"0 0 24 24\" height=\"1em\" width=\"1em\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M9.85 8.42c-.37-.15-.77-.21-1.18-.2-.26 0-.49 0-.68.01-.2-.01-.34 0-.41.01v3.36c.14.01.27.02.39.02h.53c.39 0 .78-.06 1.15-.18.32-.09.6-.28.82-.53.21-.25.31-.59.31-1.03.01-.31-.07-.62-.23-.89-.17-.26-.41-.46-.7-.57zM19.75.3H4.25C1.9.3 0 2.2 0 4.55v14.899c0 2.35 1.9 4.25 4.25 4.25h15.5c2.35 0 4.25-1.9 4.25-4.25V4.55C24 2.2 22.1.3 19.75.3zm-7.391 11.65c-.399.56-.959.98-1.609 1.22-.68.25-1.43.34-2.25.34-.24 0-.4 0-.5-.01s-.24-.01-.43-.01v3.209c.01.07-.04.131-.11.141H5.52c-.08 0-.12-.041-.12-.131V6.42c0-.07.03-.11.1-.11.17 0 .33 0 .56-.01.24-.01.49-.01.76-.02s.56-.01.87-.02c.31-.01.61-.01.91-.01.82 0 1.5.1 2.06.31.5.17.96.45 1.34.82.32.32.57.71.73 1.14.149.42.229.85.229 1.3.001.86-.199 1.57-.6 2.13zm7.091 3.89c-.28.4-.671.709-1.12.891-.49.209-1.09.318-1.811.318-.459 0-.91-.039-1.359-.129-.35-.061-.7-.17-1.02-.32-.07-.039-.121-.109-.111-.189v-1.74c0-.029.011-.07.041-.09.029-.02.06-.01.09.01.39.23.8.391 1.24.49.379.1.779.15 1.18.15.38 0 .65-.051.83-.141.16-.07.27-.24.27-.42 0-.141-.08-.27-.24-.4-.16-.129-.489-.279-.979-.471-.51-.18-.979-.42-1.42-.719-.31-.221-.569-.51-.761-.85-.159-.32-.239-.67-.229-1.021 0-.43.12-.84.341-1.21.25-.4.619-.72 1.049-.92.469-.239 1.059-.349 1.769-.349.41 0 .83.03 1.24.09.3.04.59.12.86.23.039.01.08.05.1.09.01.04.02.08.02.12v1.63c0 .04-.02.08-.05.1-.09.02-.14.02-.18 0-.3-.16-.62-.27-.96-.34-.37-.08-.74-.13-1.12-.13-.2-.01-.41.02-.601.07-.129.03-.24.1-.31.2-.05.08-.08.18-.08.27s.04.18.101.26c.09.11.209.2.34.27.229.12.47.23.709.33.541.18 1.061.43 1.541.73.33.209.6.49.789.83.16.318.24.67.23 1.029.011.471-.129.94-.389 1.331z\"></path></svg>'),
(581, 148, '_tech_icon_svg', 'field_6637a2b8cb1ea'),
(582, 148, 'tech_start_date', '20240510'),
(583, 148, '_tech_start_date', 'field_6635094313c6c'),
(584, 149, '_wp_attached_file', '2024/05/YouTube-Clone-UI.png'),
(585, 149, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:28:\"2024/05/YouTube-Clone-UI.png\";s:8:\"filesize\";i:409586;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:28:\"YouTube-Clone-UI-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:69096;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:28:\"YouTube-Clone-UI-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:29682;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:28:\"YouTube-Clone-UI-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:319651;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(586, 147, '_thumbnail_id', '149'),
(587, 150, '_wp_attached_file', '2024/05/Book-Store.png'),
(588, 150, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:22:\"2024/05/Book-Store.png\";s:8:\"filesize\";i:127179;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:22:\"Book-Store-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:28806;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:22:\"Book-Store-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:10560;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:22:\"Book-Store-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:118743;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(589, 146, '_thumbnail_id', '150'),
(599, 155, '_wp_attached_file', '2024/05/Fitness-Gym-1.png'),
(600, 155, '_wp_attachment_metadata', 'a:6:{s:5:\"width\";i:1024;s:6:\"height\";i:768;s:4:\"file\";s:25:\"2024/05/Fitness-Gym-1.png\";s:8:\"filesize\";i:685409;s:5:\"sizes\";a:3:{s:6:\"medium\";a:5:{s:4:\"file\";s:25:\"Fitness-Gym-1-300x225.png\";s:5:\"width\";i:300;s:6:\"height\";i:225;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:72245;}s:9:\"thumbnail\";a:5:{s:4:\"file\";s:25:\"Fitness-Gym-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:29906;}s:12:\"medium_large\";a:5:{s:4:\"file\";s:25:\"Fitness-Gym-1-768x576.png\";s:5:\"width\";i:768;s:6:\"height\";i:576;s:9:\"mime-type\";s:9:\"image/png\";s:8:\"filesize\";i:421526;}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(601, 145, '_thumbnail_id', '155');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_posts`
--

CREATE TABLE `portfolio_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext NOT NULL,
  `post_title` text NOT NULL,
  `post_excerpt` text NOT NULL,
  `post_status` varchar(20) NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) NOT NULL DEFAULT 'open',
  `post_password` varchar(255) NOT NULL DEFAULT '',
  `post_name` varchar(200) NOT NULL DEFAULT '',
  `to_ping` text NOT NULL,
  `pinged` text NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `guid` varchar(255) NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT 0,
  `post_type` varchar(20) NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_posts`
--

INSERT INTO `portfolio_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(4, 0, '2024-04-30 08:50:16', '2024-04-30 08:50:16', '<!-- wp:page-list /-->', 'Navigation', '', 'publish', 'closed', 'closed', '', 'navigation', '', '', '2024-04-30 08:50:16', '2024-04-30 08:50:16', '', 0, 'http://dev.portfolio-api.com/index.php/2024/04/30/navigation/', 0, 'wp_navigation', '', 0),
(10, 1, '2024-04-30 08:56:50', '2024-04-30 08:56:50', '{\"version\": 2, \"isGlobalStylesUserThemeJSON\": true }', 'Custom Styles', '', 'publish', 'closed', 'closed', '', 'wp-global-styles-twentytwentyfour', '', '', '2024-04-30 08:56:50', '2024-04-30 08:56:50', '', 0, 'http://dev.portfolio-api.com/wp-global-styles-twentytwentyfour/', 0, 'wp_global_styles', '', 0),
(11, 1, '2024-04-30 08:59:27', '2024-04-30 08:59:27', '<h2>What is ES6?</h2>\r\nES6 or the ECMAScript 2015 is the 6th and major edition of the ECMAScript language specification standard. It defines\r\nthe standard for the implementation of JavaScript and it has become much more popular than the previous edition ES5.\r\n\r\nES6 comes with significant changes to the JavaScript language. It brought several new features like, let and const\r\nkeyword, rest and spread operators, template literals, classes, modules and many other enhancements to make\r\nJavaScript programming easier and more fun. In this article, we will discuss some of the best and most popular ES6\r\nfeatures that we can use in your everyday JavaScript coding\r\n<ol>\r\n 	<li>let and const Keywords</li>\r\n 	<li>Arrow Functions</li>\r\n 	<li>Multi-line Strings</li>\r\n 	<li>Default Parameters</li>\r\n 	<li>Template Literals</li>\r\n 	<li>Destructuring Assignment</li>\r\n 	<li>Enhanced Object Literals</li>\r\n 	<li>Promises</li>\r\n 	<li>Classes</li>\r\n 	<li>Modules</li>\r\n</ol>\r\n<h2>Understanding these Features</h2>\r\n<h3>let and const keywords :</h3>\r\nThe keyword \"let\" enables the users to define variables and on the other hand, \"const\" enables the users to\r\ndefine constants. Variables were previously declared using \"var\" which had function scope and were hoisted to the\r\ntop. It means that a variable can be used before declaration. But, the \"let\" variables and constants have block\r\nscope which is surrounded by curly-braces \"{}\" and cannot be used before declaration.\r\n<pre><code>let i = 10; console.log(i); //Output 10 const PI = 3.14; console.log(PI); //Output 3.14 </code></pre>\r\n<h3>Arrow Functions</h3>\r\nES6 provides a feature known as Arrow Functions. It provides a more concise syntax for writing function expressions\r\nby removing the \"function\" and \"return\" keywords.\r\n\r\nArrow functions are defined using the fat arrow (=&gt;) notation.\r\n<pre><code>// Arrow function let sumOfTwoNumbers = (a, b) =&gt; a + b; console.log(sum(10, 20)); // Output 30 </code></pre>\r\nIt is evident that there is no \"return\" or \"function\" keyword in the arrow function declaration.\r\n\r\nWe can also skip the parenthesis in the case when there is exactly one parameter, but will always need to use it when\r\nyou have zero or more than one parameter.\r\n\r\nBut, if there are multiple expressions in the function body, then we need to wrap it with curly braces (\"{}\"). We\r\nalso need to use the \"return\" statement to return the required value.\r\n<h3>Multi-line Strings</h3>\r\nES6 also provides Multi-line Strings. Users can create multi-line strings by using back-ticks(`).\r\n\r\nIt can be done as shown below :\r\n<pre><code>let greeting = `Hello World, Greetings to all, Keep Learning and Practicing!` </code></pre>\r\n<h3>Default Parameters</h3>\r\nIn ES6, users can provide the default values right in the signature of the functions. But, in ES5, OR operator had to\r\nbe used.\r\n<pre><code>//ES6 let calculateArea = function(height = 100, width = 50) { // logic } //ES5 var calculateArea = function(height, width) { height = height || 50; width = width || 80; // logic } </code></pre>\r\n<h3>Template Literals</h3>\r\nES6 introduces very simple string templates along with placeholders for the variables. The syntax for using the\r\nstring template is ${PARAMETER} and is used inside of the back-ticked string\r\n<pre><code>let name = `My name is ${firstName} ${lastName}` </code></pre>\r\n<h3>Destructuring Assignment</h3>\r\nDestructuring is one of the most popular features of ES6. The destructuring assignment is an expression that makes it\r\neasy to extract values from arrays, or properties from objects, into distinct variables.\r\n\r\nThere are two types of destructuring assignment expressions, namely, Array Destructuring and Object Destructuring. It\r\ncan be used in the following manner :\r\n<pre><code>//Array Destructuring let fruits = [\"Apple\", \"Banana\"]; let [a, b] = fruits; // Array destructuring assignment console.log(a, b); //Object Destructuring let person = {name: \"Peter\", age: 28}; let {name, age} = person; // Object destructuring assignment console.log(name, age); </code></pre>\r\n<h3>Enhanced Object Literals</h3>\r\nES6 provides enhanced object literals which make it easy to quickly create objects with properties inside the curly\r\nbraces.\r\n<pre><code>function getMobile(manufacturer, model, year) { return { manufacturer, model, year } } getMobile(\"Samsung\", \"Galaxy\", \"2020\"); </code></pre>\r\n<h3>Promises</h3>\r\nIn ES6, Promises are used for asynchronous execution. We can use promise with the arrow function as demonstrated\r\nbelow.\r\n<pre><code>var asyncCall = new Promise((resolve, reject) =&gt; { // do something resolve();}).then(()=&gt; { console.log(\'DON!\');})</code></pre>\r\n<h3>Classes</h3>\r\nPreviously, classes never existed in JavaScript. Classes are introduced in ES6 which looks similar to classes in\r\nother object-oriented languages, such as C++, Java, PHP, etc. But, they do not work exactly the same way. ES6\r\nclasses make it simpler to create objects, implement inheritance by using the extends\" keyword and also reuse the\r\ncode efficiently.\r\n\r\nIn ES6, we can declare a class using the new \"class\" keyword followed by the name of the class.\r\n<pre><code>class UserProfile { constructor(firstName, lastName) { this.firstName = firstName; this.lastName = lastName; } getName() { console.log(`The Full-Name is ${this.firstName} ${this.lastName}`); } }let obj = new UserProfile(\'John\', \'Smith\');obj.getName(); // output: The Full-Name is John Smith</code></pre>\r\n<h3>Modules</h3>\r\nPreviously, there was no native support for modules in JavaScript. ES6 introduced a new feature called modules, in\r\nwhich each module is represented by a separate \".js\" file. We can use the \"import\" or \"export\" statement in a\r\nmodule to import or export variables, functions, classes or any other component from/to different files and modules.\r\n<pre><code>export var num = 50; export function getName(fullName) { //data};</code></pre>\r\n<pre><code>import {num, getName} from \'module\';console.log(num); // 50</code></pre>\r\n<h2>Summary</h2>\r\n<ul>\r\n 	<li>In this article, we learned about the ECMAScript 2015 or ES6, which defines the standard for JavaScript\r\nimplementation.</li>\r\n 	<li>We also learned about the top 10 features of ES6 which makes it very popular.</li>\r\n 	<li>ES6 provides classes, modules, arrow functions, template literals, destructuring assignments and many more\r\nfeatures which make working with JavaScript a lot easier.</li>\r\n</ul>', 'Top 10 Features of ES6', '', 'publish', 'open', 'open', '', 'top-10-features-of-es6', '', '', '2024-04-30 09:06:22', '2024-04-30 09:06:22', '', 0, 'http://dev.portfolio-api.com/?p=11', 0, 'post', '', 0),
(12, 1, '2024-04-30 08:59:27', '2024-04-30 08:59:27', '', 'Top 10 Features of ES6', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2024-04-30 08:59:27', '2024-04-30 08:59:27', '', 11, 'http://dev.portfolio-api.com/?p=12', 0, 'revision', '', 0),
(13, 1, '2024-04-30 09:05:40', '2024-04-30 09:05:40', '<h2>What is ES6?</h2>\r\nES6 or the ECMAScript 2015 is the 6th and major edition of the ECMAScript language specification standard. It defines\r\nthe standard for the implementation of JavaScript and it has become much more popular than the previous edition ES5.\r\n\r\nES6 comes with significant changes to the JavaScript language. It brought several new features like, let and const\r\nkeyword, rest and spread operators, template literals, classes, modules and many other enhancements to make\r\nJavaScript programming easier and more fun. In this article, we will discuss some of the best and most popular ES6\r\nfeatures that we can use in your everyday JavaScript coding\r\n<ol>\r\n 	<li>let and const Keywords</li>\r\n 	<li>Arrow Functions</li>\r\n 	<li>Multi-line Strings</li>\r\n 	<li>Default Parameters</li>\r\n 	<li>Template Literals</li>\r\n 	<li>Destructuring Assignment</li>\r\n 	<li>Enhanced Object Literals</li>\r\n 	<li>Promises</li>\r\n 	<li>Classes</li>\r\n 	<li>Modules</li>\r\n</ol>\r\n<h2>Understanding these Features</h2>\r\n<h3>let and const keywords :</h3>\r\nThe keyword \"let\" enables the users to define variables and on the other hand, \"const\" enables the users to\r\ndefine constants. Variables were previously declared using \"var\" which had function scope and were hoisted to the\r\ntop. It means that a variable can be used before declaration. But, the \"let\" variables and constants have block\r\nscope which is surrounded by curly-braces \"{}\" and cannot be used before declaration.\r\n<pre><code>let i = 10; console.log(i); //Output 10 const PI = 3.14; console.log(PI); //Output 3.14 </code></pre>\r\n<h3>Arrow Functions</h3>\r\nES6 provides a feature known as Arrow Functions. It provides a more concise syntax for writing function expressions\r\nby removing the \"function\" and \"return\" keywords.\r\n\r\nArrow functions are defined using the fat arrow (=&gt;) notation.\r\n<pre><code>// Arrow function let sumOfTwoNumbers = (a, b) =&gt; a + b; console.log(sum(10, 20)); // Output 30 </code></pre>\r\nIt is evident that there is no \"return\" or \"function\" keyword in the arrow function declaration.\r\n\r\nWe can also skip the parenthesis in the case when there is exactly one parameter, but will always need to use it when\r\nyou have zero or more than one parameter.\r\n\r\nBut, if there are multiple expressions in the function body, then we need to wrap it with curly braces (\"{}\"). We\r\nalso need to use the \"return\" statement to return the required value.\r\n<h3>Multi-line Strings</h3>\r\nES6 also provides Multi-line Strings. Users can create multi-line strings by using back-ticks(`).\r\n\r\nIt can be done as shown below :\r\n<pre><code>let greeting = `Hello World, Greetings to all, Keep Learning and Practicing!` </code></pre>\r\n<h3>Default Parameters</h3>\r\nIn ES6, users can provide the default values right in the signature of the functions. But, in ES5, OR operator had to\r\nbe used.\r\n<pre><code>//ES6 let calculateArea = function(height = 100, width = 50) { // logic } //ES5 var calculateArea = function(height, width) { height = height || 50; width = width || 80; // logic } </code></pre>\r\n<h3>Template Literals</h3>\r\nES6 introduces very simple string templates along with placeholders for the variables. The syntax for using the\r\nstring template is ${PARAMETER} and is used inside of the back-ticked string\r\n<pre><code>let name = `My name is ${firstName} ${lastName}` </code></pre>\r\n<h3>Destructuring Assignment</h3>\r\nDestructuring is one of the most popular features of ES6. The destructuring assignment is an expression that makes it\r\neasy to extract values from arrays, or properties from objects, into distinct variables.\r\n\r\nThere are two types of destructuring assignment expressions, namely, Array Destructuring and Object Destructuring. It\r\ncan be used in the following manner :\r\n<pre><code>//Array Destructuring let fruits = [\"Apple\", \"Banana\"]; let [a, b] = fruits; // Array destructuring assignment console.log(a, b); //Object Destructuring let person = {name: \"Peter\", age: 28}; let {name, age} = person; // Object destructuring assignment console.log(name, age); </code></pre>\r\n<h3>Enhanced Object Literals</h3>\r\nES6 provides enhanced object literals which make it easy to quickly create objects with properties inside the curly\r\nbraces.\r\n<pre><code>function getMobile(manufacturer, model, year) { return { manufacturer, model, year } } getMobile(\"Samsung\", \"Galaxy\", \"2020\"); </code></pre>\r\n<h3>Promises</h3>\r\nIn ES6, Promises are used for asynchronous execution. We can use promise with the arrow function as demonstrated\r\nbelow.\r\n<pre><code>var asyncCall = new Promise((resolve, reject) =&gt; { // do something resolve();}).then(()=&gt; { console.log(\'DON!\');})</code></pre>\r\n<h3>Classes</h3>\r\nPreviously, classes never existed in JavaScript. Classes are introduced in ES6 which looks similar to classes in\r\nother object-oriented languages, such as C++, Java, PHP, etc. But, they do not work exactly the same way. ES6\r\nclasses make it simpler to create objects, implement inheritance by using the extends\" keyword and also reuse the\r\ncode efficiently.\r\n\r\nIn ES6, we can declare a class using the new \"class\" keyword followed by the name of the class.\r\n<pre><code>class UserProfile { constructor(firstName, lastName) { this.firstName = firstName; this.lastName = lastName; } getName() { console.log(`The Full-Name is ${this.firstName} ${this.lastName}`); } }let obj = new UserProfile(\'John\', \'Smith\');obj.getName(); // output: The Full-Name is John Smith</code></pre>\r\n<h3>Modules</h3>\r\nPreviously, there was no native support for modules in JavaScript. ES6 introduced a new feature called modules, in\r\nwhich each module is represented by a separate \".js\" file. We can use the \"import\" or \"export\" statement in a\r\nmodule to import or export variables, functions, classes or any other component from/to different files and modules.\r\n<pre><code>export var num = 50; export function getName(fullName) { //data};</code></pre>\r\n<pre><code>import {num, getName} from \'module\';console.log(num); // 50</code></pre>\r\n<h2>Summary</h2>\r\n<ul>\r\n 	<li>In this article, we learned about the ECMAScript 2015 or ES6, which defines the standard for JavaScript\r\nimplementation.</li>\r\n 	<li>We also learned about the top 10 features of ES6 which makes it very popular.</li>\r\n 	<li>ES6 provides classes, modules, arrow functions, template literals, destructuring assignments and many more\r\nfeatures which make working with JavaScript a lot easier.</li>\r\n</ul>', 'Top 10 Features of ES6', '', 'inherit', 'closed', 'closed', '', '11-revision-v1', '', '', '2024-04-30 09:05:40', '2024-04-30 09:05:40', '', 11, 'http://dev.portfolio-api.com/?p=13', 0, 'revision', '', 0),
(14, 1, '2024-04-30 09:06:17', '2024-04-30 09:06:17', '', 'top-10-features-of-es6.jpg', '', 'inherit', 'open', 'closed', '', 'top-10-features-of-es6-jpg', '', '', '2024-04-30 09:06:17', '2024-04-30 09:06:17', '', 11, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/top-10-features-of-es6.jpg.jpg', 0, 'attachment', 'image/jpeg', 0),
(15, 1, '2024-04-30 09:07:20', '2024-04-30 09:07:20', 'HTTP request methods are important components responsible for supplying the request and specifying the\r\nclient-requested operation. In this lesson, you will learn about various HTTP methods and how they are used.\r\n<h3>What Are HTTP Request Methods?</h3>\r\nThe Internet consists of a set of resources hosted on various web servers. To access these resources, users use\r\nvarious web browsers capable of sending requests to the server and displaying the information from it. HTTP is a\r\nprotocol that is used in requests and responses for effective communication between client and server. A message\r\nsent by the client to the server is known as an HTTP request. When these requests are being sent, clients can use\r\nvarious methods, known as HTTP request methods.\r\n<h2>What Are the Types of HTTP Request Methods?</h2>\r\nVarious HTTP methods (GET, POST, PUT, HEAD, DELETE, TRACE, OPTIONS, and CONNECT.) Are explained below.\r\n<h3>GET Method</h3>\r\nThe GET method is one of the most commonly used methods of HTTP. It is usually implemented to request a particular\r\nresource data from the Web server by specifying the parameters as a query string (name and value pairs) in the URL\r\npart of the request.\r\n\r\nThe various characteristics of the GET method are:\r\n<ul>\r\n 	<li>The GET requests can be bookmarked as they appear in the URL.</li>\r\n 	<li>The GET request can be cached.</li>\r\n 	<li>The GET request is saved in the browser history if it is executed using a web browser.</li>\r\n 	<li>There are character length restrictions (2048 characters maximum) for this method as they appear in the URL.</li>\r\n 	<li>The GET method cannot be used to send binary data such as images and Word documents.</li>\r\n 	<li>The data can only be retrieved from requests that use the GET method and have no other effect.</li>\r\n 	<li>When communicating sensitive data such as login credentials, a GET request should not be used as it appears in\r\nthe URL, making it less secure.</li>\r\n 	<li>Since the GET request only requests data and does not modify any resources, it is considered a safe and ideal\r\nmethod to request data only.</li>\r\n</ul>\r\n<h3>POST Method</h3>\r\nThe POST method is a commonly used HTTP method that sends data to the Web server in the request body of HTTP. The\r\nvarious characteristics of the POST method are:\r\n<ul>\r\n 	<li>The POST requests cannot be bookmarked as they do not appear in the URL.</li>\r\n 	<li>The POST requests do not get cached.</li>\r\n 	<li>The POST requests are not saved as history by the web browsers.</li>\r\n 	<li>There is no restriction on the amount of data to be sent in a POST request.</li>\r\n 	<li>The POST method can be used to send ASCII as well as binary data.</li>\r\n 	<li>When communicating sensitive data, such as when submitting an HTML form, a POST request must be used.</li>\r\n 	<li>The data sent by the POST method goes through the HTTP header, so security depends on the HTTP protocol. By\r\nusing secure</li>\r\n 	<li>HTTP (HTTPS), you can ensure that your information is protected.</li>\r\n</ul>\r\nHere is an example code snippet that submits an HTML form using the POST method:\r\n<pre><code>&lt;html&gt;\r\n&lt;body&gt;\r\n\r\n &lt;form action=\"registration.php\" method=\"post\"&gt;\r\n Name: &lt;input type=\"text\" name=\"name\"&gt;\r\n Email: &lt;input type=\"text\" name=\"email\"&gt;\r\n &lt;input type=\"submit\"&gt;\r\n &lt;/form&gt;\r\n\r\n&lt;/body&gt;\r\n&lt;/html&gt;</code></pre>\r\n<h3>PUT Method</h3>\r\nThe HTTP PUT request method is used to update existing resources with uploaded content or to create a new resource if\r\nthe target resource is not found. The difference between POST and PUT is that PUT requests are static, which means\r\ncalling the same PUT method multiple times will not yield a different result because it will update the same content\r\neach time while POST Request Will create new content each time.\r\n<h3>HEAD Method</h3>\r\nThe HTTP HEAD method is almost identical to the GET method, but the only difference is that it will not return any\r\nresponse body. For example, if GET/users return a record of users, then HEAD/users make the same request, but it\r\nwill not return any of the users’ records.\r\n\r\nThe HEAD request becomes useful for testing whether the GET request will actually respond before making the actual\r\nGET request. Sometimes this will be useful in some situations, such as checking the response of a URL before\r\ndownloading a file.\r\n<h3>DELETE Method</h3>\r\nThe HTTP DELETE method is used to delete any specific resource.\r\n<h3>TRACE Method</h3>\r\nThe HTTP TRACE method is used for performing a message loop-back, which tests the path for the target resource. It is\r\nuseful for debugging purposes\r\n<h3>OPTIONS Method</h3>\r\nHTTP OPTIONS method is used for describing the communication preferences for any target resource\r\n<h3>CONNECT Method</h3>\r\nHTTP CONNECT method is used for establishing a tunnel to the server recognized by a given URI.', 'HTTP request methods', '', 'publish', 'open', 'open', '', 'http-request-methods', '', '', '2024-04-30 09:07:42', '2024-04-30 09:07:42', '', 0, 'http://dev.portfolio-api.com/?p=15', 0, 'post', '', 0),
(16, 1, '2024-04-30 09:07:20', '2024-04-30 09:07:20', 'HTTP request methods are important components responsible for supplying the request and specifying the\r\nclient-requested operation. In this lesson, you will learn about various HTTP methods and how they are used.\r\n<h3>What Are HTTP Request Methods?</h3>\r\nThe Internet consists of a set of resources hosted on various web servers. To access these resources, users use\r\nvarious web browsers capable of sending requests to the server and displaying the information from it. HTTP is a\r\nprotocol that is used in requests and responses for effective communication between client and server. A message\r\nsent by the client to the server is known as an HTTP request. When these requests are being sent, clients can use\r\nvarious methods, known as HTTP request methods.\r\n<h2>What Are the Types of HTTP Request Methods?</h2>\r\nVarious HTTP methods (GET, POST, PUT, HEAD, DELETE, TRACE, OPTIONS, and CONNECT.) Are explained below.\r\n<h3>GET Method</h3>\r\nThe GET method is one of the most commonly used methods of HTTP. It is usually implemented to request a particular\r\nresource data from the Web server by specifying the parameters as a query string (name and value pairs) in the URL\r\npart of the request.\r\n\r\nThe various characteristics of the GET method are:\r\n<ul>\r\n 	<li>The GET requests can be bookmarked as they appear in the URL.</li>\r\n 	<li>The GET request can be cached.</li>\r\n 	<li>The GET request is saved in the browser history if it is executed using a web browser.</li>\r\n 	<li>There are character length restrictions (2048 characters maximum) for this method as they appear in the URL.</li>\r\n 	<li>The GET method cannot be used to send binary data such as images and Word documents.</li>\r\n 	<li>The data can only be retrieved from requests that use the GET method and have no other effect.</li>\r\n 	<li>When communicating sensitive data such as login credentials, a GET request should not be used as it appears in\r\nthe URL, making it less secure.</li>\r\n 	<li>Since the GET request only requests data and does not modify any resources, it is considered a safe and ideal\r\nmethod to request data only.</li>\r\n</ul>\r\n<h3>POST Method</h3>\r\nThe POST method is a commonly used HTTP method that sends data to the Web server in the request body of HTTP. The\r\nvarious characteristics of the POST method are:\r\n<ul>\r\n 	<li>The POST requests cannot be bookmarked as they do not appear in the URL.</li>\r\n 	<li>The POST requests do not get cached.</li>\r\n 	<li>The POST requests are not saved as history by the web browsers.</li>\r\n 	<li>There is no restriction on the amount of data to be sent in a POST request.</li>\r\n 	<li>The POST method can be used to send ASCII as well as binary data.</li>\r\n 	<li>When communicating sensitive data, such as when submitting an HTML form, a POST request must be used.</li>\r\n 	<li>The data sent by the POST method goes through the HTTP header, so security depends on the HTTP protocol. By\r\nusing secure</li>\r\n 	<li>HTTP (HTTPS), you can ensure that your information is protected.</li>\r\n</ul>\r\nHere is an example code snippet that submits an HTML form using the POST method:\r\n<pre><code>&lt;html&gt;\r\n&lt;body&gt;\r\n\r\n &lt;form action=\"registration.php\" method=\"post\"&gt;\r\n Name: &lt;input type=\"text\" name=\"name\"&gt;\r\n Email: &lt;input type=\"text\" name=\"email\"&gt;\r\n &lt;input type=\"submit\"&gt;\r\n &lt;/form&gt;\r\n\r\n&lt;/body&gt;\r\n&lt;/html&gt;</code></pre>\r\n<h3>PUT Method</h3>\r\nThe HTTP PUT request method is used to update existing resources with uploaded content or to create a new resource if\r\nthe target resource is not found. The difference between POST and PUT is that PUT requests are static, which means\r\ncalling the same PUT method multiple times will not yield a different result because it will update the same content\r\neach time while POST Request Will create new content each time.\r\n<h3>HEAD Method</h3>\r\nThe HTTP HEAD method is almost identical to the GET method, but the only difference is that it will not return any\r\nresponse body. For example, if GET/users return a record of users, then HEAD/users make the same request, but it\r\nwill not return any of the users’ records.\r\n\r\nThe HEAD request becomes useful for testing whether the GET request will actually respond before making the actual\r\nGET request. Sometimes this will be useful in some situations, such as checking the response of a URL before\r\ndownloading a file.\r\n<h3>DELETE Method</h3>\r\nThe HTTP DELETE method is used to delete any specific resource.\r\n<h3>TRACE Method</h3>\r\nThe HTTP TRACE method is used for performing a message loop-back, which tests the path for the target resource. It is\r\nuseful for debugging purposes\r\n<h3>OPTIONS Method</h3>\r\nHTTP OPTIONS method is used for describing the communication preferences for any target resource\r\n<h3>CONNECT Method</h3>\r\nHTTP CONNECT method is used for establishing a tunnel to the server recognized by a given URI.', 'HTTP request methods', '', 'inherit', 'closed', 'closed', '', '15-revision-v1', '', '', '2024-04-30 09:07:20', '2024-04-30 09:07:20', '', 15, 'http://dev.portfolio-api.com/?p=16', 0, 'revision', '', 0),
(17, 1, '2024-04-30 09:07:36', '2024-04-30 09:07:36', '', 'http-request-methods', '', 'inherit', 'open', 'closed', '', 'http-request-methods-2', '', '', '2024-04-30 09:07:36', '2024-04-30 09:07:36', '', 15, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/http-request-methods.jpg', 0, 'attachment', 'image/jpeg', 0),
(18, 1, '2024-04-30 09:13:47', '2024-04-30 09:13:47', 'Visual Studio Code (often abbreviated as VS Code) has become a beloved tool among programmers worldwide. In fact,\r\nit\r\nremains as the most preferred source-code editor according to this 2022 Stack Overflow survey. As a\r\ncross-platform\r\nintegrated development environment (IDE), it offers a wide array of features that cater to the needs of\r\ndevelopers\r\nacross various domains. However, many new users may not be fully aware of the productivity-enhancing\r\ncapabilities that\r\nlie within this versatile edito\r\n\r\nIn this article, we will explore 5essential shortcuts in VS Code that every beginner coder should know. These\r\nshortcuts\r\nwill help you navigate, edit, and debug your code more efficiently, saving you time and effort along the way.\r\n<h2>1. Quick Open</h2>\r\n<strong>\r\nWindows: CTRL+P\r\nMac: CMD+P\r\n</strong>\r\n\r\nThe Quick Open shortcut allows you to quickly navigate to files in your project. Press Ctrl/Cmd+P to open the\r\nQuick Open\r\npanel, then start typing the name of the file you want to open. VS Code will intelligently suggest matching\r\nfiles in\r\nreal-time, helping you jump directly to the desired file\r\n\r\n<img src=\"/wp-content/uploads/2024/04/quick-open.gif\" alt=\"\" />\r\n<h2>2. Toggle Sidebar</h2>\r\n<strong>\r\nWindows: CTRL+B\r\nMac: CMD+B\r\n</strong>\r\n\r\nThe sidebar in VS Code provides a convenient way to access file explorer, source control, extensions, and other\r\nuseful\r\npanels. Press Ctrl/Cmd+B to toggle the sidebar visibility, giving you more screen real estate for your code.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/toggle-sideba.gif\" alt=\"\" />\r\n<h2>3. Multi-Cursor Editing</h2>\r\n<strong>\r\nWindows: Ctrl+Alt+Arrow Keys\r\nMac: Opt+Cmd+Arrow Keys\r\n</strong>\r\n\r\nMulti-Cursor Editing is a powerful feature in VS Code that allows you to edit multiple lines simultaneously.\r\nPress\r\nCtrl+Alt+Up/Down Arrow Keys to add cursors above or below the current position. Pressing Esc will reset your\r\ncursor.\r\n\r\n<img src=\"/images/blog/multi-cursor-editing1.gif\" alt=\"\" />\r\n\r\nBonus: Holding Alt/Opt and left clicking will allow you to place multiple cursors where ever you click\r\n\r\n<img src=\"/wp-content/uploads/2024/04/multi-cursor-editing2.gif\" alt=\"\" />\r\n<h2>4. Format Document</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nStill not used to code formatting? Press Shift+Alt/Opt+F to automatically format the entire document according to\r\nthe\r\nconfigured code style. Maintaining consistent code formatting is crucial for readability and maintainability. VS\r\nCode\r\nsupports a wide range of programming languages and provides customizable formatting options.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/format-document.gif\" alt=\"\" />\r\n<h2>5. Comment/Uncomment Lines</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nCommenting code blocks is a necessity when experimenting with code or debugging. To comment or uncomment a single\r\nline\r\nsimply select the line and press Ctrl/Cmd+/ . For a block of code, highlight the desired code to comment out and\r\npress\r\nShift+Alt/Opt+A. This shortcut automatically adds or removes comment symbols based on the file type.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/comment-uncoment-lines.gif\" alt=\"\" />\r\n\r\nThe list above is by no means definitive and we’ve really only scratched the surface of all the keyboard\r\nshortcuts you\r\nneed to maximize your productivity in VS Code. Like with most tools and shortcuts, their usefulness varies\r\nperson to\r\nperson and job to job — find what works best for you. Check out some of the resources below to learn even more\r\nuseful\r\nshortcuts!', '5 Visual Studio Code Shortcuts you need to be using', '', 'publish', 'open', 'open', '', '5-visual-studio-code-shortcuts-you-need-to-be-using', '', '', '2024-04-30 09:15:59', '2024-04-30 09:15:59', '', 0, 'http://dev.portfolio-api.com/?p=18', 0, 'post', '', 0),
(19, 1, '2024-04-30 09:13:47', '2024-04-30 09:13:47', 'Visual Studio Code (often abbreviated as VS Code) has become a beloved tool among programmers worldwide. In fact,\r\nit\r\nremains as the most preferred source-code editor according to this 2022 Stack Overflow survey. As a\r\ncross-platform\r\nintegrated development environment (IDE), it offers a wide array of features that cater to the needs of\r\ndevelopers\r\nacross various domains. However, many new users may not be fully aware of the productivity-enhancing\r\ncapabilities that\r\nlie within this versatile edito\r\n\r\nIn this article, we will explore 5essential shortcuts in VS Code that every beginner coder should know. These\r\nshortcuts\r\nwill help you navigate, edit, and debug your code more efficiently, saving you time and effort along the way.\r\n<h2>1. Quick Open</h2>\r\n<strong>\r\nWindows: CTRL+P\r\nMac: CMD+P\r\n</strong>\r\n\r\nThe Quick Open shortcut allows you to quickly navigate to files in your project. Press Ctrl/Cmd+P to open the\r\nQuick Open\r\npanel, then start typing the name of the file you want to open. VS Code will intelligently suggest matching\r\nfiles in\r\nreal-time, helping you jump directly to the desired file\r\n\r\n<img src=\"/images/blog/quick-open.gif\" alt=\"\" />\r\n<h2>2. Toggle Sidebar</h2>\r\n<strong>\r\nWindows: CTRL+B\r\nMac: CMD+B\r\n</strong>\r\n\r\nThe sidebar in VS Code provides a convenient way to access file explorer, source control, extensions, and other\r\nuseful\r\npanels. Press Ctrl/Cmd+B to toggle the sidebar visibility, giving you more screen real estate for your code.\r\n\r\n<img src=\"/images/blog/toggle-sideba.gif\" alt=\"\" />\r\n<h2>3. Multi-Cursor Editing</h2>\r\n<strong>\r\nWindows: Ctrl+Alt+Arrow Keys\r\nMac: Opt+Cmd+Arrow Keys\r\n</strong>\r\n\r\nMulti-Cursor Editing is a powerful feature in VS Code that allows you to edit multiple lines simultaneously.\r\nPress\r\nCtrl+Alt+Up/Down Arrow Keys to add cursors above or below the current position. Pressing Esc will reset your\r\ncursor.\r\n\r\n<img src=\"/images/blog/multi-cursor-editing1.gif\" alt=\"\" />\r\n\r\nBonus: Holding Alt/Opt and left clicking will allow you to place multiple cursors where ever you click\r\n\r\n<img src=\"/images/blog/multi-cursor-editing2.gif\" alt=\"\" />\r\n<h2>4. Format Document</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nStill not used to code formatting? Press Shift+Alt/Opt+F to automatically format the entire document according to\r\nthe\r\nconfigured code style. Maintaining consistent code formatting is crucial for readability and maintainability. VS\r\nCode\r\nsupports a wide range of programming languages and provides customizable formatting options.\r\n\r\n<img src=\"/images/blog/format-document.gif\" alt=\"\" />\r\n<h2>5. Comment/Uncomment Lines</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nCommenting code blocks is a necessity when experimenting with code or debugging. To comment or uncomment a single\r\nline\r\nsimply select the line and press Ctrl/Cmd+/ . For a block of code, highlight the desired code to comment out and\r\npress\r\nShift+Alt/Opt+A. This shortcut automatically adds or removes comment symbols based on the file type.\r\n\r\n<img src=\"/images/blog/comment-uncoment-lines.gif\" alt=\"\" />\r\n\r\nThe list above is by no means definitive and we’ve really only scratched the surface of all the keyboard\r\nshortcuts you\r\nneed to maximize your productivity in VS Code. Like with most tools and shortcuts, their usefulness varies\r\nperson to\r\nperson and job to job — find what works best for you. Check out some of the resources below to learn even more\r\nuseful\r\nshortcuts!', '5 Visual Studio Code Shortcuts you need to be using', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2024-04-30 09:13:47', '2024-04-30 09:13:47', '', 18, 'http://dev.portfolio-api.com/?p=19', 0, 'revision', '', 0),
(20, 1, '2024-04-30 09:14:04', '2024-04-30 09:14:04', '', 'comment-uncoment-lines', '', 'inherit', 'open', 'closed', '', 'comment-uncoment-lines', '', '', '2024-04-30 09:14:04', '2024-04-30 09:14:04', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/comment-uncoment-lines.gif', 0, 'attachment', 'image/gif', 0),
(21, 1, '2024-04-30 09:14:05', '2024-04-30 09:14:05', '', 'format-document', '', 'inherit', 'open', 'closed', '', 'format-document', '', '', '2024-04-30 09:14:05', '2024-04-30 09:14:05', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/format-document.gif', 0, 'attachment', 'image/gif', 0),
(22, 1, '2024-04-30 09:14:08', '2024-04-30 09:14:08', '', 'multi-cursor-editing1', '', 'inherit', 'open', 'closed', '', 'multi-cursor-editing1', '', '', '2024-04-30 09:14:08', '2024-04-30 09:14:08', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/multi-cursor-editing1.gif', 0, 'attachment', 'image/gif', 0),
(23, 1, '2024-04-30 09:14:09', '2024-04-30 09:14:09', '', 'multi-cursor-editing2', '', 'inherit', 'open', 'closed', '', 'multi-cursor-editing2', '', '', '2024-04-30 09:14:09', '2024-04-30 09:14:09', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/multi-cursor-editing2.gif', 0, 'attachment', 'image/gif', 0),
(24, 1, '2024-04-30 09:14:10', '2024-04-30 09:14:10', '', 'quick-open', '', 'inherit', 'open', 'closed', '', 'quick-open', '', '', '2024-04-30 09:14:10', '2024-04-30 09:14:10', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/quick-open.gif', 0, 'attachment', 'image/gif', 0),
(25, 1, '2024-04-30 09:14:12', '2024-04-30 09:14:12', '', 'toggle-sideba', '', 'inherit', 'open', 'closed', '', 'toggle-sideba', '', '', '2024-04-30 09:14:12', '2024-04-30 09:14:12', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/toggle-sideba.gif', 0, 'attachment', 'image/gif', 0),
(26, 1, '2024-04-30 09:15:52', '2024-04-30 09:15:52', '', 'visual-studio-code', '', 'inherit', 'open', 'closed', '', 'visual-studio-code', '', '', '2024-04-30 09:15:52', '2024-04-30 09:15:52', '', 18, 'http://dev.portfolio-api.com/wp-content/uploads/2024/04/visual-studio-code.png', 0, 'attachment', 'image/png', 0),
(27, 1, '2024-04-30 09:15:59', '2024-04-30 09:15:59', 'Visual Studio Code (often abbreviated as VS Code) has become a beloved tool among programmers worldwide. In fact,\r\nit\r\nremains as the most preferred source-code editor according to this 2022 Stack Overflow survey. As a\r\ncross-platform\r\nintegrated development environment (IDE), it offers a wide array of features that cater to the needs of\r\ndevelopers\r\nacross various domains. However, many new users may not be fully aware of the productivity-enhancing\r\ncapabilities that\r\nlie within this versatile edito\r\n\r\nIn this article, we will explore 5essential shortcuts in VS Code that every beginner coder should know. These\r\nshortcuts\r\nwill help you navigate, edit, and debug your code more efficiently, saving you time and effort along the way.\r\n<h2>1. Quick Open</h2>\r\n<strong>\r\nWindows: CTRL+P\r\nMac: CMD+P\r\n</strong>\r\n\r\nThe Quick Open shortcut allows you to quickly navigate to files in your project. Press Ctrl/Cmd+P to open the\r\nQuick Open\r\npanel, then start typing the name of the file you want to open. VS Code will intelligently suggest matching\r\nfiles in\r\nreal-time, helping you jump directly to the desired file\r\n\r\n<img src=\"/wp-content/uploads/2024/04/quick-open.gif\" alt=\"\" />\r\n<h2>2. Toggle Sidebar</h2>\r\n<strong>\r\nWindows: CTRL+B\r\nMac: CMD+B\r\n</strong>\r\n\r\nThe sidebar in VS Code provides a convenient way to access file explorer, source control, extensions, and other\r\nuseful\r\npanels. Press Ctrl/Cmd+B to toggle the sidebar visibility, giving you more screen real estate for your code.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/toggle-sideba.gif\" alt=\"\" />\r\n<h2>3. Multi-Cursor Editing</h2>\r\n<strong>\r\nWindows: Ctrl+Alt+Arrow Keys\r\nMac: Opt+Cmd+Arrow Keys\r\n</strong>\r\n\r\nMulti-Cursor Editing is a powerful feature in VS Code that allows you to edit multiple lines simultaneously.\r\nPress\r\nCtrl+Alt+Up/Down Arrow Keys to add cursors above or below the current position. Pressing Esc will reset your\r\ncursor.\r\n\r\n<img src=\"/images/blog/multi-cursor-editing1.gif\" alt=\"\" />\r\n\r\nBonus: Holding Alt/Opt and left clicking will allow you to place multiple cursors where ever you click\r\n\r\n<img src=\"/wp-content/uploads/2024/04/multi-cursor-editing2.gif\" alt=\"\" />\r\n<h2>4. Format Document</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nStill not used to code formatting? Press Shift+Alt/Opt+F to automatically format the entire document according to\r\nthe\r\nconfigured code style. Maintaining consistent code formatting is crucial for readability and maintainability. VS\r\nCode\r\nsupports a wide range of programming languages and provides customizable formatting options.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/format-document.gif\" alt=\"\" />\r\n<h2>5. Comment/Uncomment Lines</h2>\r\n<strong>\r\nWindows: Shift+Alt+F\r\nMac: Shift+Opt+F\r\n</strong>\r\n\r\nCommenting code blocks is a necessity when experimenting with code or debugging. To comment or uncomment a single\r\nline\r\nsimply select the line and press Ctrl/Cmd+/ . For a block of code, highlight the desired code to comment out and\r\npress\r\nShift+Alt/Opt+A. This shortcut automatically adds or removes comment symbols based on the file type.\r\n\r\n<img src=\"/wp-content/uploads/2024/04/comment-uncoment-lines.gif\" alt=\"\" />\r\n\r\nThe list above is by no means definitive and we’ve really only scratched the surface of all the keyboard\r\nshortcuts you\r\nneed to maximize your productivity in VS Code. Like with most tools and shortcuts, their usefulness varies\r\nperson to\r\nperson and job to job — find what works best for you. Check out some of the resources below to learn even more\r\nuseful\r\nshortcuts!', '5 Visual Studio Code Shortcuts you need to be using', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2024-04-30 09:15:59', '2024-04-30 09:15:59', '', 18, 'http://dev.portfolio-api.com/?p=27', 0, 'revision', '', 0),
(29, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:10:\"experience\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Experiences fields', 'experiences-fields', 'publish', 'closed', 'closed', '', 'group_6635032d8da17', '', '', '2024-05-10 15:10:22', '2024-05-10 15:10:22', '', 0, 'http://dev.portfolio-api.com/?post_type=acf-field-group&#038;p=29', 0, 'acf-field-group', '', 0),
(30, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Company name', 'experience_company_name', 'publish', 'closed', 'closed', '', 'field_6635033f35d80', '', '', '2024-05-03 17:37:01', '2024-05-03 17:37:01', '', 29, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=30', 0, 'acf-field', '', 0),
(31, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Company url', 'experience_company_url', 'publish', 'closed', 'closed', '', 'field_6635037f35d81', '', '', '2024-05-03 17:37:01', '2024-05-03 17:37:01', '', 29, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=31', 1, 'acf-field', '', 0),
(32, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:9:{s:10:\"aria-label\";s:0:\"\";s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"d/m/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'Start date', 'experience_start_date', 'publish', 'closed', 'closed', '', 'field_6635039635d82', '', '', '2024-05-10 15:09:41', '2024-05-10 15:09:41', '', 29, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=32', 2, 'acf-field', '', 0),
(33, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:9:{s:10:\"aria-label\";s:0:\"\";s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"d/m/Y\";s:13:\"return_format\";s:5:\"m/d/Y\";s:9:\"first_day\";i:1;}', 'End date', 'experience_end_date', 'publish', 'closed', 'closed', '', 'field_663503b635d83', '', '', '2024-05-10 15:10:22', '2024-05-10 15:10:22', '', 29, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=33', 3, 'acf-field', '', 0),
(34, 1, '2024-05-03 15:34:55', '2024-05-03 15:34:55', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:11:\"technologie\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'Technologies', 'experience_technologies', 'publish', 'closed', 'closed', '', 'field_663503d535d84', '', '', '2024-05-03 17:38:51', '2024-05-03 17:38:51', '', 29, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=34', 4, 'acf-field', '', 0),
(35, 1, '2024-05-03 15:50:34', '2024-05-03 15:50:34', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Icon svg', 'tech_icon_svg', 'publish', 'closed', 'closed', '', 'field_663507713e7a6', '', '', '2024-05-03 15:52:07', '2024-05-03 15:52:07', '', 34, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=35', 0, 'acf-field', '', 0),
(36, 1, '2024-05-03 15:50:34', '2024-05-03 15:50:34', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Name', 'tech_name', 'publish', 'closed', 'closed', '', 'field_6635079f3e7a7', '', '', '2024-05-03 15:52:07', '2024-05-03 15:52:07', '', 34, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=36', 1, 'acf-field', '', 0),
(37, 1, '2024-05-03 15:50:34', '2024-05-03 15:50:34', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"d/m/Y\";s:13:\"return_format\";s:5:\"d/m/Y\";s:9:\"first_day\";i:1;}', 'Start date', 'tech_startDate', 'publish', 'closed', 'closed', '', 'field_663507b13e7a8', '', '', '2024-05-03 15:52:07', '2024-05-03 15:52:07', '', 34, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=37', 2, 'acf-field', '', 0),
(40, 1, '2024-05-03 15:57:15', '2024-05-03 15:57:15', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:11:\"technologie\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Technologies field', 'technologies-field', 'publish', 'closed', 'closed', '', 'group_66350936273eb', '', '', '2024-05-05 15:16:45', '2024-05-05 15:16:45', '', 0, 'http://dev.portfolio-api.com/?post_type=acf-field-group&#038;p=40', 0, 'acf-field-group', '', 0),
(41, 1, '2024-05-03 15:57:15', '2024-05-03 15:57:15', 'a:8:{s:4:\"type\";s:11:\"date_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:5:\"d/m/Y\";s:13:\"return_format\";s:5:\"d/m/Y\";s:9:\"first_day\";i:1;}', 'Start date', 'tech_start_date', 'publish', 'closed', 'closed', '', 'field_6635094313c6c', '', '', '2024-05-05 15:16:45', '2024-05-05 15:16:45', '', 40, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=41', 1, 'acf-field', '', 0),
(62, 1, '2024-05-03 16:24:07', '2024-05-03 16:24:07', '', 'HTML', '', 'publish', 'closed', 'closed', '', 'html', '', '', '2024-05-12 15:24:38', '2024-05-12 15:24:38', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=62', 0, 'technologie', '', 0),
(64, 1, '2024-05-03 16:24:29', '2024-05-03 16:24:29', '', 'CSS', '', 'publish', 'closed', 'closed', '', 'css', '', '', '2024-05-12 15:24:53', '2024-05-12 15:24:53', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=64', 1, 'technologie', '', 0),
(65, 1, '2024-05-03 16:24:52', '2024-05-03 16:24:52', '', 'JavaScript', '', 'publish', 'closed', 'closed', '', 'javascript', '', '', '2024-05-12 15:26:16', '2024-05-12 15:26:16', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=65', 4, 'technologie', '', 0),
(66, 1, '2024-05-03 16:25:25', '2024-05-03 16:25:25', '', 'TypeScript', '', 'publish', 'closed', 'closed', '', 'typescript', '', '', '2024-05-12 15:28:03', '2024-05-12 15:28:03', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=66', 6, 'technologie', '', 0),
(67, 1, '2024-05-03 16:26:17', '2024-05-03 16:26:17', '', 'React.js', '', 'publish', 'closed', 'closed', '', 'reactjs', '', '', '2024-05-12 15:27:51', '2024-05-12 15:27:51', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=67', 7, 'technologie', '', 0),
(68, 1, '2024-05-03 16:26:40', '2024-05-03 16:26:40', '', 'Next.js', '', 'publish', 'closed', 'closed', '', 'next-js', '', '', '2024-05-12 15:28:44', '2024-05-12 15:28:44', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=68', 8, 'technologie', '', 0),
(69, 1, '2024-05-03 16:27:00', '2024-05-03 16:27:00', '', 'PHP', '', 'publish', 'closed', 'closed', '', 'php', '', '', '2024-05-12 15:29:29', '2024-05-12 15:29:29', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=69', 9, 'technologie', '', 0),
(70, 1, '2024-05-03 16:27:17', '2024-05-03 16:27:17', '', 'Laravel', '', 'publish', 'closed', 'closed', '', 'laravel', '', '', '2024-05-12 15:29:42', '2024-05-12 15:29:42', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=70', 10, 'technologie', '', 0);
INSERT INTO `portfolio_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(71, 1, '2024-05-03 16:27:38', '2024-05-03 16:27:38', '', 'WordPress', '', 'publish', 'closed', 'closed', '', 'wordpress', '', '', '2024-05-12 15:29:54', '2024-05-12 15:29:54', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=71', 11, 'technologie', '', 0),
(72, 1, '2024-05-03 16:27:58', '2024-05-03 16:27:58', '', 'Drupal', '', 'publish', 'closed', 'closed', '', 'drupal', '', '', '2024-05-12 15:30:06', '2024-05-12 15:30:06', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=72', 12, 'technologie', '', 0),
(73, 1, '2024-05-03 16:28:20', '2024-05-03 16:28:20', '', 'MySQL', '', 'publish', 'closed', 'closed', '', 'mysql', '', '', '2024-05-12 15:31:33', '2024-05-12 15:31:33', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=73', 13, 'technologie', '', 0),
(74, 1, '2024-05-03 16:28:41', '2024-05-03 16:28:41', '', 'Git', '', 'publish', 'closed', 'closed', '', 'git', '', '', '2024-05-12 15:30:33', '2024-05-12 15:30:33', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=74', 14, 'technologie', '', 0),
(75, 1, '2024-05-03 16:29:08', '2024-05-03 16:29:08', '', 'GitLab', '', 'publish', 'closed', 'closed', '', 'gitlab', '', '', '2024-05-12 15:30:46', '2024-05-12 15:30:46', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=75', 15, 'technologie', '', 0),
(79, 1, '2024-05-03 17:46:12', '2024-05-03 17:46:12', '<p>Tasks accomplished :</p>\r\n<ul>\r\n<li>Cutting up PSD/XD mockups</li>\r\n<li>Development of websites using the WordPress and Drupal CMS</li>\r\n<li>Development of specific modules and scripts</li>\r\n<li>Maintenance and updates of websites</li>\r\n<li>Bug fixing</li>\r\n<li>SEO optimization</li>\r\n<li>Security audits</li>\r\n</ul>\r\n\r\n', 'Full Stack Web Developer', '', 'publish', 'closed', 'closed', '', 'full-stack-php-developer', '', '', '2024-05-10 16:04:56', '2024-05-10 16:04:56', '', 0, 'http://dev.portfolio-api.com/?post_type=experience&#038;p=79', 0, 'experience', '', 0),
(80, 1, '2024-05-03 17:42:46', '2024-05-03 17:42:46', '', 'fornet', '', 'inherit', 'open', 'closed', '', 'fornet', '', '', '2024-05-03 17:42:46', '2024-05-03 17:42:46', '', 79, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/fornet.jpg', 0, 'attachment', 'image/jpeg', 0),
(81, 1, '2024-05-03 17:50:21', '2024-05-03 17:50:21', 'Tasks accomplished :\r\n<ul>\r\n 	<li>Needs analysis and solution design</li>\r\n 	<li>Development of web applications using the Laravel framework</li>\r\n 	<li>Creation of websites with the WordPress CMS</li>\r\n 	<li>Management of MySQL databases</li>\r\n 	<li>Hosting applications and websites on CentOS servers and Synology NAS</li>\r\n</ul>', 'PHP/Laravel Web Developer', '', 'publish', 'closed', 'closed', '', 'php-laravel-web-developer', '', '', '2024-05-10 16:07:37', '2024-05-10 16:07:37', '', 0, 'http://dev.portfolio-api.com/?post_type=experience&#038;p=81', 1, 'experience', '', 0),
(82, 1, '2024-05-03 17:48:18', '2024-05-03 17:48:18', '', 'ulteos', '', 'inherit', 'open', 'closed', '', 'ulteos', '', '', '2024-05-03 17:48:18', '2024-05-03 17:48:18', '', 81, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/ulteos.jpg', 0, 'attachment', 'image/jpeg', 0),
(84, 1, '2024-05-03 17:57:26', '2024-05-03 17:57:26', 'Tasks accomplished :\r\n<ul>\r\n 	<li>Needs analysis and solution design</li>\r\n 	<li>Development of specific modules</li>\r\n 	<li>Bug fixing</li>\r\n</ul>', 'PHP Web Developer', '', 'publish', 'closed', 'closed', '', 'php-web-developer', '', '', '2024-05-10 16:10:00', '2024-05-10 16:10:00', '', 0, 'http://dev.portfolio-api.com/?post_type=experience&#038;p=84', 2, 'experience', '', 0),
(85, 1, '2024-05-03 17:55:05', '2024-05-03 17:55:05', '', 'asic', '', 'inherit', 'open', 'closed', '', 'asic', '', '', '2024-05-03 17:55:05', '2024-05-03 17:55:05', '', 84, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/asic.jpg', 0, 'attachment', 'image/jpeg', 0),
(87, 1, '2024-05-04 11:42:57', '2024-05-04 11:42:57', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"project\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Projects fields', 'projects-fields', 'publish', 'closed', 'closed', '', 'group_66361e061b794', '', '', '2024-05-04 11:47:32', '2024-05-04 11:47:32', '', 0, 'http://dev.portfolio-api.com/?post_type=acf-field-group&#038;p=87', 0, 'acf-field-group', '', 0),
(88, 1, '2024-05-04 11:42:57', '2024-05-04 11:42:57', 'a:12:{s:4:\"type\";s:12:\"relationship\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:11:\"technologie\";}s:8:\"taxonomy\";s:0:\"\";s:7:\"filters\";a:1:{i:0;s:6:\"search\";}s:8:\"elements\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:13:\"return_format\";s:6:\"object\";}', 'Technologies', 'project_technologies', 'publish', 'closed', 'closed', '', 'field_66361e51b6c7a', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 87, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=88', 1, 'acf-field', '', 0),
(89, 1, '2024-05-04 11:42:57', '2024-05-04 11:42:57', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Live project url', 'project_live_project_url', 'publish', 'closed', 'closed', '', 'field_66361edfb6c7b', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 87, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=89', 2, 'acf-field', '', 0),
(90, 1, '2024-05-04 11:42:57', '2024-05-04 11:42:57', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Gitlab url', 'project_gitlab_url', 'publish', 'closed', 'closed', '', 'field_66361f1db6c7c', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 87, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=90', 3, 'acf-field', '', 0),
(91, 1, '2024-05-04 11:47:24', '2024-05-04 11:47:24', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:0:\"\";}', 'Sections', 'project_sections', 'publish', 'closed', 'closed', '', 'field_66361fdaecf6d', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 87, 'http://dev.portfolio-api.com/?post_type=acf-field&p=91', 0, 'acf-field', '', 0),
(92, 1, '2024-05-04 11:47:24', '2024-05-04 11:47:24', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'project_section_title', 'publish', 'closed', 'closed', '', 'field_6636202becf6f', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 91, 'http://dev.portfolio-api.com/?post_type=acf-field&p=92', 0, 'acf-field', '', 0),
(93, 1, '2024-05-04 11:47:24', '2024-05-04 11:47:24', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Image', 'project_section_image', 'publish', 'closed', 'closed', '', 'field_66362003ecf6e', '', '', '2024-05-04 11:47:24', '2024-05-04 11:47:24', '', 91, 'http://dev.portfolio-api.com/?post_type=acf-field&p=93', 1, 'acf-field', '', 0),
(94, 1, '2024-05-04 12:16:29', '2024-05-04 12:16:29', 'Developing an electronic store website offering a wide range of products including PCs and phones, with features for shopping, cart management, price filtering, pagination, and detailed product information. Leveraging HTML, CSS, JavaScript, React.js, React Router, and Redux for efficient state management and seamless user experience. Demonstrating expertise in front-end and state management technologies for a robust electronic shopping platform.', 'Electronic Store', 'Developing an electronic store website offering a wide range of products including PCs and phones, with features for shopping, cart management, price filtering, pagination, and detailed product information. Leveraging HTML, CSS, JavaScript, React.js, React Router, and Redux for efficient state management and seamless user experience. Demonstrating expertise in front-end and state management technologies for a robust electronic shopping platform.', 'publish', 'closed', 'closed', '', 'electronic-store', '', '', '2024-05-12 15:08:00', '2024-05-12 15:08:00', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=94', 5, 'project', '', 0),
(95, 1, '2024-05-04 12:17:27', '2024-05-04 12:17:27', '', 'leon', '', 'inherit', 'open', 'closed', '', 'leon-2', '', '', '2024-05-04 12:30:18', '2024-05-04 12:30:18', '', 110, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/leon.png', 0, 'attachment', 'image/png', 0),
(96, 1, '2024-05-04 12:17:28', '2024-05-04 12:17:28', '', 'my-agency', '', 'inherit', 'open', 'closed', '', 'my-agency-2', '', '', '2024-05-04 12:28:55', '2024-05-04 12:28:55', '', 109, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/my-agency.png', 0, 'attachment', 'image/png', 0),
(97, 1, '2024-05-04 12:17:29', '2024-05-04 12:17:29', '', 'netflix', '', 'inherit', 'open', 'closed', '', 'netflix', '', '', '2024-05-04 12:33:27', '2024-05-04 12:33:27', '', 112, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/netflix.png', 0, 'attachment', 'image/png', 0),
(98, 1, '2024-05-04 12:17:30', '2024-05-04 12:17:30', '', 'nike', '', 'inherit', 'open', 'closed', '', 'nike-2', '', '', '2024-05-04 12:26:28', '2024-05-04 12:26:28', '', 108, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/nike.png', 0, 'attachment', 'image/png', 0),
(99, 1, '2024-05-04 12:17:31', '2024-05-04 12:17:31', '', 'portfolio', '', 'inherit', 'open', 'closed', '', 'portfolio-2', '', '', '2024-05-04 12:24:40', '2024-05-04 12:24:40', '', 107, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/portfolio.png', 0, 'attachment', 'image/png', 0),
(100, 1, '2024-05-04 12:17:32', '2024-05-04 12:17:32', '', 'dashboard', '', 'inherit', 'open', 'closed', '', 'dashboard-2', '', '', '2024-05-04 12:34:46', '2024-05-04 12:34:46', '', 113, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/dashboard.png', 0, 'attachment', 'image/png', 0),
(101, 1, '2024-05-04 12:17:32', '2024-05-04 12:17:32', '', 'electro-store', '', 'inherit', 'open', 'closed', '', 'electro-store', '', '', '2024-05-04 12:18:17', '2024-05-04 12:18:17', '', 94, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/electro-store.png', 0, 'attachment', 'image/png', 0),
(102, 1, '2024-05-04 12:17:33', '2024-05-04 12:17:33', '', 'gym', '', 'inherit', 'open', 'closed', '', 'gym-2', '', '', '2024-05-04 12:36:07', '2024-05-04 12:36:07', '', 0, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/gym.png', 0, 'attachment', 'image/png', 0),
(103, 1, '2024-05-04 12:17:34', '2024-05-04 12:17:34', '', 'kasper', '', 'inherit', 'open', 'closed', '', 'kasper-2', '', '', '2024-05-04 12:31:45', '2024-05-04 12:31:45', '', 111, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/kasper.png', 0, 'attachment', 'image/png', 0),
(105, 1, '2024-05-04 12:19:57', '2024-05-04 12:19:57', '', 'real-estate', '', 'inherit', 'open', 'closed', '', 'real-estate-2', '', '', '2024-05-04 12:22:55', '2024-05-04 12:22:55', '', 106, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/real-estate.png', 0, 'attachment', 'image/png', 0),
(106, 1, '2024-05-04 12:22:55', '2024-05-04 12:22:55', '<p>Introducing my latest project: a cutting-edge real estate listings website! Powered by ReactJS, it\'s a showcase of modern technology including React hooks, modern CSS, Framer Motion, and more.</p>\n\n<!-- wp:paragraph -->\n<p></p>\n<!-- /wp:paragraph -->', 'Real Estate Landing Page', 'Introducing my latest project: a cutting-edge real estate listings website! Powered by ReactJS, it\'s a showcase of modern technology including React hooks, modern CSS, Framer Motion, and more.', 'publish', 'closed', 'closed', '', 'real-estate-landing-page', '', '', '2024-05-12 15:07:46', '2024-05-12 15:07:46', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=106', 1, 'project', '', 0),
(107, 1, '2024-05-04 12:24:40', '2024-05-04 12:24:40', 'Creating a comprehensive portfolio website featuring sections on services, work samples, testimonials, and contact information. Utilizing HTML, CSS with Tailwind CSS, JavaScript, and Next.js for a dynamic and responsive experience. Showcasing a seamless integration of front-end technologies for an impactful portfolio presentation.', 'Portfolio', 'Creating a comprehensive portfolio website featuring sections on services, work samples, testimonials, and contact information. Utilizing HTML, CSS with Tailwind CSS, JavaScript, and Next.js for a dynamic and responsive experience. Showcasing a seamless integration of front-end technologies for an impactful portfolio presentation.', 'publish', 'closed', 'closed', '', 'portfolio', '', '', '2024-05-12 15:07:32', '2024-05-12 15:07:32', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=107', 2, 'project', '', 0),
(108, 1, '2024-05-04 12:26:28', '2024-05-04 12:26:28', 'Building a static landing page inspired by Nike\'s design ethos using HTML, CSS with Tailwind CSS, and React.js. Focusing on precise replication of layout and aesthetics. Demonstrating proficiency in front-end technologies for an authentic representation', 'Nike Landing Page', 'Building a static landing page inspired by Nike\'s design ethos using HTML, CSS with Tailwind CSS, and React.js. Focusing on precise replication of layout and aesthetics. Demonstrating proficiency in front-end technologies for an authentic representation', 'publish', 'closed', 'closed', '', 'nike-landing-page', '', '', '2024-05-12 15:07:20', '2024-05-12 15:07:20', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=108', 3, 'project', '', 0),
(109, 1, '2024-05-04 12:28:55', '2024-05-04 12:28:55', 'Elzero Template is a creative one-page PSD template made by Elzero great for a portfolio, agency, or any other web page.PSD file that is fully layered and grouped. Free google font used.I have used my skills in HTML CSS and javascript to convert this PSD file into code.', 'Elzero Template', 'Elzero Template is a creative one-page PSD template made by Elzero great for a portfolio, agency, or any other web page.PSD file that is fully layered and grouped. Free google font used.I have used my skills in HTML CSS and javascript to convert this PSD file into code.', 'publish', 'closed', 'closed', '', 'elzero-template', '', '', '2024-05-12 15:06:54', '2024-05-12 15:06:54', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=109', 10, 'project', '', 0),
(110, 1, '2024-05-04 12:30:18', '2024-05-04 12:30:18', 'Leon is a free PSD template made by Graphberry. It comes in a modern, flat design with vibrant colors. This landing page is great for representing your portfolio or other websites.I have translated the design into code using my skills in HTML and CSS.', 'Leon Landing Page', 'Leon is a free PSD template made by Graphberry. It comes in a modern, flat design with vibrant colors. This landing page is great for representing your portfolio or other websites.I have translated the design into code using my skills in HTML and CSS.', 'publish', 'closed', 'closed', '', 'leon-landing-page', '', '', '2024-05-12 15:06:40', '2024-05-12 15:06:40', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=110', 9, 'project', '', 0),
(111, 1, '2024-05-04 12:31:45', '2024-05-04 12:31:45', 'Kasper is a creative one-page PSD template made by Graphberry great for a portfolio, agency, or any other web page.PSD file that is fully layered and grouped. Free google font used.I have used my skills in HTML CSS and javascript to convert this PSD file into code.', 'Kasper Landing Page', 'Kasper is a creative one-page PSD template made by Graphberry great for a portfolio, agency, or any other web page.PSD file that is fully layered and grouped. Free google font used.I have used my skills in HTML CSS and javascript to convert this PSD file into code.', 'publish', 'closed', 'closed', '', 'kasper-landing-page', '', '', '2024-05-12 15:07:05', '2024-05-12 15:07:05', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=111', 11, 'project', '', 0),
(112, 1, '2024-05-04 12:33:27', '2024-05-04 12:33:27', 'Crafting a Netflix-inspired landing page and movie listings with watch functionality. Using HTML, CSS, and JavaScript to emulate the design and features. Delivering an engaging user experience reminiscent of Netflix\'s interface, showcasing front-end development prowess.', 'Clone netflix', 'Crafting a Netflix-inspired landing page and movie listings with watch functionality. Using HTML, CSS, and JavaScript to emulate the design and features. Delivering an engaging user experience reminiscent of Netflix\'s interface, showcasing front-end development prowess.', 'publish', 'closed', 'closed', '', 'clone-netflix', '', '', '2024-05-12 15:06:20', '2024-05-12 15:06:20', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=112', 7, 'project', '', 0),
(113, 1, '2024-05-04 12:34:46', '2024-05-04 12:34:46', 'Elzero Dashboard is a creative multi-page website designed by Osama Elzero who shares it as a free design. this design is great for a portfolio, agency, or any other web page. Free google font (Cairo Font) used. I have used my skills in HTML CSS to convert this PSD file into code.', 'Elzero Dashboard', 'Elzero Dashboard is a creative multi-page website designed by Osama Elzero who shares it as a free design. this design is great for a portfolio, agency, or any other web page. Free google font (Cairo Font) used. I have used my skills in HTML CSS to convert this PSD file into code.', 'publish', 'closed', 'closed', '', 'elzero-dashboard', '', '', '2024-05-12 15:05:54', '2024-05-12 15:05:54', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=113', 8, 'project', '', 0),
(115, 1, '2024-05-04 14:03:17', '2024-05-04 14:03:17', 'a:8:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:7:\"profile\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";s:12:\"show_in_rest\";i:0;}', 'Profiles field', 'profiles-field', 'publish', 'closed', 'closed', '', 'group_66363d8ba2cff', '', '', '2024-05-05 14:53:54', '2024-05-05 14:53:54', '', 0, 'http://dev.portfolio-api.com/?post_type=acf-field-group&#038;p=115', 0, 'acf-field-group', '', 0),
(116, 1, '2024-05-04 14:03:17', '2024-05-04 14:03:17', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:0:\"\";}', 'Social', 'profile_social', 'publish', 'closed', 'closed', '', 'field_66363dfea660b', '', '', '2024-05-04 14:08:14', '2024-05-04 14:08:14', '', 115, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=116', 0, 'acf-field', '', 0),
(117, 1, '2024-05-04 14:03:17', '2024-05-04 14:03:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Title', 'profile_social_title', 'publish', 'closed', 'closed', '', 'field_66363fcca660e', '', '', '2024-05-04 14:03:17', '2024-05-04 14:03:17', '', 116, 'http://dev.portfolio-api.com/?post_type=acf-field&p=117', 0, 'acf-field', '', 0),
(118, 1, '2024-05-04 14:03:17', '2024-05-04 14:03:17', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Url', 'profile_social_url', 'publish', 'closed', 'closed', '', 'field_66363f94a660c', '', '', '2024-05-04 14:09:22', '2024-05-04 14:09:22', '', 116, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=118', 1, 'acf-field', '', 0),
(119, 1, '2024-05-04 14:03:17', '2024-05-04 14:03:17', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:0:\"\";}', 'Icon svg', 'profile_social_icon_svg', 'publish', 'closed', 'closed', '', 'field_66363faea660d', '', '', '2024-05-05 14:53:54', '2024-05-05 14:53:54', '', 116, 'http://dev.portfolio-api.com/?post_type=acf-field&#038;p=119', 2, 'acf-field', '', 0),
(120, 1, '2024-05-04 14:06:03', '2024-05-04 14:06:03', 'I\'m a software developer with a passion for <b class=\"text-gray-50 font-medium\">coding and algorithm design</b>. I keep up with the latest tech trends and enjoy finding creative solutions to complex problems.\n\nI believe in using technology to make a difference. Whether I\'m working on projects or contributing to open-source, I\'m driven by the potential to solve real-world issues and create positive change.', 'Jawad EL ACHHAB', '', 'publish', 'closed', 'closed', '', 'jawad-el-achhab', '', '', '2024-05-09 13:53:04', '2024-05-09 13:53:04', '', 0, 'http://dev.portfolio-api.com/?post_type=profile&#038;p=120', 0, 'profile', '', 0),
(123, 1, '2024-05-05 15:16:45', '2024-05-05 15:16:45', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";i:4;s:9:\"new_lines\";s:0:\"\";}', 'Icon svg', 'tech_icon_svg', 'publish', 'closed', 'closed', '', 'field_6637a2b8cb1ea', '', '', '2024-05-05 15:16:45', '2024-05-05 15:16:45', '', 40, 'http://dev.portfolio-api.com/?post_type=acf-field&p=123', 0, 'acf-field', '', 0),
(124, 1, '2024-05-05 15:25:27', '2024-05-05 15:25:27', '', 'Laravel', '', 'inherit', 'closed', 'closed', '', '70-autosave-v1', '', '', '2024-05-05 15:25:27', '2024-05-05 15:25:27', '', 70, 'http://dev.portfolio-api.com/?p=124', 0, 'revision', '', 0),
(126, 1, '2024-05-09 10:59:47', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'open', 'open', '', '', '', '', '2024-05-09 10:59:47', '0000-00-00 00:00:00', '', 0, 'http://dev.portfolio-api.com/?p=126', 0, 'post', '', 0),
(128, 1, '2024-05-09 13:47:35', '2024-05-09 13:47:35', '', 'Untitled design', '', 'inherit', 'open', 'closed', '', 'untitled-design', '', '', '2024-05-09 13:47:35', '2024-05-09 13:47:35', '', 120, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/Untitled-design.png', 0, 'attachment', 'image/png', 0),
(131, 1, '2024-05-10 15:17:26', '2024-05-10 15:17:26', '', 'Bootstrap', '', 'publish', 'closed', 'closed', '', 'bootstrap', '', '', '2024-05-12 15:25:59', '2024-05-12 15:25:59', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=131', 3, 'technologie', '', 0),
(132, 1, '2024-05-10 15:51:04', '2024-05-10 15:51:04', '', 'SASS', '', 'publish', 'closed', 'closed', '', 'sass', '', '', '2024-05-12 15:25:27', '2024-05-12 15:25:27', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=132', 2, 'technologie', '', 0),
(133, 1, '2024-05-10 15:52:33', '2024-05-10 15:52:33', '', 'Trello', '', 'publish', 'closed', 'closed', '', 'trello', '', '', '2024-05-10 15:52:33', '2024-05-10 15:52:33', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=133', 22, 'technologie', '', 0),
(134, 1, '2024-05-10 15:53:08', '2024-05-10 15:53:08', '', 'Github', '', 'publish', 'closed', 'closed', '', 'github', '', '', '2024-05-10 15:53:08', '2024-05-10 15:53:08', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=134', 21, 'technologie', '', 0),
(135, 1, '2024-05-10 15:53:57', '2024-05-10 15:53:57', '', 'Jquery', '', 'publish', 'closed', 'closed', '', 'jquery', '', '', '2024-05-12 15:26:29', '2024-05-12 15:26:29', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=135', 5, 'technologie', '', 0),
(136, 1, '2024-05-10 15:54:52', '2024-05-10 15:54:52', '', 'Tailwindcss', '', 'publish', 'closed', 'closed', '', 'tailwindcss', '', '', '2024-05-10 15:54:52', '2024-05-10 15:54:52', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=136', 19, 'technologie', '', 0),
(137, 1, '2024-05-10 15:55:44', '2024-05-10 15:55:44', '', 'Redmine', '', 'publish', 'closed', 'closed', '', 'redmine', '', '', '2024-05-10 15:55:44', '2024-05-10 15:55:44', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=137', 16, 'technologie', '', 0),
(138, 1, '2024-05-10 16:01:20', '2024-05-10 16:01:20', '', 'Gulp', '', 'publish', 'closed', 'closed', '', 'gulp', '', '', '2024-05-10 16:01:20', '2024-05-10 16:01:20', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=138', 20, 'technologie', '', 0),
(139, 1, '2024-05-10 16:03:22', '2024-05-10 16:03:22', '', 'Adobe XD', '', 'publish', 'closed', 'closed', '', 'adobe-xd', '', '', '2024-05-12 01:09:42', '2024-05-12 01:09:42', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=139', 17, 'technologie', '', 0),
(140, 1, '2024-05-10 16:08:10', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2024-05-10 16:08:10', '0000-00-00 00:00:00', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&p=140', 0, 'technologie', '', 0),
(141, 1, '2024-05-10 16:08:11', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2024-05-10 16:08:11', '0000-00-00 00:00:00', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&p=141', 0, 'technologie', '', 0),
(142, 1, '2024-05-10 16:08:12', '0000-00-00 00:00:00', '', 'Auto Draft', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2024-05-10 16:08:12', '0000-00-00 00:00:00', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&p=142', 0, 'technologie', '', 0),
(145, 1, '2024-05-11 11:44:05', '2024-05-11 11:44:05', '<!-- wp:paragraph -->\n<p>Developing a gym landing page featuring sections for \'About Our Gym,\' \'Services,\' \'Regular Gymming Benefits,\' \'Class Schedule,\' \'Our Team Members,\' \'Client Feedback,\' \'Pricing Plans,\' \'Contact Form,\' \'Recent News,\' and \'Blog.\' Using HTML, CSS with Tailwind CSS, JavaScript, and React.js to ensure functionality and interactivity. Demonstrating proficiency in front-end technologies for a fully functional gym website.</p>\n<!-- /wp:paragraph -->', 'Gym Landing Page', 'Developing a gym landing page featuring sections for \'About Our Gym,\' \'Services,\' \'Regular Gymming Benefits,\' \'Class Schedule,\' \'Our Team Members,\' \'Client Feedback,\' \'Pricing Plans,\' \'Contact Form,\' \'Recent News,\' and \'Blog.\' Using HTML, CSS with Tailwind CSS, JavaScript, and React.js to ensure functionality and interactivity. Demonstrating proficiency in front-end technologies for a fully functional gym website.', 'publish', 'closed', 'closed', '', 'gym-landing-page', '', '', '2024-05-12 15:15:07', '2024-05-12 15:15:07', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=145', 0, 'project', '', 0),
(146, 1, '2024-05-11 23:39:45', '2024-05-11 23:39:45', '<!-- wp:paragraph -->\n<p>Creating a dynamic book store website with features for shopping books, managing a cart, exploring authors, \'About Us\' section, contact form, and user registration. Utilizing HTML, CSS, JavaScript, React, React Router, and Context API for seamless navigation and state management. Showcasing expertise in front-end development to deliver a user-friendly book shopping experience.</p>\n<!-- /wp:paragraph -->', 'Book Store', 'Creating a dynamic book store website with features for shopping books, managing a cart, exploring authors, \'About Us\' section, contact form, and user registration. Utilizing HTML, CSS, JavaScript, React, React Router, and Context API for seamless navigation and state management. Showcasing expertise in front-end development to deliver a user-friendly book shopping experience.', 'publish', 'closed', 'closed', '', 'book-store', '', '', '2024-05-12 14:56:15', '2024-05-12 14:56:15', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=146', 4, 'project', '', 0),
(147, 1, '2024-05-11 23:41:57', '2024-05-11 23:41:57', '<!-- wp:paragraph -->\n<p>Developing a static clone of YouTube\'s homepage using HTML, CSS, and JavaScript. Our project aims for accurate replication of layout and design elements. Providing a familiar browsing experience within a static environment, showcasing front-end proficiency.</p>\n<!-- /wp:paragraph -->', 'Clone Youtube', 'Developing a static clone of YouTube\'s homepage using HTML, CSS, and JavaScript. Our project aims for accurate replication of layout and design elements. Providing a familiar browsing experience within a static environment, showcasing front-end proficiency.', 'publish', 'closed', 'closed', '', 'clone-youtube', '', '', '2024-05-12 15:09:02', '2024-05-12 15:09:02', '', 0, 'http://dev.portfolio-api.com/?post_type=project&#038;p=147', 6, 'project', '', 0),
(148, 1, '2024-05-12 01:09:16', '2024-05-12 01:09:16', '', 'Photoshop', '', 'publish', 'closed', 'closed', '', 'photoshop', '', '', '2024-05-12 01:09:16', '2024-05-12 01:09:16', '', 0, 'http://dev.portfolio-api.com/?post_type=technologie&#038;p=148', 18, 'technologie', '', 0),
(149, 1, '2024-05-12 01:25:32', '2024-05-12 01:25:32', '', 'YouTube Clone UI', '', 'inherit', 'open', 'closed', '', 'youtube-clone-ui', '', '', '2024-05-12 01:25:32', '2024-05-12 01:25:32', '', 147, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/YouTube-Clone-UI.png', 0, 'attachment', 'image/png', 0),
(150, 1, '2024-05-12 01:29:22', '2024-05-12 01:29:22', '', 'Book Store', '', 'inherit', 'open', 'closed', '', 'book-store-2', '', '', '2024-05-12 01:29:22', '2024-05-12 01:29:22', '', 146, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/Book-Store.png', 0, 'attachment', 'image/png', 0),
(155, 1, '2024-05-12 15:15:02', '2024-05-12 15:15:02', '', 'Fitness Gym (1)', '', 'inherit', 'open', 'closed', '', 'fitness-gym-1', '', '', '2024-05-12 15:15:02', '2024-05-12 15:15:02', '', 145, 'http://dev.portfolio-api.com/wp-content/uploads/2024/05/Fitness-Gym-1.png', 0, 'attachment', 'image/png', 0);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_termmeta`
--

CREATE TABLE `portfolio_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_termmeta`
--

INSERT INTO `portfolio_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_pll_strings_translations', 'a:1:{i:0;a:2:{i:0;s:13:\"Portfolio API\";i:1;s:13:\"Portfolio API\";}}'),
(2, 5, '_pll_strings_translations', 'a:1:{i:0;a:2:{i:0;s:13:\"Portfolio API\";i:1;s:13:\"Portfolio API\";}}');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_terms`
--

CREATE TABLE `portfolio_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) NOT NULL DEFAULT '',
  `slug` varchar(200) NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_terms`
--

INSERT INTO `portfolio_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Uncategorized', 'uncategorized', 0),
(2, 'English', 'en', 0),
(3, 'English', 'pll_en', 0),
(4, 'pll_6630b21805243', 'pll_6630b21805243', 0),
(5, 'Français', 'fr', 0),
(6, 'Français', 'pll_fr', 0),
(7, 'Uncategorized', 'uncategorized-fr', 0),
(8, 'pll_6630b22ec5c85', 'pll_6630b22ec5c85', 0),
(10, 'twentytwentyfour', 'twentytwentyfour', 0);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_term_relationships`
--

CREATE TABLE `portfolio_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `term_order` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_term_relationships`
--

INSERT INTO `portfolio_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 3, 0),
(1, 4, 0),
(7, 4, 0),
(7, 6, 0),
(10, 10, 0),
(11, 1, 0),
(11, 2, 0),
(15, 1, 0),
(15, 2, 0),
(18, 1, 0),
(18, 2, 0),
(62, 2, 0),
(64, 2, 0),
(65, 2, 0),
(66, 2, 0),
(67, 2, 0),
(68, 2, 0),
(69, 2, 0),
(70, 2, 0),
(71, 2, 0),
(72, 2, 0),
(73, 2, 0),
(74, 2, 0),
(75, 2, 0),
(79, 2, 0),
(81, 2, 0),
(84, 2, 0),
(94, 2, 0),
(106, 2, 0),
(107, 2, 0),
(108, 2, 0),
(109, 2, 0),
(110, 2, 0),
(111, 2, 0),
(112, 2, 0),
(113, 2, 0),
(120, 2, 0),
(126, 2, 0),
(131, 2, 0),
(132, 2, 0),
(133, 2, 0),
(134, 2, 0),
(135, 2, 0),
(136, 2, 0),
(137, 2, 0),
(138, 2, 0),
(139, 2, 0),
(140, 2, 0),
(141, 2, 0),
(142, 2, 0),
(145, 2, 0),
(146, 2, 0),
(147, 2, 0),
(148, 2, 0);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_term_taxonomy`
--

CREATE TABLE `portfolio_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `taxonomy` varchar(32) NOT NULL DEFAULT '',
  `description` longtext NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `count` bigint(20) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_term_taxonomy`
--

INSERT INTO `portfolio_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 3),
(2, 2, 'language', 'a:3:{s:6:\"locale\";s:5:\"en_GB\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"gb\";}', 0, 42),
(3, 3, 'term_language', '', 0, 1),
(4, 4, 'term_translations', 'a:2:{s:2:\"en\";i:1;s:2:\"fr\";i:7;}', 0, 2),
(5, 5, 'language', 'a:3:{s:6:\"locale\";s:5:\"fr_FR\";s:3:\"rtl\";i:0;s:9:\"flag_code\";s:2:\"fr\";}', 0, 0),
(6, 6, 'term_language', '', 0, 1),
(7, 7, 'category', '', 0, 0),
(8, 8, 'term_translations', 'a:1:{s:2:\"en\";i:7;}', 0, 0),
(10, 10, 'wp_theme', '', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_usermeta`
--

CREATE TABLE `portfolio_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `meta_key` varchar(255) DEFAULT NULL,
  `meta_value` longtext DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_usermeta`
--

INSERT INTO `portfolio_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'admin'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'portfolio_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'portfolio_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"6ec3c3e9e54c9fa269a9497fa429f62ea72b783aec6ecc84292b6895cae06e74\";a:4:{s:10:\"expiration\";i:1715698935;s:2:\"ip\";s:9:\"127.0.0.1\";s:2:\"ua\";s:111:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36\";s:5:\"login\";i:1715526135;}}'),
(17, 1, 'portfolio_dashboard_quick_press_last_post_id', '126'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:9:\"127.0.0.0\";}'),
(19, 1, 'closedpostboxes_dashboard', 'a:5:{i:0;s:21:\"dashboard_site_health\";i:1;s:19:\"dashboard_right_now\";i:2;s:18:\"dashboard_activity\";i:3;s:21:\"dashboard_quick_press\";i:4;s:17:\"dashboard_primary\";}'),
(20, 1, 'metaboxhidden_dashboard', 'a:0:{}'),
(21, 1, 'portfolio_persisted_preferences', 'a:3:{s:14:\"core/edit-post\";a:2:{s:26:\"isComplementaryAreaVisible\";b:1;s:12:\"welcomeGuide\";b:0;}s:9:\"_modified\";s:24:\"2024-05-11T10:48:25.415Z\";s:4:\"core\";a:2:{s:10:\"openPanels\";a:3:{i:0;s:15:\"page-attributes\";i:1;s:14:\"featured-image\";i:2;s:12:\"post-excerpt\";}s:10:\"editorMode\";s:6:\"visual\";}}'),
(22, 1, 'portfolio_user-settings', 'editor=html&libraryContent=browse&hidetb=0'),
(23, 1, 'portfolio_user-settings-time', '1714925911'),
(24, 1, 'meta-box-order_project', 'a:4:{s:15:\"acf_after_title\";s:0:\"\";s:4:\"side\";s:51:\"submitdiv,categorydiv,tagsdiv-post_tag,postimagediv\";s:6:\"normal\";s:60:\"postexcerpt,acf-group_66361e061b794,commentstatusdiv,slugdiv\";s:8:\"advanced\";s:0:\"\";}'),
(25, 1, 'screen_layout_project', '2'),
(26, 1, 'rcpo-post-first-pointer-dismissed', '1'),
(27, 1, 'manageedit-acf-ui-options-pagecolumnshidden', 'a:1:{i:0;s:7:\"acf-key\";}'),
(28, 1, 'acf_user_settings', 'a:1:{s:23:\"options-pages-first-run\";b:1;}'),
(29, 1, 'closedpostboxes_profile', 'a:1:{i:0;s:6:\"ml_box\";}'),
(30, 1, 'metaboxhidden_profile', 'a:0:{}'),
(31, 1, 'wfls-last-login', '1715526135');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_users`
--

CREATE TABLE `portfolio_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) NOT NULL DEFAULT '',
  `user_pass` varchar(255) NOT NULL DEFAULT '',
  `user_nicename` varchar(50) NOT NULL DEFAULT '',
  `user_email` varchar(100) NOT NULL DEFAULT '',
  `user_url` varchar(100) NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT 0,
  `display_name` varchar(250) NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Déchargement des données de la table `portfolio_users`
--

INSERT INTO `portfolio_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'admin', '$P$B925hEODg2vcq95iulW9RNiYtX0IRN.', 'admin', 'elachhabjawad@gmail.com', 'http://dev.portfolio-api.com', '2024-04-30 08:50:10', '', 0, 'admin');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfblockediplog`
--

CREATE TABLE `portfolio_wfblockediplog` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `countryCode` varchar(2) NOT NULL,
  `blockCount` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `unixday` int(10) UNSIGNED NOT NULL,
  `blockType` varchar(50) NOT NULL DEFAULT 'generic'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfblocks7`
--

CREATE TABLE `portfolio_wfblocks7` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `blockedTime` bigint(20) NOT NULL,
  `reason` varchar(255) NOT NULL,
  `lastAttempt` int(10) UNSIGNED DEFAULT 0,
  `blockedHits` int(10) UNSIGNED DEFAULT 0,
  `expiration` bigint(20) UNSIGNED NOT NULL DEFAULT 0,
  `parameters` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfconfig`
--

CREATE TABLE `portfolio_wfconfig` (
  `name` varchar(100) NOT NULL,
  `val` longblob DEFAULT NULL,
  `autoload` enum('no','yes') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `portfolio_wfconfig`
--

INSERT INTO `portfolio_wfconfig` (`name`, `val`, `autoload`) VALUES
('activatingIP', 0x3132372e302e302e31, 'yes'),
('actUpdateInterval', 0x32, 'yes'),
('addCacheComment', 0x30, 'yes'),
('advancedCommentScanning', 0x31, 'yes'),
('ajaxWatcherDisabled_admin', 0x30, 'yes'),
('ajaxWatcherDisabled_front', 0x30, 'yes'),
('alertEmails', '', 'yes'),
('alertOn_adminLogin', 0x31, 'yes'),
('alertOn_block', 0x31, 'yes'),
('alertOn_breachLogin', 0x31, 'yes'),
('alertOn_firstAdminLoginOnly', 0x30, 'yes'),
('alertOn_firstNonAdminLoginOnly', 0x30, 'yes'),
('alertOn_loginLockout', 0x31, 'yes'),
('alertOn_lostPasswdForm', 0x31, 'yes'),
('alertOn_nonAdminLogin', 0x30, 'yes'),
('alertOn_scanIssues', 0x31, 'yes'),
('alertOn_severityLevel', 0x3235, 'yes'),
('alertOn_throttle', 0x30, 'yes'),
('alertOn_update', 0x30, 'yes'),
('alertOn_wafDeactivated', 0x31, 'yes'),
('alertOn_wordfenceDeactivated', 0x31, 'yes'),
('alert_maxHourly', 0x30, 'yes'),
('allowed404s', 0x2f66617669636f6e2e69636f0a2f6170706c652d746f7563682d69636f6e2a2e706e670a2f2a4032782e706e670a2f62726f77736572636f6e6669672e786d6c, 'yes'),
('allowed404s6116Migration', 0x31, 'yes'),
('allowHTTPSCaching', 0x30, 'yes'),
('allowLegacy2FA', 0x30, 'yes'),
('allowMySQLi', 0x31, 'yes'),
('allScansScheduled', 0x613a303a7b7d, 'yes'),
('apiKey', '', 'yes'),
('autoBlockScanners', 0x31, 'yes'),
('autoUpdate', 0x30, 'yes'),
('autoUpdateAttempts', 0x30, 'yes'),
('bannedURLs', '', 'yes'),
('blockCustomText', '', 'yes'),
('blockedTime', 0x333030, 'yes'),
('blocks702Migration', 0x31, 'yes'),
('cacheType', 0x64697361626c6564, 'yes'),
('cbl_action', 0x626c6f636b, 'yes'),
('cbl_bypassRedirDest', '', 'yes'),
('cbl_bypassRedirURL', '', 'yes'),
('cbl_bypassViewURL', '', 'yes'),
('cbl_cookieVal', 0x36363363616363333331323462, 'yes'),
('cbl_loggedInBlocked', '', 'yes'),
('cbl_redirURL', '', 'yes'),
('cbl_restOfSiteBlocked', 0x31, 'yes'),
('checkSpamIP', 0x31, 'yes'),
('config701Migration', 0x31, 'yes'),
('config720Migration', 0x31, 'yes'),
('dbTest', 0x613a313a7b733a353a226e6f6e6365223b733a36343a2231646237396532326537326165313231393734386464333439646265623934656364303233383937393334663562643430323033363232303263306135343161223b7d, 'no'),
('dbVersion', 0x31302e342e32372d4d617269614442, 'yes'),
('debugOn', 0x30, 'yes'),
('deleteTablesOnDeact', 0x30, 'yes'),
('detectProxyNextCheck', 0x31373135383537323138, 'no'),
('detectProxyNonce', 0x35626563313731346665383761366463636131653832613066306133653733326234633761356133383164323464343163353538346339633433313532616232, 'no'),
('detectProxyRecommendation', '', 'no'),
('diagnosticsWflogsRemovalHistory', 0x5b5d, 'no'),
('disableCodeExecutionUploads', 0x30, 'yes'),
('disableConfigCaching', 0x30, 'yes'),
('disableWAFIPBlocking', 0x30, 'yes'),
('dismissAutoPrependNotice', 0x30, 'yes'),
('displayAutomaticBlocks', 0x31, 'yes'),
('displayTopLevelBlocking', 0x30, 'yes'),
('displayTopLevelLiveTraffic', 0x30, 'yes'),
('displayTopLevelOptions', 0x31, 'yes'),
('email_summary_dashboard_widget_enabled', 0x31, 'yes'),
('email_summary_enabled', 0x31, 'yes'),
('email_summary_excluded_directories', 0x77702d636f6e74656e742f63616368652c77702d636f6e74656e742f77666c6f6773, 'yes'),
('email_summary_interval', 0x7765656b6c79, 'yes'),
('enableRemoteIpLookup', 0x31, 'yes'),
('encKey', 0x33363733333763373262313933326138, 'yes'),
('fileContentsGSB6315Migration', 0x31, 'yes'),
('firewallEnabled', 0x31, 'yes'),
('hasKeyConflict', 0x30, 'yes'),
('howGetIPs', '', 'yes'),
('howGetIPs_trusted_proxies', '', 'yes'),
('howGetIPs_trusted_proxy_preset', '', 'yes'),
('isPaid', '', 'yes'),
('keyType', 0x66726565, 'yes'),
('lastAdminLogin', 0x613a363a7b733a363a22757365724944223b693a313b733a383a22757365726e616d65223b733a353a2261646d696e223b733a393a2266697273744e616d65223b733a303a22223b733a383a226c6173744e616d65223b733a303a22223b733a343a2274696d65223b733a32353a2253756e2031327468204d617920402030333a30323a3135504d223b733a323a224950223b733a393a223132372e302e302e31223b7d, 'yes'),
('lastBlockAggregation', 0x31373135353232323239, 'yes'),
('lastDailyCron', 0x31373135353232323238, 'yes'),
('lastNotificationID', 0x3130, 'no'),
('lastPermissionsTemplateCheck', 0x31373135353232353631, 'yes'),
('liveActivityPauseEnabled', 0x31, 'yes'),
('liveTrafficEnabled', 0x30, 'yes'),
('liveTraf_displayExpandedRecords', 0x30, 'no'),
('liveTraf_ignoreIPs', '', 'yes'),
('liveTraf_ignorePublishers', 0x31, 'yes'),
('liveTraf_ignoreUA', '', 'yes'),
('liveTraf_ignoreUsers', '', 'yes'),
('liveTraf_maxAge', 0x3330, 'yes'),
('liveTraf_maxRows', 0x32303030, 'yes'),
('loginSecurityEnabled', 0x31, 'yes'),
('loginSec_blockAdminReg', 0x31, 'yes'),
('loginSec_breachPasswds', 0x61646d696e73, 'yes'),
('loginSec_breachPasswds_enabled', 0x31, 'yes'),
('loginSec_countFailMins', 0x323430, 'yes'),
('loginSec_disableApplicationPasswords', 0x31, 'yes'),
('loginSec_disableAuthorScan', 0x31, 'yes'),
('loginSec_disableOEmbedAuthor', 0x30, 'yes'),
('loginSec_enableSeparateTwoFactor', '', 'yes'),
('loginSec_lockInvalidUsers', 0x30, 'yes'),
('loginSec_lockoutMins', 0x323430, 'yes'),
('loginSec_maskLoginErrors', 0x31, 'yes'),
('loginSec_maxFailures', 0x3230, 'yes'),
('loginSec_maxForgotPasswd', 0x3230, 'yes'),
('loginSec_requireAdminTwoFactor', 0x30, 'yes'),
('loginSec_strongPasswds', 0x70756273, 'yes'),
('loginSec_strongPasswds_enabled', 0x31, 'yes'),
('loginSec_userBlacklist', '', 'yes'),
('longEncKey', 0x37393336353161636132343730613432626433303432623163633337643266663138636263646636663132383264666261656133653239353837326139333863, 'yes'),
('lowResourceScansEnabled', 0x30, 'yes'),
('manualScanType', 0x6f6e63654461696c79, 'yes'),
('max404Crawlers', 0x44495341424c4544, 'yes'),
('max404Crawlers_action', 0x7468726f74746c65, 'yes'),
('max404Humans', 0x44495341424c4544, 'yes'),
('max404Humans_action', 0x7468726f74746c65, 'yes'),
('maxExecutionTime', 0x30, 'yes'),
('maxGlobalRequests', 0x44495341424c4544, 'yes'),
('maxGlobalRequests_action', 0x7468726f74746c65, 'yes'),
('maxMem', 0x323536, 'yes'),
('maxRequestsCrawlers', 0x44495341424c4544, 'yes'),
('maxRequestsCrawlers_action', 0x7468726f74746c65, 'yes'),
('maxRequestsHumans', 0x44495341424c4544, 'yes'),
('maxRequestsHumans_action', 0x7468726f74746c65, 'yes'),
('migration636_email_summary_excluded_directories', 0x31, 'no'),
('needsNewTour_blocking', 0x31, 'yes'),
('needsNewTour_dashboard', 0x31, 'yes'),
('needsNewTour_firewall', 0x31, 'yes'),
('needsNewTour_livetraffic', 0x31, 'yes'),
('needsNewTour_loginsecurity', 0x31, 'yes'),
('needsNewTour_scan', 0x31, 'yes'),
('needsUpgradeTour_blocking', 0x30, 'yes'),
('needsUpgradeTour_dashboard', 0x30, 'yes'),
('needsUpgradeTour_firewall', 0x30, 'yes'),
('needsUpgradeTour_livetraffic', 0x30, 'yes'),
('needsUpgradeTour_loginsecurity', 0x30, 'yes'),
('needsUpgradeTour_scan', 0x30, 'yes'),
('neverBlockBG', 0x6e65766572426c6f636b5665726966696564, 'yes'),
('notification_blogHighlights', 0x31, 'yes'),
('notification_productUpdates', 0x31, 'yes'),
('notification_promotions', 0x31, 'yes'),
('notification_scanStatus', 0x31, 'yes'),
('notification_securityAlerts', 0x31, 'yes'),
('notification_updatesNeeded', 0x31, 'yes'),
('onboardingAttempt1', 0x736b6970706564, 'yes'),
('onboardingAttempt2', 0x736b6970706564, 'yes'),
('onboardingAttempt3', '', 'no'),
('onboardingAttempt3Initial', 0x30, 'yes'),
('onboardingDelayedAt', 0x31373135353232353735, 'yes'),
('other_blockBadPOST', 0x30, 'yes'),
('other_bypassLitespeedNoabort', 0x30, 'yes'),
('other_hideWPVersion', 0x30, 'yes'),
('other_pwStrengthOnUpdate', 0x31, 'yes'),
('other_scanComments', 0x31, 'yes'),
('other_scanOutside', 0x30, 'yes'),
('other_WFNet', 0x31, 'yes'),
('previousWflogsFileList', 0x5b222e6874616363657373222c2261747461636b2d646174612e706870222c22636f6e6669672d6c6976657761662e706870222c22636f6e6669672d73796e6365642e706870222c22636f6e6669672d7472616e7369656e742e706870222c22636f6e6669672e706870222c2247656f4c697465322d436f756e7472792e6d6d6462222c226970732e706870222c2272756c65732e706870222c2274656d706c6174652e706870225d, 'yes'),
('satisfactionPromptDismissed', 0x30, 'yes'),
('satisfactionPromptInstallDate', 0x31373135323532343137, 'yes'),
('satisfactionPromptOverride', 0x31, 'yes'),
('scansEnabled_checkGSB', 0x31, 'yes'),
('scansEnabled_checkHowGetIPs', 0x31, 'yes'),
('scansEnabled_checkReadableConfig', 0x31, 'yes'),
('scansEnabled_comments', 0x31, 'yes'),
('scansEnabled_core', 0x31, 'yes'),
('scansEnabled_coreUnknown', 0x31, 'yes'),
('scansEnabled_diskSpace', 0x31, 'yes'),
('scansEnabled_fileContents', 0x31, 'yes'),
('scansEnabled_fileContentsGSB', 0x31, 'yes'),
('scansEnabled_geoipSupport', 0x31, 'yes'),
('scansEnabled_highSense', 0x30, 'yes'),
('scansEnabled_malware', 0x31, 'yes'),
('scansEnabled_oldVersions', 0x31, 'yes'),
('scansEnabled_options', 0x31, 'yes'),
('scansEnabled_passwds', 0x31, 'yes'),
('scansEnabled_plugins', 0x30, 'yes'),
('scansEnabled_posts', 0x31, 'yes'),
('scansEnabled_scanImages', 0x30, 'yes'),
('scansEnabled_suspectedFiles', 0x31, 'yes'),
('scansEnabled_suspiciousAdminUsers', 0x31, 'yes'),
('scansEnabled_suspiciousOptions', 0x31, 'yes'),
('scansEnabled_themes', 0x30, 'yes'),
('scansEnabled_wafStatus', 0x31, 'yes'),
('scansEnabled_wpscan_directoryListingEnabled', 0x31, 'yes'),
('scansEnabled_wpscan_fullPathDisclosure', 0x31, 'yes'),
('scanType', 0x7374616e64617264, 'yes'),
('scan_exclude', '', 'yes'),
('scan_force_ipv4_start', 0x30, 'yes'),
('scan_include_extra', '', 'yes'),
('scan_maxDuration', '', 'yes'),
('scan_maxIssues', 0x31303030, 'yes'),
('scan_max_resume_attempts', 0x32, 'yes'),
('schedMode', 0x6175746f, 'yes'),
('schedStartHour', 0x3230, 'yes'),
('scheduledScansEnabled', 0x31, 'yes'),
('serverDNS', 0x313731353235323432333b3333343838303b3132372e302e302e31, 'yes'),
('showAdminBarMenu', 0x31, 'yes'),
('spamvertizeCheck', 0x31, 'yes'),
('ssl_verify', 0x31, 'yes'),
('startScansRemotely', 0x30, 'yes'),
('supportContent', 0x7b7d, 'no'),
('supportHash', '', 'no'),
('timeoffset_wf', 0x2d31, 'yes'),
('timeoffset_wf_updated', 0x31373135353232323239, 'yes'),
('totalAlertsSent', 0x32, 'yes'),
('totalLoginHits', 0x34, 'yes'),
('totalLogins', 0x32, 'yes'),
('touppBypassNextCheck', 0x30, 'yes'),
('touppPromptNeeded', 0x31, 'yes'),
('vulnerabilities_plugin', 0x613a373a7b693a303b613a343a7b733a343a22736c7567223b733a32363a22616476616e6365642d637573746f6d2d6669656c64732d70726f223b733a31313a2266726f6d56657273696f6e223b733a353a22362e322e30223b733a31303a2276756c6e657261626c65223b623a303b733a393a22746f56657273696f6e223b733a353a22362e322e39223b7d693a313b613a333a7b733a343a22736c7567223b733a33343a226a77742d61757468656e7469636174696f6e2d666f722d77702d726573742d617069223b733a31313a2266726f6d56657273696f6e223b733a353a22312e332e34223b733a31303a2276756c6e657261626c65223b623a303b7d693a323b613a333a7b733a343a22736c7567223b733a383a22706f6c796c616e67223b733a31313a2266726f6d56657273696f6e223b733a353a22332e362e31223b733a31303a2276756c6e657261626c65223b623a303b7d693a333b613a333a7b733a343a22736c7567223b733a31363a22706f73742d74797065732d6f72646572223b733a31313a2266726f6d56657273696f6e223b733a353a22322e322e31223b733a31303a2276756c6e657261626c65223b623a303b7d693a343b613a333a7b733a343a22736c7567223b733a31343a227375637572692d7363616e6e6572223b733a31313a2266726f6d56657273696f6e223b733a363a22312e382e3434223b733a31303a2276756c6e657261626c65223b623a303b7d693a353b613a333a7b733a343a22736c7567223b733a393a22776f726466656e6365223b733a31313a2266726f6d56657273696f6e223b733a363a22372e31312e35223b733a31303a2276756c6e657261626c65223b623a303b7d693a363b613a333a7b733a343a22736c7567223b733a31343a227770732d686964652d6c6f67696e223b733a31313a2266726f6d56657273696f6e223b733a383a22312e392e31352e32223b733a31303a2276756c6e657261626c65223b623a303b7d7d, 'yes'),
('wafAlertInterval', 0x363030, 'yes'),
('wafAlertOnAttacks', 0x31, 'yes'),
('wafAlertThreshold', 0x313030, 'yes'),
('wafAlertWhitelist', '', 'yes'),
('waf_status', 0x6c6561726e696e672d6d6f6465, 'yes'),
('whitelisted', '', 'yes'),
('whitelistedServices', 0x7b7d, 'yes'),
('whitelistHash', '', 'no'),
('whitelistPresets', 0x7b7d, 'no'),
('wordfenceI18n', 0x31, 'yes'),
('wordpressPluginVersions', 0x613a373a7b733a32363a22616476616e6365642d637573746f6d2d6669656c64732d70726f223b733a353a22362e322e30223b733a33343a226a77742d61757468656e7469636174696f6e2d666f722d77702d726573742d617069223b733a353a22312e332e34223b733a383a22706f6c796c616e67223b733a353a22332e362e31223b733a31363a22706f73742d74797065732d6f72646572223b733a353a22322e322e31223b733a31343a227375637572692d7363616e6e6572223b733a363a22312e382e3434223b733a393a22776f726466656e6365223b733a363a22372e31312e35223b733a31343a227770732d686964652d6c6f67696e223b733a383a22312e392e31352e32223b7d, 'yes'),
('wordpressThemeVersions', 0x613a313a7b733a393a22706f7274666f6c696f223b733a353a22312e302e30223b7d, 'yes'),
('wordpressVersion', 0x362e352e33, 'yes'),
('wp_home_url', 0x687474703a2f2f6465762e706f7274666f6c696f2d6170692e636f6d, 'yes'),
('wp_site_url', 0x687474703a2f2f6465762e706f7274666f6c696f2d6170692e636f6d, 'yes');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfcrawlers`
--

CREATE TABLE `portfolio_wfcrawlers` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `patternSig` binary(16) NOT NULL,
  `status` char(8) NOT NULL,
  `lastUpdate` int(10) UNSIGNED NOT NULL,
  `PTR` varchar(255) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wffilechanges`
--

CREATE TABLE `portfolio_wffilechanges` (
  `filenameHash` char(64) NOT NULL,
  `file` varchar(1000) NOT NULL,
  `md5` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wffilemods`
--

CREATE TABLE `portfolio_wffilemods` (
  `filenameMD5` binary(16) NOT NULL,
  `filename` varchar(1000) NOT NULL,
  `real_path` text NOT NULL,
  `knownFile` tinyint(3) UNSIGNED NOT NULL,
  `oldMD5` binary(16) NOT NULL,
  `newMD5` binary(16) NOT NULL,
  `SHAC` binary(32) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `stoppedOnSignature` varchar(255) NOT NULL DEFAULT '',
  `stoppedOnPosition` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `isSafeFile` varchar(1) NOT NULL DEFAULT '?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfhits`
--

CREATE TABLE `portfolio_wfhits` (
  `id` int(10) UNSIGNED NOT NULL,
  `attackLogTime` double(17,6) UNSIGNED NOT NULL,
  `ctime` double(17,6) UNSIGNED NOT NULL,
  `IP` binary(16) DEFAULT NULL,
  `jsRun` tinyint(4) DEFAULT 0,
  `statusCode` int(11) NOT NULL DEFAULT 200,
  `isGoogle` tinyint(4) NOT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `newVisit` tinyint(3) UNSIGNED NOT NULL,
  `URL` text DEFAULT NULL,
  `referer` text DEFAULT NULL,
  `UA` text DEFAULT NULL,
  `action` varchar(64) NOT NULL DEFAULT '',
  `actionDescription` text DEFAULT NULL,
  `actionData` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `portfolio_wfhits`
--

INSERT INTO `portfolio_wfhits` (`id`, `attackLogTime`, `ctime`, `IP`, `jsRun`, `statusCode`, `isGoogle`, `userID`, `newVisit`, `URL`, `referer`, `UA`, `action`, `actionDescription`, `actionData`) VALUES
(1, 0.000000, 1715353241.111824, 0x00000000000000000000ffff7f000001, 0, 302, 0, 1, 0, 'http://dev.portfolio-api.com/bo_elachhab/', 'http://dev.portfolio-api.com/bo_elachhab/', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 'loginOK', NULL, NULL),
(2, 0.000000, 1715526135.159609, 0x00000000000000000000ffff7f000001, 0, 200, 0, 1, 0, 'http://dev.portfolio-api.com/bo_elachhab/', 'http://dev.portfolio-api.com/bo_elachhab/?interim-login=1&wp_lang=en_US', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36', 'loginOK', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfhoover`
--

CREATE TABLE `portfolio_wfhoover` (
  `id` int(10) UNSIGNED NOT NULL,
  `owner` text DEFAULT NULL,
  `host` text DEFAULT NULL,
  `path` text DEFAULT NULL,
  `hostKey` varbinary(124) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfissues`
--

CREATE TABLE `portfolio_wfissues` (
  `id` int(10) UNSIGNED NOT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  `lastUpdated` int(10) UNSIGNED NOT NULL,
  `status` varchar(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  `severity` tinyint(3) UNSIGNED NOT NULL,
  `ignoreP` char(32) NOT NULL,
  `ignoreC` char(32) NOT NULL,
  `shortMsg` varchar(255) NOT NULL,
  `longMsg` text DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfknownfilelist`
--

CREATE TABLE `portfolio_wfknownfilelist` (
  `id` int(11) UNSIGNED NOT NULL,
  `path` text NOT NULL,
  `wordpress_path` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wflivetraffichuman`
--

CREATE TABLE `portfolio_wflivetraffichuman` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `identifier` binary(32) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `expiration` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wflocs`
--

CREATE TABLE `portfolio_wflocs` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `ctime` int(10) UNSIGNED NOT NULL,
  `failed` tinyint(3) UNSIGNED NOT NULL,
  `city` varchar(255) DEFAULT '',
  `region` varchar(255) DEFAULT '',
  `countryName` varchar(255) DEFAULT '',
  `countryCode` char(2) DEFAULT '',
  `lat` float(10,7) DEFAULT 0.0000000,
  `lon` float(10,7) DEFAULT 0.0000000
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wflogins`
--

CREATE TABLE `portfolio_wflogins` (
  `id` int(10) UNSIGNED NOT NULL,
  `hitID` int(11) DEFAULT NULL,
  `ctime` double(17,6) UNSIGNED NOT NULL,
  `fail` tinyint(3) UNSIGNED NOT NULL,
  `action` varchar(40) NOT NULL,
  `username` varchar(255) NOT NULL,
  `userID` int(10) UNSIGNED NOT NULL,
  `IP` binary(16) DEFAULT NULL,
  `UA` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `portfolio_wflogins`
--

INSERT INTO `portfolio_wflogins` (`id`, `hitID`, `ctime`, `fail`, `action`, `username`, `userID`, `IP`, `UA`) VALUES
(1, 1, 1715353241.548172, 0, 'loginOK', 'admin', 1, 0x00000000000000000000ffff7f000001, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36'),
(2, 2, 1715526135.331133, 0, 'loginOK', 'admin', 1, 0x00000000000000000000ffff7f000001, 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/124.0.0.0 Safari/537.36');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfls_2fa_secrets`
--

CREATE TABLE `portfolio_wfls_2fa_secrets` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `secret` tinyblob NOT NULL,
  `recovery` blob NOT NULL,
  `ctime` int(10) UNSIGNED NOT NULL,
  `vtime` int(10) UNSIGNED NOT NULL,
  `mode` enum('authenticator') NOT NULL DEFAULT 'authenticator'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfls_role_counts`
--

CREATE TABLE `portfolio_wfls_role_counts` (
  `serialized_roles` varbinary(255) NOT NULL,
  `two_factor_inactive` tinyint(1) NOT NULL,
  `user_count` bigint(20) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=MEMORY DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfls_settings`
--

CREATE TABLE `portfolio_wfls_settings` (
  `name` varchar(191) NOT NULL DEFAULT '',
  `value` longblob DEFAULT NULL,
  `autoload` enum('no','yes') NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `portfolio_wfls_settings`
--

INSERT INTO `portfolio_wfls_settings` (`name`, `value`, `autoload`) VALUES
('2fa-user-grace-period', 0x3130, 'yes'),
('allow-disabling-ntp', 0x31, 'yes'),
('allow-xml-rpc', 0x31, 'yes'),
('captcha-stats', 0x7b22636f756e7473223a5b302c302c302c302c302c302c302c302c302c302c305d2c22617667223a307d, 'yes'),
('delete-deactivation', '', 'yes'),
('disable-temporary-tables', 0x30, 'yes'),
('enable-auth-captcha', '', 'yes'),
('enable-login-history-columns', 0x31, 'yes'),
('enable-shortcode', '', 'yes'),
('enable-woocommerce-account-integration', '', 'yes'),
('enable-woocommerce-integration', '', 'yes'),
('global-notices', 0x5b5d, 'yes'),
('ip-source', '', 'yes'),
('ip-trusted-proxies', '', 'yes'),
('last-secret-refresh', 0x31373135323532343134, 'yes'),
('ntp-failure-count', 0x30, 'yes'),
('ntp-offset', 0x302e3537323733353534383031393431, 'yes'),
('recaptcha-threshold', 0x302e35, 'yes'),
('remember-device', '', 'yes'),
('remember-device-duration', 0x32353932303030, 'yes'),
('require-2fa-grace-period-enabled', '', 'yes'),
('require-2fa.administrator', '', 'yes'),
('schema-version', 0x32, 'yes'),
('shared-hash-secret', 0x32396261613038366235326635633232313232623861656565393635646132306464306138383861643763306437356366623539313761343736376631653636, 'yes'),
('shared-symmetric-secret', 0x63663833366164653264663165316462653634313665393464646663646639346632383166346633653133626138333730336536613134663463346438383235, 'yes'),
('stack-ui-columns', 0x31, 'yes'),
('use-ntp', 0x31, 'yes'),
('user-count-query-state', 0x30, 'yes'),
('whitelisted', '', 'yes'),
('xmlrpc-enabled', 0x31, 'yes');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfnotifications`
--

CREATE TABLE `portfolio_wfnotifications` (
  `id` varchar(32) NOT NULL DEFAULT '',
  `new` tinyint(3) UNSIGNED NOT NULL DEFAULT 1,
  `category` varchar(255) NOT NULL,
  `priority` int(11) NOT NULL DEFAULT 1000,
  `ctime` int(10) UNSIGNED NOT NULL,
  `html` text NOT NULL,
  `links` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Déchargement des données de la table `portfolio_wfnotifications`
--

INSERT INTO `portfolio_wfnotifications` (`id`, `new`, `category`, `priority`, `ctime`, `html`, `links`) VALUES
('site-AEAAAAA', 1, 'wfplugin_updates', 502, 1715522230, '<a href=\"http://dev.portfolio-api.com/wp-admin/update-core.php\">An update is available for 1 plugin</a>', '[]');

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfpendingissues`
--

CREATE TABLE `portfolio_wfpendingissues` (
  `id` int(10) UNSIGNED NOT NULL,
  `time` int(10) UNSIGNED NOT NULL,
  `lastUpdated` int(10) UNSIGNED NOT NULL,
  `status` varchar(10) NOT NULL,
  `type` varchar(20) NOT NULL,
  `severity` tinyint(3) UNSIGNED NOT NULL,
  `ignoreP` char(32) NOT NULL,
  `ignoreC` char(32) NOT NULL,
  `shortMsg` varchar(255) NOT NULL,
  `longMsg` text DEFAULT NULL,
  `data` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfreversecache`
--

CREATE TABLE `portfolio_wfreversecache` (
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `host` varchar(255) NOT NULL,
  `lastUpdate` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfsecurityevents`
--

CREATE TABLE `portfolio_wfsecurityevents` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) NOT NULL DEFAULT '',
  `data` text NOT NULL,
  `event_time` double(14,4) NOT NULL,
  `state` enum('new','sending','sent') NOT NULL DEFAULT 'new',
  `state_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfsnipcache`
--

CREATE TABLE `portfolio_wfsnipcache` (
  `id` int(10) UNSIGNED NOT NULL,
  `IP` varchar(45) NOT NULL DEFAULT '',
  `expiration` timestamp NOT NULL DEFAULT current_timestamp(),
  `body` varchar(255) NOT NULL DEFAULT '',
  `count` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `type` int(10) UNSIGNED NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfstatus`
--

CREATE TABLE `portfolio_wfstatus` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ctime` double(17,6) UNSIGNED NOT NULL,
  `level` tinyint(3) UNSIGNED NOT NULL,
  `type` char(5) NOT NULL,
  `msg` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wftrafficrates`
--

CREATE TABLE `portfolio_wftrafficrates` (
  `eMin` int(10) UNSIGNED NOT NULL,
  `IP` binary(16) NOT NULL DEFAULT '\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0\0',
  `hitType` enum('hit','404') NOT NULL DEFAULT 'hit',
  `hits` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- --------------------------------------------------------

--
-- Structure de la table `portfolio_wfwaffailures`
--

CREATE TABLE `portfolio_wfwaffailures` (
  `id` int(10) UNSIGNED NOT NULL,
  `throwable` text NOT NULL,
  `rule_id` int(10) UNSIGNED DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `portfolio_commentmeta`
--
ALTER TABLE `portfolio_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `portfolio_comments`
--
ALTER TABLE `portfolio_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Index pour la table `portfolio_links`
--
ALTER TABLE `portfolio_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Index pour la table `portfolio_options`
--
ALTER TABLE `portfolio_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Index pour la table `portfolio_postmeta`
--
ALTER TABLE `portfolio_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `portfolio_posts`
--
ALTER TABLE `portfolio_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Index pour la table `portfolio_termmeta`
--
ALTER TABLE `portfolio_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `portfolio_terms`
--
ALTER TABLE `portfolio_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Index pour la table `portfolio_term_relationships`
--
ALTER TABLE `portfolio_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Index pour la table `portfolio_term_taxonomy`
--
ALTER TABLE `portfolio_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Index pour la table `portfolio_usermeta`
--
ALTER TABLE `portfolio_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Index pour la table `portfolio_users`
--
ALTER TABLE `portfolio_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Index pour la table `portfolio_wfblockediplog`
--
ALTER TABLE `portfolio_wfblockediplog`
  ADD PRIMARY KEY (`IP`,`unixday`,`blockType`);

--
-- Index pour la table `portfolio_wfblocks7`
--
ALTER TABLE `portfolio_wfblocks7`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `IP` (`IP`),
  ADD KEY `expiration` (`expiration`);

--
-- Index pour la table `portfolio_wfconfig`
--
ALTER TABLE `portfolio_wfconfig`
  ADD PRIMARY KEY (`name`);

--
-- Index pour la table `portfolio_wfcrawlers`
--
ALTER TABLE `portfolio_wfcrawlers`
  ADD PRIMARY KEY (`IP`,`patternSig`);

--
-- Index pour la table `portfolio_wffilechanges`
--
ALTER TABLE `portfolio_wffilechanges`
  ADD PRIMARY KEY (`filenameHash`);

--
-- Index pour la table `portfolio_wffilemods`
--
ALTER TABLE `portfolio_wffilemods`
  ADD PRIMARY KEY (`filenameMD5`);

--
-- Index pour la table `portfolio_wfhits`
--
ALTER TABLE `portfolio_wfhits`
  ADD PRIMARY KEY (`id`),
  ADD KEY `k1` (`ctime`),
  ADD KEY `k2` (`IP`,`ctime`),
  ADD KEY `attackLogTime` (`attackLogTime`);

--
-- Index pour la table `portfolio_wfhoover`
--
ALTER TABLE `portfolio_wfhoover`
  ADD PRIMARY KEY (`id`),
  ADD KEY `k2` (`hostKey`);

--
-- Index pour la table `portfolio_wfissues`
--
ALTER TABLE `portfolio_wfissues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lastUpdated` (`lastUpdated`),
  ADD KEY `status` (`status`),
  ADD KEY `ignoreP` (`ignoreP`),
  ADD KEY `ignoreC` (`ignoreC`);

--
-- Index pour la table `portfolio_wfknownfilelist`
--
ALTER TABLE `portfolio_wfknownfilelist`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `portfolio_wflivetraffichuman`
--
ALTER TABLE `portfolio_wflivetraffichuman`
  ADD PRIMARY KEY (`IP`,`identifier`),
  ADD KEY `expiration` (`expiration`);

--
-- Index pour la table `portfolio_wflocs`
--
ALTER TABLE `portfolio_wflocs`
  ADD PRIMARY KEY (`IP`);

--
-- Index pour la table `portfolio_wflogins`
--
ALTER TABLE `portfolio_wflogins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `k1` (`IP`,`fail`),
  ADD KEY `hitID` (`hitID`);

--
-- Index pour la table `portfolio_wfls_2fa_secrets`
--
ALTER TABLE `portfolio_wfls_2fa_secrets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Index pour la table `portfolio_wfls_role_counts`
--
ALTER TABLE `portfolio_wfls_role_counts`
  ADD PRIMARY KEY (`serialized_roles`,`two_factor_inactive`);

--
-- Index pour la table `portfolio_wfls_settings`
--
ALTER TABLE `portfolio_wfls_settings`
  ADD PRIMARY KEY (`name`);

--
-- Index pour la table `portfolio_wfnotifications`
--
ALTER TABLE `portfolio_wfnotifications`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `portfolio_wfpendingissues`
--
ALTER TABLE `portfolio_wfpendingissues`
  ADD PRIMARY KEY (`id`),
  ADD KEY `lastUpdated` (`lastUpdated`),
  ADD KEY `status` (`status`),
  ADD KEY `ignoreP` (`ignoreP`),
  ADD KEY `ignoreC` (`ignoreC`);

--
-- Index pour la table `portfolio_wfreversecache`
--
ALTER TABLE `portfolio_wfreversecache`
  ADD PRIMARY KEY (`IP`);

--
-- Index pour la table `portfolio_wfsecurityevents`
--
ALTER TABLE `portfolio_wfsecurityevents`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `portfolio_wfsnipcache`
--
ALTER TABLE `portfolio_wfsnipcache`
  ADD PRIMARY KEY (`id`),
  ADD KEY `expiration` (`expiration`),
  ADD KEY `IP` (`IP`),
  ADD KEY `type` (`type`);

--
-- Index pour la table `portfolio_wfstatus`
--
ALTER TABLE `portfolio_wfstatus`
  ADD PRIMARY KEY (`id`),
  ADD KEY `k1` (`ctime`),
  ADD KEY `k2` (`type`);

--
-- Index pour la table `portfolio_wftrafficrates`
--
ALTER TABLE `portfolio_wftrafficrates`
  ADD PRIMARY KEY (`eMin`,`IP`,`hitType`);

--
-- Index pour la table `portfolio_wfwaffailures`
--
ALTER TABLE `portfolio_wfwaffailures`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `portfolio_commentmeta`
--
ALTER TABLE `portfolio_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `portfolio_comments`
--
ALTER TABLE `portfolio_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `portfolio_links`
--
ALTER TABLE `portfolio_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_options`
--
ALTER TABLE `portfolio_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=840;

--
-- AUTO_INCREMENT pour la table `portfolio_postmeta`
--
ALTER TABLE `portfolio_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=602;

--
-- AUTO_INCREMENT pour la table `portfolio_posts`
--
ALTER TABLE `portfolio_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=156;

--
-- AUTO_INCREMENT pour la table `portfolio_termmeta`
--
ALTER TABLE `portfolio_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `portfolio_terms`
--
ALTER TABLE `portfolio_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `portfolio_term_taxonomy`
--
ALTER TABLE `portfolio_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `portfolio_usermeta`
--
ALTER TABLE `portfolio_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT pour la table `portfolio_users`
--
ALTER TABLE `portfolio_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `portfolio_wfblocks7`
--
ALTER TABLE `portfolio_wfblocks7`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfhits`
--
ALTER TABLE `portfolio_wfhits`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `portfolio_wfhoover`
--
ALTER TABLE `portfolio_wfhoover`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfissues`
--
ALTER TABLE `portfolio_wfissues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfknownfilelist`
--
ALTER TABLE `portfolio_wfknownfilelist`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wflogins`
--
ALTER TABLE `portfolio_wflogins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `portfolio_wfls_2fa_secrets`
--
ALTER TABLE `portfolio_wfls_2fa_secrets`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfpendingissues`
--
ALTER TABLE `portfolio_wfpendingissues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfsecurityevents`
--
ALTER TABLE `portfolio_wfsecurityevents`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfsnipcache`
--
ALTER TABLE `portfolio_wfsnipcache`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfstatus`
--
ALTER TABLE `portfolio_wfstatus`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `portfolio_wfwaffailures`
--
ALTER TABLE `portfolio_wfwaffailures`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
