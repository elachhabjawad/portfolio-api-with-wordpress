<?php

/**
 * Allow SVG file uploads in WordPress by customizing the file type and extension check.
 *
 * @param array $data Existing data for the file type and extension.
 * @param string $file File name with extension.
 * @param string $filename File name without path.
 * @param array $mimes List of allowed MIME types.
 * @return array Modified data with SVG allowed.
 */
function allow_svg_upload($data, $file, $filename, $mimes)
{
    global $wp_version;


    if ($wp_version !== '4.7.1') {
        return $data;
    }

    $filetype = wp_check_filetype($filename, $mimes);

    return [
        'ext'             => $filetype['ext'],
        'type'            => $filetype['type'],
        'proper_filename' => $data['proper_filename']
    ];
}

add_filter('wp_check_filetype_and_ext', 'allow_svg_upload', 10, 4);


/**
 * Adds the SVG MIME type to allow uploads.
 *
 * @param array $mimes Existing MIME types.
 * @return array Modified MIME types with SVG support.
 */

function cc_mime_types($mimes)
{
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

/**
 * Fixes the display of SVG thumbnails in the WordPress admin.
 * Ensures SVG images display properly in the media library.
 */
function fix_svg()
{
    echo '<style type="text/css">
        .attachment-266x266, .thumbnail img {
             width: 100% !important;
             height: auto !important;
        }
        </style>';
}
add_action('admin_head', 'fix_svg');
