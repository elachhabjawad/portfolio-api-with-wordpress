<?php

// Load REST API utilities
require_once __DIR__ . '/rest-api-utils.php';

/**
 * Customizes the REST API response for the "technology" custom post type.
 * It cleans, modifies, and adds custom fields to the REST API response.
 *
 * @param WP_REST_Response $response The original response object.
 * @param WP_Post $post The post object.
 * @param WP_REST_Request $request The REST API request.
 * @return WP_REST_Response The modified response object.
 */
function prepare_technologie_rest_response($response, $post, $request)
{
    clean_rest_response($response);
    modify_rest_response($response);
    add_thumbnail_to_response('iconSvg', $post, $response);

    $field_mappings = [
        'tech_icon_svg' => ['label' => 'iconSvg'],
        'tech_start_date' => ['label' => 'startDate'],
    ];

    add_acf_fields_to_response($field_mappings, $post, $response);

    return $response;
}


add_filter('rest_prepare_technologie', 'prepare_technologie_rest_response', 10, 3);

/**
 * Customizes the REST API response for the "experience" custom post type.
 *
 * @param WP_REST_Response $response The original response object.
 * @param WP_Post $post The post object.
 * @param WP_REST_Request $request The REST API request.
 * @return WP_REST_Response The modified response object.
 */
function prepare_experience_rest_response($response, $post, $request)
{
    clean_rest_response($response);
    modify_rest_response($response);
    add_thumbnail_to_response('companyLogo', $post, $response);

    $field_mappings = [
        'experience_company_name' => ['label' => 'companyName'],
        'experience_company_url' => ['label' => 'companyUrl'],
        'experience_start_date' => ['label' => 'startDate'],
        'experience_end_date' => ['label' => 'endDate'],
    ];

    add_acf_fields_to_response($field_mappings, $post, $response);
    add_related_acf_field('technologies', 'experience_technologies', $post, $response);

    return $response;
}

add_filter('rest_prepare_experience', 'prepare_experience_rest_response', 10, 3);

/**
 * Customizes the REST API response for the "project" custom post type.
 *
 * @param WP_REST_Response $response The original response object.
 * @param WP_Post $post The post object.
 * @param WP_REST_Request $request The REST API request.
 * @return WP_REST_Response The modified response object.
 */
function prepare_project_rest_response($response, $post, $request)
{
    clean_rest_response($response);
    modify_rest_response($response);

    add_thumbnail_to_response('thumbnail', $post, $response);
    add_thumbnail_to_response('pageThumbnail', $post, $response);

    $field_mappings = [
        'project_sections' => [
            'label' => 'sections',
            'fields' => [
                'project_section_title' => ['label' => 'title'],
                'project_section_image' => ['label' => 'image'],
            ],
        ],
        'project_live_project_url' => ['label' => 'liveProjectUrl'],
        'project_gitlab_url' => ['label' => 'gitlabUrl'],
    ];

    add_acf_fields_to_response($field_mappings, $post, $response);
    add_related_acf_field('technologies', 'project_technologies', $post, $response);

    return $response;
}

add_filter('rest_prepare_project', 'prepare_project_rest_response', 10, 3);

/**
 * Customizes the REST API response for the "profile" custom post type.
 *
 * @param WP_REST_Response $response The original response object.
 * @param WP_Post $post The post object.
 * @param WP_REST_Request $request The REST API request.
 * @return WP_REST_Response The modified response object.
 */
function prepare_profile_rest_response($response, $post, $request)
{
    clean_rest_response($response);
    modify_rest_response($response);
    add_thumbnail_to_response('profilePicture', $post, $response);

    // Map ACF fields to the REST response
    $field_mappings = [
        'profile_social' => [
            'label' => 'socials',
            'fields' => [
                'profile_social_title' => ['label' => 'title'],
                'profile_social_url' => ['label' => 'url'],
                'profile_social_icon_svg' => ['label' => 'iconSvg'],
            ],
        ],
    ];

    add_acf_fields_to_response($field_mappings, $post, $response);

    return $response;
}

add_filter('rest_prepare_profile', 'prepare_profile_rest_response', 10, 3);

/**
 * Sets the default order by 'menu_order' for specific custom post types in REST API queries.
 *
 * @param array $args The original query arguments.
 * @param WP_REST_Request $request The REST API request.
 * @return array The modified query arguments with default order set to 'menu_order'.
 */
function default_order_by_menu_order($args, $request)
{
    if (!isset($args['orderby'])) {
        $args['orderby'] = 'menu_order';
        $args['order'] = 'ASC'; 
    }

    return $args;
}

add_filter('rest_technologie_query', 'default_order_by_menu_order', 10, 2);
add_filter('rest_experience_query', 'default_order_by_menu_order', 10, 2);
add_filter('rest_project_query', 'default_order_by_menu_order', 10, 2);
add_filter('rest_profile_query', 'default_order_by_menu_order', 10, 2);
