<?php

/**
 * Cleans the REST API response by removing unnecessary or unwanted fields.
 *
 * @param WP_REST_Response $response The REST API response object.
 */
function clean_rest_response($response)
{
    $fields_to_remove = array(
        'date', 'date_gmt', 'guid', 'modified',
        'featured_media', 'modified_gmt', 'template', 'acf', '_links'
    );

    foreach ($fields_to_remove as $field) {
        unset($response->data[$field]);
    }
}

/**
 * Adds a post's thumbnail to the REST API response.
 *
 * @param string $thumbnail_name The name to use for the thumbnail in the response.
 * @param WP_Post $post The WordPress post object.
 * @param WP_REST_Response $response The REST API response object.
 * @param string $size The thumbnail size to retrieve (default is 'full').
 */
function add_thumbnail_to_response($thumbnail_name, $post, $response, $size = 'full')
{
    $thumbnail_id = get_post_thumbnail_id($post->ID);

    if ($thumbnail_id) {
        $image_src = wp_get_attachment_image_src($thumbnail_id, $size);
        if ($image_src) {
            $response->data[$thumbnail_name] = $image_src[0];
        }
    }
}

/**
 * Modifies the REST API response to ensure proper rendering of the post title.
 *
 * @param WP_REST_Response $response The REST API response object.
 */
function modify_rest_response($response)
{
    if (isset($response->data['title']) && is_array($response->data['title'])) {
        $response->data['title'] = $response->data['title']['rendered'];
    }
}

/**
 * Adds specified Advanced Custom Fields (ACF) to the REST API response.
 *
 * @param array $field_mappings The mapping of ACF field keys to response labels.
 * @param WP_Post $post The WordPress post object.
 * @param WP_REST_Response $response The REST API response object.
 */
function add_acf_fields_to_response($field_mappings, $post, $response)
{
    foreach ($field_mappings as $field_name => $field_info) {
        $field_label = $field_info['label'] ?? $field_name;

        if (isset($field_info['fields'])) {
            $subfield_values = array();
            $repeater_data = get_field($field_name, $post->ID);

            if ($repeater_data && is_array($repeater_data)) {
                foreach ($repeater_data as $repeater_row) {
                    $processed_row = array();

                    foreach ($field_info['fields'] as $subfield_key => $subfield_info) {
                        $subfield_label = $subfield_info['label'] ?? $subfield_key;
                        $processed_row[$subfield_label] = $repeater_row[$subfield_key] ?? null;
                    }

                    $subfield_values[] = $processed_row;
                }

                $response->data[$field_label] = $subfield_values;
            }
        } else {

            $acf_value = get_field($field_name, $post->ID);
            if ($acf_value) {
                $response->data[$field_label] = $acf_value;
            }
        }
    }
}

/**
 * Adds related Advanced Custom Fields (ACF) posts to the REST API response.
 *
 * @param string $field_name The name to use for the related posts in the response.
 * @param string $acf_field_key The ACF field key to retrieve related posts.
 * @param WP_Post $post The WordPress post object.
 * @param WP_REST_Response $response The REST API response object.
 */
function add_related_acf_field($field_name, $acf_field_key, $post, $response)
{
    $related_posts = get_field($acf_field_key, $post->ID);

    if ($related_posts) {
        $related_posts = is_array($related_posts) ? $related_posts : array($related_posts);

        $related_data = array();
        foreach ($related_posts as $related_post) {
            $related_data[] = array(
                'id' => $related_post->ID,
                'title' => $related_post->post_title,
                'content' => $related_post->post_content,
            );
        }

        $response->data[$field_name] = $related_data;
    }
}
