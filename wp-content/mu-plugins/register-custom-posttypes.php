<?php


add_action('init', 'register_custom_post_types');

/**
 * Registers multiple custom post types in WordPress using a generic function.
 * The function creates and registers custom post types based on defined configurations.
 */
function register_custom_post_types()
{
    $post_types = array(
        array(
            'slug' => 'profile',
            'label' => 'Profiles',
            'description' => 'Custom post type for profiles',
            'supports' => array('title', 'editor', 'thumbnail'),
            'menu_icon' => 'dashicons-id-alt',
            'rewrite_slug' => 'profiles'
        ),
        array(
            'slug' => 'technologie',
            'label' => 'Technologies',
            'description' => 'Custom post type for technologies',
            'supports' => array('title'),
            'menu_icon' => 'dashicons-admin-tools',
            'rewrite_slug' => 'technologies'
        ),
        array(
            'slug' => 'project',
            'label' => 'Projects',
            'description' => 'Custom post type for projects',
            'supports' => array('title', 'editor', 'thumbnail', 'excerpt'),
            'menu_icon' => 'dashicons-portfolio',
            'rewrite_slug' => 'projects'
        ),
        array(
            'slug' => 'experience',
            'label' => 'Experiences',
            'description' => 'Custom post type for experiences',
            'supports' => array('title', 'editor', 'thumbnail'),
            'menu_icon' => 'dashicons-businessperson',
            'rewrite_slug' => 'experiences'
        ),
    );

    foreach ($post_types as $post_type) {
        register_post_type($post_type['slug'], array(
            'label' => $post_type['label'],
            'description' => $post_type['description'],
            'public' => true,
            'hierarchical' => false,
            'menu_icon' => $post_type['menu_icon'],
            'show_in_rest' => true,
            'supports' => $post_type['supports'],
            'has_archive' => true,
            'rewrite' => array('slug' => $post_type['rewrite_slug']),
            'show_in_menu' => true,
            'capability_type' => 'post',
        ));
    }
}
